﻿ using Hasar1raGen.Clases;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Linq;

namespace IcgFrontFacturacionH2G
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string _server = "";
            string _database = "";
            string _user = "";
            string _codVendedor = "";
            string _tipodoc = "";
            string _serie = "";
            string _numero = "";
            string _n = "";
            int _tipo = 12;
            int _accion = 0;
            string _textoRegalo1 = "";
            string _textoRegalo2 = "";
            string _textoRegalo3 = "";
            string _textoAjuste = "";
            string _caja = "";
            string _terminal = "";
            string _tef = "0";
            string _ptoVtaManual = "";
            ModelosImpresoras.Modelos _modelo = 0;
            int _puerto = 0;
            int _baudios = 0;
            string _nroComprobante = "";
            string _pathCaballito = "";
            string _pathOlmos = "";
            int _nroClienteOlmos = 0;
            int _nroPosOlmos = 0;
            int _nroRubroOlmos = 0;
            Int64 _cuit = 0;
            string _procesoOlmos = "";

            Hasar1raGen.InfoIrsa _infoIrsa = new Hasar1raGen.InfoIrsa();

            //Lanzamos el programa de XaerSoft para el desdoble de pagos.
            if (File.Exists("icgretailplugins_5.exe"))
            {
                Process p = new Process();
                ProcessStartInfo psi = new ProcessStartInfo("icgretailplugins_5.exe");
                p.StartInfo = psi;
                p.Start();
                p.WaitForExit();
            }
            else
            {
                MessageBox.Show("No se ha encontrado el plugin icgretailplugins_5.exe. Comuniquese con ICG Argentina.");
                return;
            }

            //Espero 5 segundo.
            System.Threading.Thread.Sleep(2000);
            Application.DoEvents();

            //Cambio fiscal10.xml
            if (File.Exists("DesdoblarMediosPagoFile.XML"))
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("DesdoblarMediosPagoFile.XML");

                XmlNodeList _List1 = xDoc.SelectNodes("/doc/bd");
                foreach (XmlNode xn in _List1)
                {
                    _server = xn["server"].InnerText;
                    _database = xn["database"].InnerText;
                    _user = xn["user"].InnerText;
                }

                XmlNodeList _List2 = xDoc.SelectNodes("/doc");
                foreach (XmlNode xn in _List2)
                {
                    _codVendedor = xn["codvendedor"].InnerText;
                    _tipodoc = xn["tipodoc"].InnerText;
                    _serie = xn["serie"].InnerText;
                    _numero = xn["numero"].InnerText;
                    _n = xn["n"].InnerText;
                    _tef = xn["GuardandoTef"].InnerText;
                }
                //MessageBox.Show("Lei Desdoblar");
            
                //Salimos si es un pedido.
                if (_tipodoc.ToUpper() == "PEDVENTA")
                {
                    return;
                }
                //Salimos si es un remito.
                if (_tipodoc.ToUpper() == "ALBCOMPRA")
                {
                    return;
                }
                //Salimos si es un remito.
                if (_tipodoc.ToUpper() == "ALBVENTA")
                {
                    return;
                }

                if (File.Exists("Config1raGeneracion.xml"))
                {
                    XmlDocument xConfig = new XmlDocument();
                    xConfig.Load("Config1raGeneracion.xml");

                    XmlNodeList _ListCfg = xConfig.SelectNodes("/config");
                    foreach (XmlNode xn in _ListCfg)
                    {
                        _caja = xn["caja"].InnerText;
                        _textoRegalo1 = xn["tktregalo1"].InnerText;
                        _textoRegalo2 = xn["tktregalo2"].InnerText;
                        _textoRegalo3 = xn["tktregalo3"].InnerText;
                        _textoAjuste = xn["textoajuste"].InnerText;
                        _ptoVtaManual = xn["ptovtamanual"].InnerText;
                    }

                    XmlNodeList _ListIrsa = xConfig.SelectNodes("/config/Irsa");
                    foreach (XmlNode xn in _ListIrsa)
                    {
                        _infoIrsa.contrato = xn["Contrato"].InnerText;
                        _infoIrsa.local = xn["Local"].InnerText;
                        _infoIrsa.pathSalida = xn["PathSalida"].InnerText;
                        _infoIrsa.pos = xn["Pos"].InnerText;
                        _infoIrsa.rubro = xn["Rubro"].InnerText;
                    }

                    XmlNodeList _ListCaballito = xConfig.SelectNodes("/config/ShoppingCaballito");
                    foreach (XmlNode xn in _ListCaballito)
                    {
                        _pathCaballito = xn["PathSalida"].InnerText;
                    }

                    XmlNodeList _ListOlmos = xConfig.SelectNodes("/config/ShoppingOlmos");
                    foreach (XmlNode xn in _ListOlmos)
                    {
                        _pathOlmos = xn["PathSalida"].InnerText;
                        _nroClienteOlmos = Convert.ToInt32(xn["NroCliente"].InnerText);
                        _nroPosOlmos = Convert.ToInt32(xn["NroPos"].InnerText);
                        _nroRubroOlmos = Convert.ToInt32(xn["Rubro"].InnerText);
                        _cuit = Convert.ToInt64(xn["Cuit"].InnerText);
                        _procesoOlmos = xn["ProcesoOlmos"].InnerText;
                    }

                    XmlNodeList _ListImpresora = xConfig.SelectNodes("/config/impresora");
                    foreach (XmlNode xn in _ListImpresora)
                    {
                        ModelosImpresoras.Modelos x = 0;
                        Enum.TryParse(xn["modelo"].InnerText, out x);
                        _modelo = x;
                        _puerto = Convert.ToInt32(xn["puerto"].InnerText);
                        _baudios = Convert.ToInt32(xn["baudios"].InnerText);

                        Hasar1raGen.Acciones.baudios = _baudios;
                        Hasar1raGen.Acciones.modelo = _modelo;
                        Hasar1raGen.Acciones.puerto = _puerto;
                    }
                    //MessageBox.Show("Lei todos los XML");
                    // Solo imprimimos las que N = B
                    if (_n.ToUpper() == "B")
                    {
                        if (_tef == "0")
                        {
                            // Armamos el stringConnection.
                            string strConnection = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=" + _user + ";Password=masterkey;";
                            // Conectamos.
                            using (SqlConnection _connection = new SqlConnection(strConnection))
                            {
                                try
                                {
                                    _connection.Open();

                                    // Busco el comprobante en FacturasVentaSerieResol.
                                    Hasar1raGen.FacturasVentaSerieResol _fsr = Hasar1raGen.FacturasVentaSerieResol.GetTiquet(_serie, Convert.ToInt32(_numero), _n, _connection);
                                    // Vemos si no existe y la imprimos, si existe la ReImprimimos.
                                    if (_fsr.numeroFiscal == 0)
                                    {
                                        _terminal = System.Environment.MachineName;
                                        // Recuperamos los datos de la cabecera.
                                        Hasar1raGen.Cabecera _cab = Hasar1raGen.Cabecera.GetCabecera(_serie, Convert.ToInt32(_numero), _n, _connection);
                                        List<Hasar1raGen.Items> _items = Hasar1raGen.Items.GetItems(_cab.numserie, _cab.n, _cab.numalbaran, _connection);
                                        List<Hasar1raGen.OtrosTributos> _oTributos = Hasar1raGen.OtrosTributos.GetOtrosTributos(_serie, _n, Convert.ToInt32(_numero), _connection);
                                        List<Hasar1raGen.Recargos> _recargos = Hasar1raGen.Recargos.GetRecargos(_serie, _n, Convert.ToInt32(_numero), _connection);

                                        if (_items.Count > 0)
                                        {
                                            List<Hasar1raGen.Pagos> _pagos = Hasar1raGen.Pagos.GetPagos(_cab.numserie, _cab.numfac, _cab.n, _connection);

                                            bool _cuitOK = true;
                                            bool _rSocialOK = true;
                                            bool _hasChange = false;
                                            string _tipoComprobante = "";

                                            // Vemos si debemos validar el cuit.
                                            if (_cab.regfacturacioncliente != 4)
                                            {

                                                int _digitoValidador = Hasar1raGen.FuncionesVarias.CalcularDigitoCuit(_cab.nrodoccliente.Replace("-", ""));
                                                int _digitorecibido = Convert.ToInt16(_cab.nrodoccliente.Substring(_cab.nrodoccliente.Length - 1));
                                                if (_digitorecibido == _digitoValidador)
                                                    _cuitOK = true;
                                                else
                                                {
                                                    frmCuit _frm = new frmCuit();
                                                    _frm.ShowDialog();
                                                    if (String.IsNullOrEmpty(_frm._cuit))
                                                        _cuitOK = false;
                                                    else
                                                    {
                                                        _cab.nrodoccliente = _frm._cuit;
                                                        _cuitOK = true;
                                                        _hasChange = true;
                                                    }
                                                    _frm.Dispose();
                                                }
                                            }
                                            else
                                            {
                                                if (String.IsNullOrEmpty(_cab.nrodoccliente))
                                                    _cab.nrodoccliente = "11111111";
                                            }

                                            // Validamos la direccion.
                                            if (String.IsNullOrEmpty(_cab.direccioncliente))
                                                _cab.direccioncliente = ".";

                                            // Vaidamos la razon social.
                                            if (String.IsNullOrEmpty(_cab.nombrecliente))
                                            {
                                                frmRazonSocial _frm = new frmRazonSocial();
                                                _frm.ShowDialog();
                                                if (String.IsNullOrEmpty(_frm._rSocial))
                                                    _rSocialOK = false;
                                                else
                                                {
                                                    _cab.nombrecliente = _frm._rSocial;
                                                    _rSocialOK = true;
                                                    _hasChange = true;
                                                }
                                                _frm.Dispose();
                                            }

                                            // Vaidamos la razon social.
                                            if (String.IsNullOrEmpty(_cab.nombrecliente))
                                            {
                                                frmRazonSocial _frm = new frmRazonSocial();
                                                _frm.ShowDialog();
                                                if (String.IsNullOrEmpty(_frm._rSocial))
                                                    _rSocialOK = false;
                                                else
                                                {
                                                    _cab.nombrecliente = _frm._rSocial;
                                                    _rSocialOK = true;
                                                }
                                                _frm.Dispose();
                                            }

                                            switch (_cab.descripcionticket.Substring(0, 3))
                                            {
                                                case "003":
                                                case "008":
                                                    {
                                                        if (_cuitOK)
                                                        {
                                                            if (_rSocialOK)
                                                            {
                                                                // Busco documentos Asociados.
                                                                List<Hasar1raGen.DocumentoAsociado> _docAsoc = Hasar1raGen.DocumentoAsociado.GetDocumentoAsociado(_items[0]._abonoDeNumseie, _items[0]._abonoDe_N, _items[0]._abonoDeNumAlbaran, _connection);
                                                                bool _faltaPapel;
                                                                bool _necesitaCierreZ = false;
                                                                //Vemos si tenemos items con 
                                                                var _it = from element in _items where element._cantidad > 0 select element;
                                                                if (_it.Count() == 0)
                                                                {
                                                                    string _respImpre = Hasar1raGen.Impresiones.ImprimirNotaCreditoAB(_cab, _items, _pagos, _oTributos, _recargos, _docAsoc, _connection, out _faltaPapel, out _necesitaCierreZ, out _nroComprobante);
                                                                    _tipoComprobante = "C";

                                                                    // NECESITA CIERRE Z? 
                                                                    if (!String.IsNullOrEmpty(_respImpre))
                                                                        MessageBox.Show(_respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                            MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                                    if (_faltaPapel)
                                                                        MessageBox.Show(new Form() { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                                    if (Hasar1raGen.FuncionesVarias.TengoComprobantesSinImprimmir(_connection, _caja))
                                                                        MessageBox.Show(new Form() { TopMost = true }, "Existen comprobantes sin fiscalizar. Por favor fiscalicelos o anulelos.",
                                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                }
                                                                else
                                                                {
                                                                   // MessageBox.Show(new Form() { TopMost = true }, "Las notas de credito no pueden tener unidades positivas.",
                                                                   //"ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                    //T123
                                                                    string _respImpre = Hasar1raGen.Impresiones.ImprimirNotaCreditoABMixta(_cab, _items, _pagos, _oTributos, _recargos, _docAsoc, _connection, out _faltaPapel, out _necesitaCierreZ, out _nroComprobante);
                                                                    _tipoComprobante = "C";

                                                                    // NECESITA CIERRE Z? 
                                                                    if (!String.IsNullOrEmpty(_respImpre))
                                                                        MessageBox.Show(_respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                            MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                    //Fin T123
                                                                }
                                                            }
                                                            else
                                                                MessageBox.Show(new Form() { TopMost = true }, "El Cliente no posee Razon Social. Por favor ingresela y luego reimprimar el comprobante.",
                                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        }
                                                        else
                                                            MessageBox.Show(new Form() { TopMost = true }, "El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                        break;
                                                    }
                                                case "001":
                                                case "006":
                                                    {
                                                        if (_cuitOK)
                                                        {
                                                            if (_rSocialOK)
                                                            {
                                                                bool _faltaPapel;
                                                                bool _necesitaCierreZ = false;
                                                                _tipoComprobante = "D";
                                                                string _respImpre = Hasar1raGen.Impresiones.ImprimirFacturasAB(_cab, _items, _pagos, _oTributos, _recargos,
                                                                     _connection, _textoAjuste, out _faltaPapel, out _necesitaCierreZ, out _nroComprobante);

                                                                // NECESITA CIERRE Z?
                                                                string _msg = "Desea imprimir el ticket de cambio?.";
                                                                if(MessageBox.Show(new Form() { TopMost = true }, _msg, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                                {
                                                                    Hasar1raGen.Impresiones.ImprimirTicketRegalo(_cab, _items, _textoRegalo1, _textoRegalo2, _textoRegalo3, _nroComprobante, out _faltaPapel);
                                                                    // puesto + nro 4 + 8
                                                                }

                                                                if (!String.IsNullOrEmpty(_respImpre))
                                                                    MessageBox.Show(new Form() { TopMost = true }, _respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                                if (_faltaPapel)
                                                                    MessageBox.Show(new Form() { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);


                                                                if (Hasar1raGen.FuncionesVarias.TengoComprobantesSinImprimmir(_connection, _caja))
                                                                    MessageBox.Show(new Form() { TopMost = true }, "Existen comprobantes sin fiscalizar. Por favor fiscalicelos o anulelos.",
                                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                            }
                                                            else
                                                                MessageBox.Show(new Form() { TopMost = true }, "El Cliente no posee Razon Social. Por favor ingresela y luego reimprimar el comprobante.",
                                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        }
                                                        else
                                                            MessageBox.Show(new Form() { TopMost = true }, "El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        break;
                                                    }
                                                case "002":
                                                case "007":
                                                    {
                                                        // Notas de Debito.
                                                        MessageBox.Show(new Form() { TopMost = true }, "Notas de Debito. Aún no estan configuradas.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        break;
                                                    }
                                                default:
                                                    {
                                                        MessageBox.Show(new Form() { TopMost = true }, "Tipo de Documento no configurado para su fiscalización.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        break;
                                                    }
                                            }
                                            // Enviamos los cambios a central.
                                            Hasar1raGen.RemTransacciones.GraboRemTransacciones(_terminal, _caja, _cab.z.ToString(), 0, 1, _serie, _numero, _n, _connection);
                                            // vemos si tenemos modificacion.
                                            if (_hasChange)
                                            {
                                                Hasar1raGen.Clientes.UpdateCliente(_cab.nombrecliente, _cab.nrodoccliente, _cab.codcliente, _connection);
                                                Hasar1raGen.RemTransacciones.GraboRemTransacciones(_terminal, _caja, _cab.z.ToString(), _tipo, _accion, "", _cab.codcliente.ToString(), _cab.n, _connection);
                                            }

                                            //Vemos si tenemos que lanzar IRSA.
                                            if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                            {
                                                try
                                                {
                                                    Hasar1raGen.Irsa.LanzarTrancomp(_cab, _pagos, _infoIrsa, _tipoComprobante, _nroComprobante, _connection);
                                                }
                                                catch(Exception ex)
                                                {
                                                    if (!Directory.Exists(@"C:\ICG\IRSALOG"))
                                                    {
                                                        Directory.CreateDirectory(@"C:\ICG\IRSALOG");
                                                    }
                                                    using (StreamWriter sw = new StreamWriter(@"C:\ICG\IRSALOG\icgIrsa.log", true))
                                                    {
                                                        sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                                                        sw.Flush();
                                                    }
                                                }
                                            }


                                            //Vemos si tenemos que lanzar Shoppping Caballito.
                                            if (!String.IsNullOrEmpty(_pathCaballito))
                                            {
                                                try
                                                {
                                                    Hasar1raGen.ShoppingCaballito.LanzarShoppingCaballito(_cab, _pagos, _pathCaballito, "V", _nroComprobante, _connection);
                                                }
                                                catch (Exception ex)
                                                {
                                                    if (!Directory.Exists(@"C:\ICG\CABALLITOLOG"))
                                                    {
                                                        Directory.CreateDirectory(@"C:\ICG\CABALLITOLOG");
                                                    }
                                                    using (StreamWriter sw = new StreamWriter(@"C:\ICG\LOG\icgCaballito.log", true))
                                                    {
                                                        sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                                                        sw.Flush();
                                                    }
                                                }
                                            }

                                            //Vemos si tenemos que lanzar Shoppping Caballito.
                                            if (!String.IsNullOrEmpty(_pathOlmos))
                                            {
                                                try
                                                {
                                                    decimal _cantidad = _items.Sum(i => i._cantidad);
                                                    Hasar1raGen.ShoppingOlmos.LanzarShoppingOlmos(_cab, _pagos, _pathOlmos,  _nroComprobante, _nroClienteOlmos, _nroPosOlmos,
                                                        _nroRubroOlmos, Convert.ToInt32(_cantidad), _cuit, _procesoOlmos, true, _connection);                                                    
                                                }
                                                catch (Exception ex)
                                                {
                                                    if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                                                    {
                                                        Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                                                    }
                                                    using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.log", true))
                                                    {
                                                        sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                                                        sw.Flush();
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show(new Form() { TopMost = true }, "No existen Items registrados. Por favor revise el comprobante.",
                                                               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                                catch (Exception ex)
                                {

                                    MessageBox.Show(new Form() { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                                               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                    if (MessageBox.Show(new Form() { TopMost = true }, "No se pudo establecer comunicacion con la Controladora Fiscal. Desea ingresar el Nro. del Talonario?.",
                                      "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                    {
                                        string _tipoComprobante = Hasar1raGen.FuncionesVarias.QueComprobanteEs(_serie, _numero, _n, _codVendedor, _connection);
                                        Hasar1raGen.SeriesResol _dtoContador = Hasar1raGen.FuncionesVarias.GetContador(_ptoVtaManual, _tipoComprobante.Substring(0, 3), _connection);
                                        imgresoManual frm = new imgresoManual(_tipoComprobante, _ptoVtaManual, _dtoContador.contador + 1);
                                        frm.ShowDialog();
                                        string _ptoVta = frm._ptoVta;
                                        int _nroCbte = frm._nroCbte;
                                        frm.Dispose();
                                        if ( _nroCbte > 0)
                                        {
                                            bool _Ok = Hasar1raGen.FacturasVentaSerieResol.GrabarTiquet(_serie, Convert.ToInt32(_numero), _n, _ptoVta.ToString(), _tipoComprobante, _nroCbte, _connection);
                                            bool _rta1 = Hasar1raGen.FuncionesVarias.UpdateContador(_ptoVtaManual, _tipoComprobante.Substring(0, 3), _nroCbte, _connection);

                                            if (!_Ok)
                                            {
                                                string _numeroError = _ptoVta.ToString().PadLeft(4, '0') + "-" + _nroCbte.ToString().PadLeft(8, '0');
                                                string _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                                                MessageBox.Show(new Form() { TopMost = true }, _rta, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                            }

                                            //Envio de info a Shoppings.
                                            if (!String.IsNullOrEmpty(_infoIrsa.pathSalida) || !String.IsNullOrEmpty(_pathCaballito) || !String.IsNullOrEmpty(_pathOlmos))
                                            {
                                                Hasar1raGen.Cabecera _cab = Hasar1raGen.Cabecera.GetCabecera(_serie, Convert.ToInt32(_numero), _n, _connection);
                                                List<Hasar1raGen.Pagos> _pagos = Hasar1raGen.Pagos.GetPagos(_cab.numserie, _cab.numfac, _cab.n, _connection);
                                                //Vemos si tenemos que lanzar IRSA.
                                                if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                                {
                                                    try
                                                    {
                                                        Hasar1raGen.Irsa.LanzarTrancomp(_cab, _pagos, _infoIrsa, _tipoComprobante, _nroComprobante, _connection);
                                                    }
                                                    catch (Exception exS)
                                                    {
                                                        if (!Directory.Exists(@"C:\ICG\IRSALOG"))
                                                        {
                                                            Directory.CreateDirectory(@"C:\ICG\IRSALOG");
                                                        }
                                                        using (StreamWriter sw = new StreamWriter(@"C:\ICG\IRSALOG\icgIrsa.log", true))
                                                        {
                                                            sw.WriteLine(DateTime.Today.ToShortDateString() + exS.Message);
                                                            sw.Flush();
                                                        }
                                                    }
                                                }
                                                //Vemos si tenemos que lanzar Shoppping Caballito.
                                                if (!String.IsNullOrEmpty(_pathCaballito))
                                                {
                                                    try
                                                    {
                                                        Hasar1raGen.ShoppingCaballito.LanzarShoppingCaballito(_cab, _pagos, _pathCaballito, "V", _nroComprobante, _connection);
                                                    }
                                                    catch (Exception exS)
                                                    {
                                                        if (!Directory.Exists(@"C:\ICG\CABALLITOLOG"))
                                                        {
                                                            Directory.CreateDirectory(@"C:\ICG\CABALLITOLOG");
                                                        }
                                                        using (StreamWriter sw = new StreamWriter(@"C:\ICG\LOG\icgCaballito.log", true))
                                                        {
                                                            sw.WriteLine(DateTime.Today.ToShortDateString() + exS.Message);
                                                            sw.Flush();
                                                        }
                                                    }
                                                }
                                                //Vemos si tenemos que lanzar Shoppping Caballito.
                                                if (!String.IsNullOrEmpty(_pathOlmos))
                                                {
                                                    try
                                                    {
                                                        List<Hasar1raGen.Items> _items = Hasar1raGen.Items.GetItems(_cab.numserie, _cab.n, _cab.numalbaran, _connection);
                                                        decimal _cantidad = _items.Sum(i => i._cantidad);
                                                        Hasar1raGen.ShoppingOlmos.LanzarShoppingOlmos(_cab, _pagos, _pathOlmos, _nroComprobante, _nroClienteOlmos, _nroPosOlmos,
                                                            _nroRubroOlmos, Convert.ToInt32(_cantidad), _cuit, _procesoOlmos, true, _connection);
                                                    }
                                                    catch (Exception exS)
                                                    {
                                                        if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                                                        {
                                                            Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                                                        }
                                                        using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.log", true))
                                                        {
                                                            sw.WriteLine(DateTime.Today.ToShortDateString() + exS.Message);
                                                            sw.Flush();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show(new Form() { TopMost = true }, "No se encuentra el archivo de configuración. Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            else
                MessageBox.Show("NO EXISTE DesdoblarMediosPagoFile.XML no lo proceso.");
            DeleteLog();
        }

        private static void DeleteLog()
        {
            if (File.Exists("Fiscal.log"))
            {
                File.Delete("Fiscal.log");
            }

            if (File.Exists("HasarLog.log"))
            {
                File.Delete("HasarLog.log");
            }
        }

    }
}
