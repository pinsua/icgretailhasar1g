﻿using System;
using System.Windows.Forms;

namespace IcgFrontFacturacionH2G
{
    public partial class imgresoManual : Form
    {
        public string _ptoVta;
        public int _nroCbte = 0;
        public string _tipoComp;

        public imgresoManual(string tComp, string ptovta, int _numero)
        {
            InitializeComponent();
            _tipoComp = tComp;
            _ptoVta = ptovta;
            _nroCbte = _numero;
        }

        private void imgresoManual_Load(object sender, EventArgs e)
        {
            lblTipoComprobante.Text = _tipoComp;
            txtPtoVta.Text = _ptoVta.ToString();
            txtNro.Text = _nroCbte.ToString();
        }

        private void btAceptar_Click(object sender, EventArgs e)
        {
            int _validoCbte = Convert.ToInt32(txtNro.Text);
            //MessageBox.Show("Nro recibido: " + _nroCbte.ToString());
            //MessageBox.Show("Nro ingresado: " + _validoCbte.ToString());
            if (_nroCbte > _validoCbte)
            {
                MessageBox.Show("El numero de comprobante ingresado no puede ser menor al preestablecido.",
                                          "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            else
            {
                if (txtPtoVta.Text.Length > 0 && txtNro.Text.Length > 0)
                {
                    _ptoVta = txtPtoVta.Text;
                    _nroCbte = Convert.ToInt32(txtNro.Text);

                    this.Close();
                }
                else
                    MessageBox.Show("Debe ingresar un valor en el Punto de Venta y en el Numero de Comprobante.!",
                                          "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void txtPtoVta_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) // permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                // el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
