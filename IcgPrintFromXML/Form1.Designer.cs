﻿namespace IcgPrintFromXML
{
    partial class frmMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.grpMonitorearDirectorio = new System.Windows.Forms.GroupBox();
            this.btnDirectorioMonitoreo = new System.Windows.Forms.Button();
            this.edtDirectorioMonitoreo = new System.Windows.Forms.TextBox();
            this.btnDetenerMonitoreo = new System.Windows.Forms.Button();
            this.btnIniciarMonitoreo = new System.Windows.Forms.Button();
            this.brwDirectorioMonitoreo = new System.Windows.Forms.FolderBrowserDialog();
            this.memLog = new System.Windows.Forms.TextBox();
            this.grpOperacionesManuales = new System.Windows.Forms.GroupBox();
            this.btnImprimirXML = new System.Windows.Forms.Button();
            this.btnCierreZ = new System.Windows.Forms.Button();
            this.btnLeerXML = new System.Windows.Forms.Button();
            this.dlgOpenXML = new System.Windows.Forms.OpenFileDialog();
            this.pnlAyuda = new System.Windows.Forms.FlowLayoutPanel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btnCerrarAyuda = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.grpIngresoManual = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbN = new System.Windows.Forms.ComboBox();
            this.lblN = new System.Windows.Forms.Label();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblSerie = new System.Windows.Forms.Label();
            this.edtSerie = new System.Windows.Forms.TextBox();
            this.edtNumero = new System.Windows.Forms.TextBox();
            this.grpMonitorearDirectorio.SuspendLayout();
            this.grpOperacionesManuales.SuspendLayout();
            this.pnlAyuda.SuspendLayout();
            this.grpIngresoManual.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpMonitorearDirectorio
            // 
            this.grpMonitorearDirectorio.Controls.Add(this.btnDirectorioMonitoreo);
            this.grpMonitorearDirectorio.Controls.Add(this.edtDirectorioMonitoreo);
            this.grpMonitorearDirectorio.Controls.Add(this.btnDetenerMonitoreo);
            this.grpMonitorearDirectorio.Controls.Add(this.btnIniciarMonitoreo);
            this.grpMonitorearDirectorio.Location = new System.Drawing.Point(14, 14);
            this.grpMonitorearDirectorio.Name = "grpMonitorearDirectorio";
            this.grpMonitorearDirectorio.Size = new System.Drawing.Size(300, 136);
            this.grpMonitorearDirectorio.TabIndex = 4;
            this.grpMonitorearDirectorio.TabStop = false;
            this.grpMonitorearDirectorio.Text = " Monitorear Directorio ";
            // 
            // btnDirectorioMonitoreo
            // 
            this.btnDirectorioMonitoreo.Location = new System.Drawing.Point(243, 27);
            this.btnDirectorioMonitoreo.Name = "btnDirectorioMonitoreo";
            this.btnDirectorioMonitoreo.Size = new System.Drawing.Size(38, 23);
            this.btnDirectorioMonitoreo.TabIndex = 6;
            this.btnDirectorioMonitoreo.Text = "...";
            this.btnDirectorioMonitoreo.UseVisualStyleBackColor = true;
            this.btnDirectorioMonitoreo.Click += new System.EventHandler(this.BtnDirectorioMonitoreo_Click);
            // 
            // edtDirectorioMonitoreo
            // 
            this.edtDirectorioMonitoreo.Location = new System.Drawing.Point(7, 28);
            this.edtDirectorioMonitoreo.Name = "edtDirectorioMonitoreo";
            this.edtDirectorioMonitoreo.Size = new System.Drawing.Size(236, 21);
            this.edtDirectorioMonitoreo.TabIndex = 5;
            // 
            // btnDetenerMonitoreo
            // 
            this.btnDetenerMonitoreo.Enabled = false;
            this.btnDetenerMonitoreo.Location = new System.Drawing.Point(164, 58);
            this.btnDetenerMonitoreo.Name = "btnDetenerMonitoreo";
            this.btnDetenerMonitoreo.Size = new System.Drawing.Size(117, 27);
            this.btnDetenerMonitoreo.TabIndex = 5;
            this.btnDetenerMonitoreo.Text = "Detener";
            this.btnDetenerMonitoreo.UseVisualStyleBackColor = true;
            this.btnDetenerMonitoreo.Click += new System.EventHandler(this.BtnDetenerMonitoreo_Click);
            // 
            // btnIniciarMonitoreo
            // 
            this.btnIniciarMonitoreo.Location = new System.Drawing.Point(7, 58);
            this.btnIniciarMonitoreo.Name = "btnIniciarMonitoreo";
            this.btnIniciarMonitoreo.Size = new System.Drawing.Size(117, 27);
            this.btnIniciarMonitoreo.TabIndex = 4;
            this.btnIniciarMonitoreo.Text = "Iniciar";
            this.btnIniciarMonitoreo.UseVisualStyleBackColor = true;
            this.btnIniciarMonitoreo.Click += new System.EventHandler(this.BtnIniciarMonitoreo_Click);
            // 
            // memLog
            // 
            this.memLog.BackColor = System.Drawing.Color.White;
            this.memLog.Enabled = false;
            this.memLog.Font = new System.Drawing.Font("MS Reference Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memLog.Location = new System.Drawing.Point(14, 156);
            this.memLog.Multiline = true;
            this.memLog.Name = "memLog";
            this.memLog.ReadOnly = true;
            this.memLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.memLog.Size = new System.Drawing.Size(676, 130);
            this.memLog.TabIndex = 7;
            this.memLog.Text = "[04/01/2019 12:35:00] Imprimir Documento ### - OK";
            // 
            // grpOperacionesManuales
            // 
            this.grpOperacionesManuales.Controls.Add(this.btnImprimirXML);
            this.grpOperacionesManuales.Controls.Add(this.btnCierreZ);
            this.grpOperacionesManuales.Controls.Add(this.btnLeerXML);
            this.grpOperacionesManuales.Location = new System.Drawing.Point(320, 15);
            this.grpOperacionesManuales.Name = "grpOperacionesManuales";
            this.grpOperacionesManuales.Size = new System.Drawing.Size(164, 135);
            this.grpOperacionesManuales.TabIndex = 8;
            this.grpOperacionesManuales.TabStop = false;
            this.grpOperacionesManuales.Text = " Operaciones Manuales ";
            // 
            // btnImprimirXML
            // 
            this.btnImprimirXML.Location = new System.Drawing.Point(27, 21);
            this.btnImprimirXML.Name = "btnImprimirXML";
            this.btnImprimirXML.Size = new System.Drawing.Size(106, 27);
            this.btnImprimirXML.TabIndex = 4;
            this.btnImprimirXML.Text = "Imprimir XML";
            this.btnImprimirXML.UseVisualStyleBackColor = true;
            this.btnImprimirXML.Click += new System.EventHandler(this.btnImprimirXML_Click);
            // 
            // btnCierreZ
            // 
            this.btnCierreZ.Location = new System.Drawing.Point(28, 90);
            this.btnCierreZ.Name = "btnCierreZ";
            this.btnCierreZ.Size = new System.Drawing.Size(105, 27);
            this.btnCierreZ.TabIndex = 3;
            this.btnCierreZ.Text = "CierreZ";
            this.btnCierreZ.UseVisualStyleBackColor = true;
            this.btnCierreZ.Click += new System.EventHandler(this.BtnCierreZ_Click_1);
            // 
            // btnLeerXML
            // 
            this.btnLeerXML.Location = new System.Drawing.Point(27, 57);
            this.btnLeerXML.Name = "btnLeerXML";
            this.btnLeerXML.Size = new System.Drawing.Size(106, 27);
            this.btnLeerXML.TabIndex = 2;
            this.btnLeerXML.Text = "Leer XML";
            this.btnLeerXML.UseVisualStyleBackColor = true;
            this.btnLeerXML.Click += new System.EventHandler(this.btnLeerXML_Click_1);
            // 
            // dlgOpenXML
            // 
            this.dlgOpenXML.FileName = "openFileDialog1";
            // 
            // pnlAyuda
            // 
            this.pnlAyuda.Controls.Add(this.richTextBox1);
            this.pnlAyuda.Controls.Add(this.btnCerrarAyuda);
            this.pnlAyuda.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.pnlAyuda.Location = new System.Drawing.Point(120, 330);
            this.pnlAyuda.Name = "pnlAyuda";
            this.pnlAyuda.Size = new System.Drawing.Size(701, 342);
            this.pnlAyuda.TabIndex = 7;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(0, 3);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(698, 297);
            this.richTextBox1.TabIndex = 10;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // btnCerrarAyuda
            // 
            this.btnCerrarAyuda.Location = new System.Drawing.Point(623, 306);
            this.btnCerrarAyuda.Name = "btnCerrarAyuda";
            this.btnCerrarAyuda.Size = new System.Drawing.Size(75, 23);
            this.btnCerrarAyuda.TabIndex = 11;
            this.btnCerrarAyuda.Text = "Cerrar";
            this.btnCerrarAyuda.UseVisualStyleBackColor = true;
            this.btnCerrarAyuda.Click += new System.EventHandler(this.btnCerrarAyuda_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(615, 301);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(75, 23);
            this.btnCerrar.TabIndex = 12;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.Location = new System.Drawing.Point(534, 301);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(75, 23);
            this.btnAyuda.TabIndex = 13;
            this.btnAyuda.Text = "Ayuda";
            this.btnAyuda.UseVisualStyleBackColor = true;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // grpIngresoManual
            // 
            this.grpIngresoManual.Controls.Add(this.button1);
            this.grpIngresoManual.Controls.Add(this.cmbN);
            this.grpIngresoManual.Controls.Add(this.lblN);
            this.grpIngresoManual.Controls.Add(this.lblNumero);
            this.grpIngresoManual.Controls.Add(this.lblSerie);
            this.grpIngresoManual.Controls.Add(this.edtSerie);
            this.grpIngresoManual.Controls.Add(this.edtNumero);
            this.grpIngresoManual.Location = new System.Drawing.Point(490, 15);
            this.grpIngresoManual.Name = "grpIngresoManual";
            this.grpIngresoManual.Size = new System.Drawing.Size(200, 135);
            this.grpIngresoManual.TabIndex = 14;
            this.grpIngresoManual.TabStop = false;
            this.grpIngresoManual.Text = " Ingreso Manual ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 102);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(182, 27);
            this.button1.TabIndex = 17;
            this.button1.Text = "Imprimir";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbN
            // 
            this.cmbN.FormattingEnabled = true;
            this.cmbN.Items.AddRange(new object[] {
            "N",
            "B"});
            this.cmbN.Location = new System.Drawing.Point(73, 69);
            this.cmbN.Name = "cmbN";
            this.cmbN.Size = new System.Drawing.Size(121, 23);
            this.cmbN.TabIndex = 16;
            // 
            // lblN
            // 
            this.lblN.AutoSize = true;
            this.lblN.Location = new System.Drawing.Point(9, 77);
            this.lblN.Name = "lblN";
            this.lblN.Size = new System.Drawing.Size(15, 15);
            this.lblN.TabIndex = 15;
            this.lblN.Text = "N";
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Location = new System.Drawing.Point(9, 50);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(52, 15);
            this.lblNumero.TabIndex = 14;
            this.lblNumero.Text = "Número";
            // 
            // lblSerie
            // 
            this.lblSerie.AutoSize = true;
            this.lblSerie.Location = new System.Drawing.Point(9, 26);
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(37, 15);
            this.lblSerie.TabIndex = 13;
            this.lblSerie.Text = "Serie";
            // 
            // edtSerie
            // 
            this.edtSerie.Location = new System.Drawing.Point(73, 20);
            this.edtSerie.Name = "edtSerie";
            this.edtSerie.Size = new System.Drawing.Size(121, 21);
            this.edtSerie.TabIndex = 12;
            // 
            // edtNumero
            // 
            this.edtNumero.Location = new System.Drawing.Point(73, 44);
            this.edtNumero.Name = "edtNumero";
            this.edtNumero.Size = new System.Drawing.Size(121, 21);
            this.edtNumero.TabIndex = 11;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 336);
            this.Controls.Add(this.pnlAyuda);
            this.Controls.Add(this.grpIngresoManual);
            this.Controls.Add(this.btnAyuda);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.grpOperacionesManuales);
            this.Controls.Add(this.memLog);
            this.Controls.Add(this.grpMonitorearDirectorio);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmMain";
            this.Text = "IcgPrintFromXML1raG";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.grpMonitorearDirectorio.ResumeLayout(false);
            this.grpMonitorearDirectorio.PerformLayout();
            this.grpOperacionesManuales.ResumeLayout(false);
            this.pnlAyuda.ResumeLayout(false);
            this.grpIngresoManual.ResumeLayout(false);
            this.grpIngresoManual.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox grpMonitorearDirectorio;
        private System.Windows.Forms.Button btnDirectorioMonitoreo;
        private System.Windows.Forms.TextBox edtDirectorioMonitoreo;
        private System.Windows.Forms.Button btnDetenerMonitoreo;
        private System.Windows.Forms.Button btnIniciarMonitoreo;
        private System.Windows.Forms.FolderBrowserDialog brwDirectorioMonitoreo;
        private System.Windows.Forms.TextBox memLog;
        private System.Windows.Forms.GroupBox grpOperacionesManuales;
        private System.Windows.Forms.Button btnCierreZ;
        private System.Windows.Forms.Button btnLeerXML;
        private System.Windows.Forms.OpenFileDialog dlgOpenXML;
        private System.Windows.Forms.FlowLayoutPanel pnlAyuda;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btnCerrarAyuda;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.GroupBox grpIngresoManual;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cmbN;
        private System.Windows.Forms.Label lblN;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblSerie;
        private System.Windows.Forms.TextBox edtSerie;
        private System.Windows.Forms.TextBox edtNumero;
        private System.Windows.Forms.Button btnImprimirXML;
    }
}

