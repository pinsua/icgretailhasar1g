﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace IcgPrintFromXML
{
    public partial class frmMain : Form
    {
        public SqlConnection _conn = new SqlConnection();

        bool _KeyIsOk = true;
        Hasar1raGen.Doc doc;
        int _tipo = 12;
        int _accion = 1;
        public string _caja = "Caja";
        public string _terminal = "Terminal";
        FileSystemWatcher watcher = new FileSystemWatcher();
        private Hasar1raGen.Config1raG config;

        public frmMain()
        {
            InitializeComponent();
        }

        private void GraboRemTransacciones(string _z, string _serie, string _numero, string _N)
        {
            try
            {
                string _sql = "INSERT INTO REM_TRANSACCIONES(TERMINAL, CAJA, CAJANUM, Z, TIPO, ACCION, SERIE, NUMERO, N, FECHA, HORA, FO, IDCENTRAL, TALLA, COLOR, CODIGOSTR) " +
                 "VALUES(@terminal, @caja, 0, @Z, 0, 1, @serie, @numero, @N, @date, @hora, 0, 1, '.', '.', '')";

                using (SqlCommand _cmd = new SqlCommand(_sql, _conn))
                {
                    _cmd.Parameters.AddWithValue("@terminal", _terminal);
                    _cmd.Parameters.AddWithValue("@caja", _caja);
                    _cmd.Parameters.AddWithValue("@Z", Convert.ToInt32(_z));
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", Convert.ToInt32(_numero));
                    _cmd.Parameters.AddWithValue("@N", _N);
                    _cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                    _cmd.Parameters.AddWithValue("@hora", DateTime.Now.Date);

                    int rta = _cmd.ExecuteNonQuery();

                }
            }
            catch { }
        }

        private void ImprimirDocumento(string file, string serie = "", string numero = "", string n = "")
        {
            try
            {
                string testData = File.ReadAllText(file);
                XmlSerializer serializer = new XmlSerializer(typeof(Hasar1raGen.Doc));
                TextReader reader = new StringReader(testData);
                doc = (Hasar1raGen.Doc)serializer.Deserialize(reader);
            }
            catch (InvalidOperationException exc)
            {
                MessageBox.Show(exc.Message, String.Format("Error en el archivo: {0}", file));
                return;
            }
            catch (Exception )
            {
                MessageBox.Show(String.Format("Error leyendo el archivo: {0}", file));
                return;
            }

            try
            {
                string testData = File.ReadAllText(@".\Config1raGeneracion.xml");
                XmlSerializer serializer = new XmlSerializer(typeof(Hasar1raGen.Config1raG));
                TextReader reader = new StringReader(testData);
                config = (Hasar1raGen.Config1raG)serializer.Deserialize(reader);
            }
            catch (InvalidOperationException )
            {
                MessageBox.Show("Error en el la estructura XML del archivo de configuración");
                return;
            }
            catch (Exception )
            {
                MessageBox.Show(String.Format("Error leyendo el archivo: {0}", file));
                return;
            }

            Hasar1raGen.Clases.ModelosImpresoras.Modelos x = 0;
            Enum.TryParse(config.Impresora.Modelo, out x);
            Hasar1raGen.Acciones.baudios = int.Parse(config.Impresora.Baudios);            
            Hasar1raGen.Acciones.modelo = x;
            Hasar1raGen.Acciones.puerto = int.Parse(config.Impresora.Puerto);

            log(String.Format("Serie: {0} - Numero: {1}, N. {2}", serie, numero, n));

            if (Hasar1raGen.Acciones.modelo == 0)
            {
                MessageBox.Show("Modelo de Impresora Incorrecto");
                return;
            }

            if (doc.GuardandoTef == "1")
            {
                log("Impresión cancelada...");
                return;
            }

            log(String.Format("Imprimiendo archivo: {0}", file));
            // memLog.AppendText(String.Format("[{0}] Imprimiendo archivo {1}{2}", DateTime.Now.ToShortTimeString(), file, Environment.NewLine));

            // Armamos el stringConnection.
            string strConnection = "Data Source=" + doc.Bd.Server + ";Initial Catalog=" + doc.Bd.Database + ";User Id=" + doc.Bd.User + ";Password=masterkey;";

            // Validamos el KeySoftware.
            _KeyIsOk = true; // ValidoKeySoftware(_keyIcg);

            // Conectamos.


            try
            {
                if (_conn.State != System.Data.ConnectionState.Open)
                {
                    _conn.ConnectionString = strConnection;
                    _conn.Open();
                }

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
                return;
            }

            try
            {
                if (_KeyIsOk)
                {
                    if (!String.IsNullOrEmpty(doc.Numero))
                    {

                        string _numserie = string.IsNullOrEmpty(serie) ? doc.Serie : serie;// grImpresiones.SelectedRows[0].Cells[0].Value.ToString();
                        string _N = string.IsNullOrEmpty(n) ? doc.N : n; // grImpresiones.SelectedRows[0].Cells[8].Value.ToString();
                        string _numFactura = string.IsNullOrEmpty(numero) ? doc.Numero : numero; // grImpresiones.SelectedRows[0].Cells[1].Value.ToString();
                        string _ptoVta = ""; // grImpresiones.SelectedRows[0].Cells[2].Value.ToString();
                        string _nroFiscal = ""; // grImpresiones.SelectedRows[0].Cells[3].Value.ToString();
                        string _compro = _ptoVta + "-" + _nroFiscal.PadLeft(8, '0');
                        string _z = ""; // grImpresiones.SelectedRows[0].Cells[12].Value.ToString();

                        // Recuperamos los datos de la cabecera.
                        Hasar1raGen.Cabecera _cab = Hasar1raGen.Cabecera.GetCabecera(_numserie, Convert.ToInt32(_numFactura), _N, _conn);
                        List<Hasar1raGen.Items> _items = Hasar1raGen.Items.GetItems(_cab.numserie, _cab.n, _cab.numalbaran, _conn);
                        List<Hasar1raGen.OtrosTributos> _oTributos = Hasar1raGen.OtrosTributos.GetOtrosTributos(_numserie, _N, Convert.ToInt32(_numFactura), _conn);
                        List<Hasar1raGen.Recargos> _recargos = Hasar1raGen.Recargos.GetRecargos(_numserie, _N, Convert.ToInt32(_numFactura), _conn);

                        if (_items.Count > 0)
                        {
                            List<Hasar1raGen.Pagos> _pagos = Hasar1raGen.Pagos.GetPagos(_cab.numserie, _cab.numfac, _cab.n, _conn);
                            List<Hasar1raGen.Items> _lstItems = Hasar1raGen.Items.GetItems(_numserie, _N, _cab.numalbaran, _conn);

                            if (_pagos.Count > 0)
                            {
                                bool _cuitOK = true;
                                bool _rSocialOK = true;
                                bool _hasChange = false;

                                // Vemos si debemos validar el cuit.
                                if (_cab.regfacturacioncliente != 4)
                                {
                                    int _digitoValidador =  Hasar1raGen.FuncionesVarias.CalcularDigitoCuit(_cab.nrodoccliente.Replace("-", ""));
                                    int _digitorecibido = Convert.ToInt16(_cab.nrodoccliente.Substring(_cab.nrodoccliente.Length - 1));
                                    if (_digitorecibido == _digitoValidador)
                                        _cuitOK = true;
                                    else
                                    {                                        
                                        frmCuit _frm = new frmCuit();
                                        _frm.ShowDialog(this);
                                        if (String.IsNullOrEmpty(_frm._cuit))
                                            _cuitOK = false;
                                        else
                                        {
                                            _cab.nrodoccliente = _frm._cuit;
                                            _cuitOK = true;
                                        }
                                        _frm.Dispose();
                                        _hasChange = true;
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(_cab.nrodoccliente))
                                        _cab.nrodoccliente = "11111111";
                                }
                                // Validamos el campo direccion del cliente.
                                if (String.IsNullOrEmpty(_cab.direccioncliente))
                                    _cab.direccioncliente = ".";
                                // Validamos la Razon Social.
                                if (String.IsNullOrEmpty(_cab.nombrecliente))
                                {
                                    frmRazonSocial _frm = new frmRazonSocial();
                                    _frm.ShowDialog(this);
                                    if (String.IsNullOrEmpty(_frm._rSocial))
                                        _rSocialOK = false;
                                    else
                                    {
                                        _cab.nombrecliente = _frm._rSocial;
                                        _rSocialOK = true;
                                    }
                                    _frm.Dispose();
                                    _hasChange = true;
                                }

                                switch (_cab.descripcionticket.Substring(0, 3))
                                {
                                    case "003":
                                    case "008":
                                        {
                                            if (_cuitOK)
                                            {
                                                if (_rSocialOK)
                                                {
                                                    // Busco documentos Asociados.
                                                    List<Hasar1raGen.DocumentoAsociado> _docAsoc = Hasar1raGen.DocumentoAsociado.GetDocumentoAsociado(_items[0]._abonoDeNumseie, _items[0]._abonoDe_N, _items[0]._abonoDeNumAlbaran, _conn);
                                                    bool _faltaPapel;
                                                    bool _necesitaCierreZ = false;

                                                    string _respImpre = Hasar1raGen.Impresiones.ImprimirNotaCreditoAB(_cab, _items, _pagos, _oTributos, _recargos, _docAsoc, _conn, out _faltaPapel, out _necesitaCierreZ);
                                                    if (!String.IsNullOrEmpty(_respImpre))
                                                    {
                                                        log(_respImpre);
                                                        MessageBox.Show(_respImpre, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }
                                                    else
                                                    {
                                                        // Grabamos en Rem_Transacciones para que la mande de nuevo a manager.
                                                        GraboRemTransacciones(_z, _numserie, _numFactura, _N);
                                                    }

                                                    if (_faltaPapel)
                                                        MessageBox.Show("La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                }
                                                else
                                                    MessageBox.Show("El Cliente no posee Razon Social. Por favor ingresela y luego reimprimar el comprobante.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                            }
                                            else
                                                MessageBox.Show("El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                            break;
                                        }
                                    case "001":
                                    case "006":
                                        {
                                            if (_cuitOK)
                                            {
                                                if (_rSocialOK)
                                                {
                                                    bool _necesitaCierreZ = false;
                                                    bool _faltaPapel;
                                                    string _nroComprobante;
                                                    string _respImpre = Hasar1raGen.Impresiones.ImprimirFacturasAB(_cab, _items, _pagos, _oTributos, _recargos, _conn, config.TextoAjuste, out  _faltaPapel, out _necesitaCierreZ, out  _nroComprobante);
                                                    string _msg = "Desea imprimir el ticket de regalo?.";
                                                    if (MessageBox.Show(_msg, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                    {
                                                        Hasar1raGen.Impresiones.ImprimirTicketRegalo(_cab, _items, "", config.Tktregalo1, config.Tktregalo2, config.Tktregalo3, _nroComprobante, false, out _faltaPapel);
                                                        // puesto + nro 4 + 8
                                                    }
                                                    if (!String.IsNullOrEmpty(_respImpre))
                                                    {
                                                        log(_respImpre);
                                                        MessageBox.Show(_respImpre, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }
                                                    else
                                                    {
                                                        // Grabamos en Rem_Transacciones para que la mande de nuevo a manager.
                                                        GraboRemTransacciones(_z, _numserie, _numFactura, _N);
                                                    }

                                                    if (_faltaPapel)
                                                        MessageBox.Show("La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                }
                                                else
                                                    MessageBox.Show("El Cliente no posee Razon Social. Por favor ingresela y luego reimprimar el comprobante.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                            }
                                            else
                                                MessageBox.Show("El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                            break;
                                        }
                                    case "002":
                                    case "007":
                                        {
                                            // Notas de Debito.
                                            MessageBox.Show("Notas de Debito. Aún no estan configuradas.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                            break;
                                        }
                                    default:
                                        {
                                            MessageBox.Show("Tipo de Documento no configurado para su fiscalización.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                            break;
                                        }
                                }
                                // vemos si tenemos modificacion.
                                if (_hasChange)
                                {
                                    Hasar1raGen.Clientes.UpdateCliente(_cab.nombrecliente, _cab.nrodoccliente, _cab.codcliente, _conn);
                                    Hasar1raGen.RemTransacciones.GraboRemTransacciones(_terminal, _caja, _z, _tipo, _accion, "", _cab.codcliente.ToString(), _N, _conn);
                                }
                                // Enviamos los cambios a central.
                                Hasar1raGen.RemTransacciones.GraboRemTransacciones(_terminal, _caja, _z, 0, 1, _numserie, _numFactura, _N, _conn);                                
                            }
                            else
                            {
                                MessageBox.Show("No existen Pagos registrados, verifique que el importe sea mayor a cero. Por favor revise el comprobante.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No existen Items registrados. Por favor revise el comprobante.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
                else
                    MessageBox.Show("La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Monitorear()
        {

            watcher.SynchronizingObject = this;
            watcher.Path = edtDirectorioMonitoreo.Text;
            watcher.NotifyFilter = NotifyFilters.CreationTime |
                        NotifyFilters.LastAccess |
                        NotifyFilters.LastWrite |
                        NotifyFilters.FileName;
            watcher.Filter = "*.xml";
            watcher.Created += new FileSystemEventHandler(OnCreated);
            watcher.EnableRaisingEvents = true;
        }

        private void OnCreated(object sender, FileSystemEventArgs e)
        {
            ImprimirDocumento(e.Name);
        }

        private void BtnDirectorioMonitoreo_Click(object sender, EventArgs e)
        {
            if (brwDirectorioMonitoreo.ShowDialog() == DialogResult.OK)
            {
                edtDirectorioMonitoreo.Text = brwDirectorioMonitoreo.SelectedPath;
            }

        }

        private void BtnIniciarMonitoreo_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(edtDirectorioMonitoreo.Text))
            {
                MessageBox.Show("Debe selecionar un directorio válido");
            }
            else
            {
                log(String.Format("Monitoreando {0}", edtDirectorioMonitoreo.Text));
                // memLog.AppendText(String.Format("[{0}] Monitoreando: {1}{2}", DateTime.Now.ToShortTimeString(), edtDirectorioMonitoreo.Text, Environment.NewLine));
                btnIniciarMonitoreo.Enabled = false;
                btnDetenerMonitoreo.Enabled = true;
                Monitorear();
            }

        }

        private void BtnDetenerMonitoreo_Click(object sender, EventArgs e)
        {
            log("Monitoreo detenido");
            // memLog.AppendText(String.Format("[{0}] Monitoreo detenido: {1} {2}", DateTime.Now.ToShortTimeString(), edtDirectorioMonitoreo.Text, Environment.NewLine));
            btnIniciarMonitoreo.Enabled = true;
            btnDetenerMonitoreo.Enabled = false;
            watcher.EnableRaisingEvents = false;

        }

        private void BtnCierreZ_Click(object sender, EventArgs e)
        {
            string op1;
            string op2;
            string op3;
            MessageBox.Show(Hasar1raGen.Cierres.ImprimirCierreZ(out  op1,out  op2,out  op3));
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            pnlAyuda.Top = 6;
            pnlAyuda.Left = 3;
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            edtDirectorioMonitoreo.Text = Path.GetDirectoryName(path);

            memLog.Clear();
            log("Aplicación Iniciada");
            // memLog.AppendText(String.Format("[{0}] Aplicacion iniciada {1}", DateTime.Now.ToShortTimeString(), Environment.NewLine));
        }

        private void btnLeerXML_Click_1(object sender, EventArgs e)
        {
            dlgOpenXML.InitialDirectory = edtDirectorioMonitoreo.Text;
            dlgOpenXML.Filter = "Archivo XML|*.xml";
            if (dlgOpenXML.ShowDialog() == DialogResult.OK)
            {
                ImprimirDocumento(dlgOpenXML.FileName);
            }

        }

        private void log(string msj)
        {
            try
            {
                memLog.AppendText(String.Format("[{0} {1}] {2} {3}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), msj, Environment.NewLine));
            }
            catch (Exception )
            {
            }

        }

        private void BtnCierreZ_Click_1(object sender, EventArgs e)
        {
            try
            {
                string testData = File.ReadAllText(@".\Config1raGeneracion.xml");
                XmlSerializer serializer = new XmlSerializer(typeof(Hasar1raGen.Config1raG));
                TextReader reader = new StringReader(testData);
                config = (Hasar1raGen.Config1raG)serializer.Deserialize(reader);
            }
            catch (InvalidOperationException )
            {
                MessageBox.Show("Error en el la estructura XML del archivo de configuración");
                return;
            }
            catch (Exception )
            {
                MessageBox.Show("Error leyendo el archivo: de  configuración");
                return;
            }

            

            Hasar1raGen.Clases.ModelosImpresoras.Modelos x = 0;
            Enum.TryParse("Active", out x);
            Hasar1raGen.Acciones.baudios = int.Parse(config.Impresora.Baudios);
            Hasar1raGen.Acciones.modelo = x;
            Hasar1raGen.Acciones.puerto = int.Parse(config.Impresora.Puerto); ;


            try
            {
                string op1;
                string op2;
                string op3;
                Hasar1raGen.Cierres.ImprimirCierreZ(out op1, out op2, out  op3);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            log("CierreZ generado");
        }

        private void fileSystemWatcher1_Created(object sender, FileSystemEventArgs e)
        {
            log(String.Format("Se ha detectado un nuevo archivo: {0}", e.Name));
            ImprimirDocumento(e.Name);
        }

        private void btnCerrarAyuda_Click(object sender, EventArgs e)
        {
            pnlAyuda.Visible = false;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            pnlAyuda.Visible = true;
            pnlAyuda.Top = 12;
            pnlAyuda.Left = 12;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            ImprimirDocumento(@".\data.xml", edtSerie.Text, edtNumero.Text, cmbN.Text);
        }

        private void btnImprimirXML_Click(object sender, EventArgs e)
        {
            ImprimirDocumento(@".\data.xml");
        }
    }
}
