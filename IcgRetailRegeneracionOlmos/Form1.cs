﻿using Hasar1raGen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace IcgRetailRegeneracionOlmos
{
    public partial class Form1 : Form
    {
        string _server = "";
        string _database = "";
        string _user = "";
        //string _codVendedor = "";
        //string _tipodoc = "";
        //string _serie = "";
        //string _numero = "";
        //string _n = "";
        //int _tipo = 12;
        //int _accion = 0;
        //string _textoRegalo1 = "";
        //string _textoRegalo2 = "";
        //string _textoRegalo3 = "";
        //string _textoAjuste = "";
        //string _caja = "";
        //string _terminal = "";
        //string _tef = "0";
        //string _ptoVtaManual = "";
        //string _nroComprobante = "";
        //string _pathCaballito = "";
        string _pathOlmos = "";
        int _nroClienteOlmos = 0;
        int _nroPosOlmos = 0;
        int _nroRubroOlmos = 0;
        Int64 _cuit = 0;
        string _procesoOlmos = "";

        public Form1()
        {
            InitializeComponent();
            //Titulo 
            this.Text = this.Text + " - Retail - V." + Application.ProductVersion;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists("Config1raGeneracion.xml"))
            {
                XmlDocument xConfig = new XmlDocument();
                xConfig.Load("Config1raGeneracion.xml");

                XmlNodeList _Listdb = xConfig.SelectNodes("/config/bd");
                foreach (XmlNode xn in _Listdb)
                {
                    _server = xn["server"].InnerText;
                    _user = xn["user"].InnerText;
                    _database = xn["database"].InnerText;
                }

                XmlNodeList _ListOlmos = xConfig.SelectNodes("/config/ShoppingOlmos");
                foreach (XmlNode xn in _ListOlmos)
                {
                    _pathOlmos = xn["PathSalida"].InnerText;
                    _nroClienteOlmos = Convert.ToInt32(xn["NroCliente"].InnerText);
                    _nroPosOlmos = Convert.ToInt32(xn["NroPos"].InnerText);
                    _nroRubroOlmos = Convert.ToInt32(xn["Rubro"].InnerText);
                    _cuit = Convert.ToInt64(xn["Cuit"].InnerText);
                    _procesoOlmos = xn["ProcesoOlmos"].InnerText;
                }
            }
            else
            {
                MessageBox.Show("No se encuentra el archivo de configuración. Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                this.Close();
            }
        }

        private bool ValidoKeySoftware(string keyIcg)
        {
            string _key = IcgVarios.NumerosSerie.GetCPUId();

            string _cod = IcgVarios.Licencia.GenerarLicenciaICG(_key);

            if (keyIcg == _cod)
                return true;
            else
                return false;

        }

        private void btnRegenerar_Click(object sender, EventArgs e)
        {
            //Recupero los datos de las fechas.
            DateTime _desde = dateTimePicker1.Value;
            DateTime _hasta = dateTimePicker2.Value;
            //string _TipoComprobante;
            string _nroComprobante;
            //string _tipoCompronateCab;
            //string _path = String.IsNullOrEmpty(_infoIrsa.pathSalida) ? _pathCaballito : _infoIrsa.pathSalida;
            // Armamos el stringConnection.
            string strConnection = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=" + _user + ";Password=masterkey;";

            try
            {
                using (SqlConnection _conexion = new SqlConnection(strConnection))
                {
                    //Abrimos la conexion.
                    _conexion.Open();
                    //Recupero la lista de ticketsl
                    List<Tickets> _lst = Irsa.GetTikets(_desde, _hasta, _conexion);
                    //Recorro la lista.
                    foreach (Tickets tck in _lst)
                    {
                        Cabecera _cabecera = Cabecera.GetCabecera(tck.NumSerie, tck.NumFactura, tck.N, _conexion);
                        List<Pagos> _pagos = Pagos.GetPagos(tck.NumSerie, tck.NumFactura, tck.N, _conexion);
                        List<Items> _items = Items.GetItems(_cabecera.numserie, _cabecera.n, _cabecera.numalbaran, _conexion);
                        _nroComprobante = tck.SerieFiscal1.PadLeft(4, '0') + "-" + tck.NumeroFiscal.ToString().PadLeft(8, '0');
                        decimal _cantidad = _items.Sum(i => i._cantidad);
                        ShoppingOlmos.LanzarShoppingOlmos(_cabecera, _pagos, _pathOlmos, _nroComprobante, _nroClienteOlmos,
                            _nroPosOlmos, _nroRubroOlmos, Convert.ToInt32(_cantidad), _cuit, _procesoOlmos, false, _conexion);

                    }
                }

                MessageBox.Show("El proceso termino correctamente. El archivo se encuentra en C:\\ICG\\OLMOSLOG\\icgOlmos.txt",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error: " + ex.Message + Environment.NewLine + ". Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
