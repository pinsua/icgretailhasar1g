﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Hasar1raGen;

namespace IcgRetailRegeneracionIrsa
{
    public partial class frmMain : Form
    {
        public string _server;
        public string _user;
        public string _catalog;
        public string _N = "B";
        public string _numfiscal;
        public string _textoRegalo1;
        public string _textoRegalo2;
        public string _textoRegalo3;
        public string _textoAjuste;
        public string _caja;
        public string _terminal;
        public string strConnection;
        public string _keyIcg = "";
        public bool _KeyIsOk;
        public string _pathCaballito = "";
        public Hasar1raGen.InfoIrsa _infoIrsa = new Hasar1raGen.InfoIrsa();

        public frmMain()
        {
            InitializeComponent();
            //Titulo 
            this.Text = this.Text + " - Retail - V." + Application.ProductVersion;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            if (File.Exists("Config1raGeneracion.xml"))
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("Config1raGeneracion.xml");

                XmlNodeList _List1 = xDoc.SelectNodes("/config");
                foreach (XmlNode xn in _List1)
                {

                    //_ip = xn["ip"].InnerText;
                    _caja = xn["caja"].InnerText;
                    _textoRegalo1 = xn["tktregalo1"].InnerText;
                    _textoRegalo2 = xn["tktregalo2"].InnerText;
                    _textoRegalo3 = xn["tktregalo3"].InnerText;
                    _textoAjuste = xn["textoajuste"].InnerText;
                    _terminal = xn["terminal"].InnerText;
                    //_password = xn["super"].InnerText;
                    //_hasarLog = Convert.ToBoolean(xn["hasarlog"].InnerText);
                    _keyIcg = xn["keyICG"].InnerText;
                }

                XmlNodeList _Listdb = xDoc.SelectNodes("/config/bd");
                foreach (XmlNode xn in _Listdb)
                {
                    _server = xn["server"].InnerText;
                    _user = xn["user"].InnerText;
                    _catalog = xn["database"].InnerText;
                }

                XmlNodeList _ListIrsa = xDoc.SelectNodes("/config/Irsa");
                foreach (XmlNode xn in _ListIrsa)
                {
                    _infoIrsa.contrato = xn["Contrato"].InnerText;
                    _infoIrsa.local = xn["Local"].InnerText;
                    _infoIrsa.pathSalida = xn["PathSalida"].InnerText;
                    _infoIrsa.pos = xn["Pos"].InnerText;
                    _infoIrsa.rubro = xn["Rubro"].InnerText;
                }

                XmlNodeList _ListCaballito = xDoc.SelectNodes("/config/ShoppingCaballito");
                foreach (XmlNode xn in _ListCaballito)
                {
                    _pathCaballito = xn["PathSalida"].InnerText;
                }

                // Armamos el stringConnection.
                strConnection = "Data Source=" + _server + ";Initial Catalog=" + _catalog + ";User Id=" + _user + ";Password=masterkey;";

                // Validamos el KeySoftware.
                _KeyIsOk = ValidoKeySoftware(_keyIcg);       
            }
            else
            {
                MessageBox.Show("No se encuentra el archivo de configuración. Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                this.Close();
            }
        }

        private bool ValidoKeySoftware(string keyIcg)
        {
            string _key = IcgVarios.NumerosSerie.GetCPUId();

            string _cod = IcgVarios.Licencia.GenerarLicenciaICG(_key);

            if (keyIcg == _cod)
                return true;
            else
                return false;

        }

        private void btnRegenerar_Click(object sender, EventArgs e)
        {
            //Recupero los datos de las fechas.
            DateTime _desde = dateTimePicker1.Value;
            DateTime _hasta = dateTimePicker2.Value;
            string _TipoComprobante;
            string _NroComprobante;
            string _tipoCompronateCab;
            string _path = String.IsNullOrEmpty(_infoIrsa.pathSalida) ? _pathCaballito : _infoIrsa.pathSalida;
            try
            {
                using (SqlConnection _conexion = new SqlConnection(strConnection))
                {
                    //Abrimos la conexion.
                    _conexion.Open();
                    //Recupero la lista de ticketsl
                    List<Tickets> _lst = Irsa.GetTikets(_desde, _hasta, _conexion);
                    //Recorro la lista.
                    foreach(Tickets tck in _lst)
                    {
                        _TipoComprobante = "";
                        _tipoCompronateCab = "";
                        Cabecera _cabecera = Cabecera.GetCabecera(tck.NumSerie, tck.NumFactura, tck.N, _conexion);
                        List<Pagos> _pagos = Pagos.GetPagos(tck.NumSerie, tck.NumFactura, tck.N, _conexion);
                        _NroComprobante = tck.SerieFiscal1.PadLeft(4, '0') +"-"+ tck.NumeroFiscal.ToString().PadLeft(8, '0');
                        //Vemos el tipo de comprobante.
                        switch (_cabecera.descripcionticket.Substring(0, 3))
                        {
                            case "003":
                            case "008":
                                {
                                    _TipoComprobante = "C";
                                    _tipoCompronateCab = "C";
                                    break;
                                }
                            case "001":
                            case "006":
                                {
                                    _TipoComprobante = "D";
                                    _tipoCompronateCab = "V";
                                    break;
                                }
                        }
                        //Vemos si vamos por IRSA.
                        if(!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                            Irsa.LanzarTrancomp(_cabecera, _pagos, _infoIrsa, _TipoComprobante, _NroComprobante, _conexion);

                        //Vemos si vamos por Caballito.
                        if (!String.IsNullOrEmpty(_pathCaballito))
                            ShoppingCaballito.LanzarShoppingCaballito(_cabecera, _pagos, _pathCaballito, _tipoCompronateCab, _NroComprobante, _conexion);

                    }
                }

                MessageBox.Show("El proceso termino correctamente. El archivo se encuentra en " + _path,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

            }
            catch(Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error: " + ex.Message + Environment.NewLine + ". Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
