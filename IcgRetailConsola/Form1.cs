﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Xml;

namespace IcgRetailConsolaH2G
{
    public partial class Form1 : Form
    {
        //public string _ip;
        public string _server;
        public string _user;
        public string _catalog;
        public int _puerto = 0;
        public int _baudios = 0;
        public Hasar1raGen.Clases.ModelosImpresoras.Modelos _modelo = 0;

        public SqlConnection _conn = new SqlConnection();
        public string _N = "B";
        public string _numfiscal;
        public string _textoRegalo1;
        public string _textoRegalo2;
        public string _textoRegalo3;
        public string _textoAjuste;
        public string _caja;
        public string _terminal;
        public int _tipo = 12;
        public int _accion = 1;
        public string strConnection;
        public static string _password;
        public string _keyIcg = "";
        public bool _KeyIsOk;
        public string _pathCaballito = "";
        string _pathOlmos = "";
        int _nroClienteOlmos = 0;
        int _nroPosOlmos = 0;
        int _nroRubroOlmos = 0;
        Int64 _cuit = 0;
        string _procesoOlmos = "";

        public Hasar1raGen.InfoIrsa _infoIrsa = new Hasar1raGen.InfoIrsa();

        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        public Form1()
        {
            InitializeComponent();

            this.Text = this.Text + " Consola Retail - V." + Application.ProductVersion;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists("Config1raGeneracion.xml"))
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("Config1raGeneracion.xml");

                XmlNodeList _List1 = xDoc.SelectNodes("/config");
                foreach (XmlNode xn in _List1)
                {
                    _caja = xn["caja"].InnerText;
                    _textoRegalo1 = xn["tktregalo1"].InnerText;
                    _textoRegalo2 = xn["tktregalo2"].InnerText;
                    _textoRegalo3 = xn["tktregalo3"].InnerText;
                    _textoAjuste = xn["textoajuste"].InnerText;
                    //_terminal = xn["terminal"].InnerText;
                    _keyIcg = xn["keyICG"].InnerText;
                }

                XmlNodeList _Listdb = xDoc.SelectNodes("/config/bd");
                foreach (XmlNode xn in _Listdb)
                {
                    _server = xn["server"].InnerText;
                    _user = xn["user"].InnerText;
                    _catalog = xn["database"].InnerText;
                }

                XmlNodeList _ListIrsa = xDoc.SelectNodes("/config/Irsa");
                foreach (XmlNode xn in _ListIrsa)
                {
                    _infoIrsa.contrato = xn["Contrato"].InnerText;
                    _infoIrsa.local = xn["Local"].InnerText;
                    _infoIrsa.pathSalida = xn["PathSalida"].InnerText;
                    _infoIrsa.pos = xn["Pos"].InnerText;
                    _infoIrsa.rubro = xn["Rubro"].InnerText;
                }

                XmlNodeList _ListCaballito = xDoc.SelectNodes("/config/ShoppingCaballito");
                foreach (XmlNode xn in _ListIrsa)
                {
                    _pathCaballito = xn["PathSalida"].InnerText;
                }

                XmlNodeList _ListOlmos = xDoc.SelectNodes("/config/ShoppingOlmos");
                foreach (XmlNode xn in _ListOlmos)
                {
                    _pathOlmos = xn["PathSalida"].InnerText;
                    _nroClienteOlmos = Convert.ToInt32(xn["NroCliente"].InnerText);
                    _nroPosOlmos = Convert.ToInt32(xn["NroPos"].InnerText);
                    _nroRubroOlmos = Convert.ToInt32(xn["Rubro"].InnerText);
                    _cuit = Convert.ToInt64(xn["Cuit"].InnerText);
                    _procesoOlmos = xn["ProcesoOlmos"].InnerText;
                }

                XmlNodeList _ListImpresora = xDoc.SelectNodes("/config/impresora");
                foreach (XmlNode xn in _ListImpresora)
                {
                    Hasar1raGen.Clases.ModelosImpresoras.Modelos x = 0;

                    Enum.TryParse(xn["modelo"].InnerText, out x);

                    _modelo =  x;
                    _puerto = Convert.ToInt32(xn["puerto"].InnerText);
                    _baudios = Convert.ToInt32(xn["baudios"].InnerText);

                    Hasar1raGen.Acciones.baudios = _baudios;
                    Hasar1raGen.Acciones.modelo = _modelo;
                    Hasar1raGen.Acciones.puerto = _puerto;
                }

                // Armamos el stringConnection.
                strConnection = "Data Source=" + _server + ";Initial Catalog=" + _catalog + ";User Id=" + _user + ";Password=masterkey;";

                // Validamos el KeySoftware.
                _KeyIsOk = ValidoKeySoftware(_keyIcg);

                _terminal = Environment.MachineName;

                // Conectamos.
                _conn.ConnectionString = strConnection;

                try
                {
                    _conn.Open();

                    GetDataset(_conn);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Se produjo el siguiente error: " + ex.Message + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
            else
            {
                DeshabilitarBotones();
                MessageBox.Show("No se encuentra el archivo de configuración. Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private bool ValidoKeySoftware(string keyIcg)
        {
            string _key = IcgVarios.LicenciaIcg.Value();

            string _cod = IcgVarios.NuevaLicencia.Value(_key);

            if (keyIcg == _cod)
                return true;
            else
                return false;

        }

        /// <summary>
        /// Encrypts the string.
        /// </summary>
        /// <param name="clearText">The clear text.</param>
        /// <param name="Key">The key.</param>
        /// <param name="IV">The IV.</param>
        /// <returns></returns>
        private static byte[] EncryptString(byte[] clearText, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = Key;
            alg.IV = IV;
            CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(clearText, 0, clearText.Length);
            cs.Close();
            byte[] encryptedData = ms.ToArray();
            return encryptedData;
        }

        private void DeshabilitarBotones()
        {
            // Desabilito los botones.
            btImprimir.Enabled = false;
            btReimprimir.Enabled = false;
            btTicketRegalo.Enabled = false;
        }

        private void btExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_conn.State == ConnectionState.Open)
                _conn.Close();
        }

        #region Tickets
        private void GetDataset(SqlConnection _con)
        {
            string _sql; 

            if (rbSinImprimir.Checked)
            {
                _sql = "SELECT ALBVENTACAB.NUMSERIE, ALBVENTACAB.NUMALBARAN, ALBVENTACAB.N, ALBVENTACAB.NUMFAC as NUMFACTURA," +
                "ALBVENTACAB.TOTALBRUTO,  ALBVENTACAB.TOTALNETO, CLIENTES.NOMBRECLIENTE, FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.SERIEFISCAL2, " +
                "FACTURASVENTASERIESRESOL.NUMEROFISCAL, TIPOSDOC.DESCRIPCION, ALBVENTACAB.FECHA, ALBVENTACAB.Z " +
                "FROM FACTURASVENTASERIESRESOL RIGHT JOIN ALBVENTACAB on FACTURASVENTASERIESRESOL.NUMSERIE = ALBVENTACAB.NUMSERIE AND " +
                "FACTURASVENTASERIESRESOL.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTASERIESRESOL.N = ALBVENTACAB.N " +
                "INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                "WHERE (FACTURASVENTASERIESRESOL.NUMEROFISCAL is null or FACTURASVENTASERIESRESOL.NUMEROFISCAL = '') " +
                //"AND ALBVENTACAB.TIQUET = 'T' AND  ALBVENTACAB.FECHA = @date AND ALBVENTACAB.N = @N AND ALBVENTACAB.CAJA = @caja " +
                "AND ALBVENTACAB.NUMFAC > -1 AND  ALBVENTACAB.FECHA = @date AND ALBVENTACAB.N = @N AND ALBVENTACAB.CAJA = @caja " +
                "ORDER BY ALBVENTACAB.FECHA";
            }
            else
            {
                _sql = "SELECT ALBVENTACAB.NUMSERIE, ALBVENTACAB.NUMALBARAN, ALBVENTACAB.N, ALBVENTACAB.NUMFAC as NUMFACTURA," +
                "ALBVENTACAB.TOTALBRUTO,  ALBVENTACAB.TOTALNETO, CLIENTES.NOMBRECLIENTE, FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.SERIEFISCAL2, " +
                "FACTURASVENTASERIESRESOL.NUMEROFISCAL, TIPOSDOC.DESCRIPCION, ALBVENTACAB.FECHA, ALBVENTACAB.Z " +
                "FROM FACTURASVENTASERIESRESOL RIGHT JOIN ALBVENTACAB on FACTURASVENTASERIESRESOL.NUMSERIE = ALBVENTACAB.NUMSERIE AND " +
                "FACTURASVENTASERIESRESOL.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTASERIESRESOL.N = ALBVENTACAB.N " +
                "INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                "WHERE (FACTURASVENTASERIESRESOL.NUMEROFISCAL is not null or FACTURASVENTASERIESRESOL.NUMEROFISCAL != '') " +
                //"AND ALBVENTACAB.TIQUET = 'T' AND ALBVENTACAB.FECHA = @date AND ALBVENTACAB.N = @N AND ALBVENTACAB.CAJA = @caja " +
                "AND ALBVENTACAB.NUMFAC > -1 AND ALBVENTACAB.FECHA = @date AND ALBVENTACAB.N = @N AND ALBVENTACAB.CAJA = @caja " +
                "ORDER BY ALBVENTACAB.FECHA";
            }

            // Limpio el dataset
            dtsImpresiones.Tables["Impresiones"].Clear();

            using (SqlCommand _cmd = new SqlCommand(_sql, _con))
            {
                _cmd.Parameters.AddWithValue("@date", dtpSeleccion.Value.Date);
                _cmd.Parameters.AddWithValue("@N", _N);
                _cmd.Parameters.AddWithValue("@caja", _caja);

                using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                {
                    _sda.Fill(dtsImpresiones, "Impresiones");
                }
            }
        }

        private void dtpSeleccion_ValueChanged(object sender, EventArgs e)
        {
            GetDataset(_conn);
        }

        private void grImpresiones_SelectionChanged(object sender, EventArgs e)
        {
            if (grImpresiones.RowCount > 0)
            {
                if (grImpresiones.SelectedRows.Count > 0)
                {
                    // object _numfiscal = grImpresiones.SelectedRows[0].Cells["NUMEROFISCAL"].Value;
                    _numfiscal = grImpresiones.SelectedRows[0].Cells[3].Value.ToString();
                    if (String.IsNullOrEmpty(_numfiscal))
                    {
                        btReimprimir.Enabled = false;
                        btTicketRegalo.Enabled = false;
                        btImprimir.Enabled = true;
                    }
                    else
                    {
                        if (grImpresiones.SelectedRows[0].Cells[4].Value.ToString().ToUpper().Contains("FACTURAS")
                            || grImpresiones.SelectedRows[0].Cells[4].Value.ToString().ToUpper().Contains("TIQUET"))
                        {
                            btImprimir.Enabled = false;
                            btReimprimir.Enabled = true;
                            btTicketRegalo.Enabled = true;
                        }
                        else
                        {
                            btImprimir.Enabled = false;
                            btReimprimir.Enabled = true;
                            btTicketRegalo.Enabled = false;
                        }
                    }
                }
            }
        }
        
        private void btReimprimir_Click(object sender, EventArgs e)
        {

        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                if (grImpresiones.RowCount > 0)
                {
                    if (grImpresiones.SelectedRows.Count > 0)
                    {
                        try
                        {
                            string _numserie = grImpresiones.SelectedRows[0].Cells[0].Value.ToString();
                            string _N = grImpresiones.SelectedRows[0].Cells[8].Value.ToString();
                            string _numFactura = grImpresiones.SelectedRows[0].Cells[1].Value.ToString();
                            string _ptoVta = grImpresiones.SelectedRows[0].Cells[2].Value.ToString();
                            string _nroFiscal = grImpresiones.SelectedRows[0].Cells[3].Value.ToString();
                            string _compro = _ptoVta + "-" + _nroFiscal.PadLeft(8, '0');
                            string _z = grImpresiones.SelectedRows[0].Cells[12].Value.ToString();
                            string _nroComprobante;

                            // Recuperamos los datos de la cabecera.
                            Hasar1raGen.Cabecera _cab = Hasar1raGen.Cabecera.GetCabecera(_numserie, Convert.ToInt32(_numFactura), _N, _conn);
                            List<Hasar1raGen.Items> _items = Hasar1raGen.Items.GetItems(_cab.numserie, _cab.n, _cab.numalbaran, _conn);
                            List<Hasar1raGen.OtrosTributos> _oTributos = Hasar1raGen.OtrosTributos.GetOtrosTributos(_numserie, _N, Convert.ToInt32(_numFactura), _conn);
                            List<Hasar1raGen.Recargos> _recargos = Hasar1raGen.Recargos.GetRecargos(_numserie, _N, Convert.ToInt32(_numFactura), _conn);

                            if (_items.Count > 0)
                            {
                                List<Hasar1raGen.Pagos> _pagos = Hasar1raGen.Pagos.GetPagos(_cab.numserie, _cab.numfac, _cab.n, _conn);
                                List<Hasar1raGen.Items> _lstItems = Hasar1raGen.Items.GetItems(_numserie, _N, _cab.numalbaran, _conn);

                                if (_pagos.Count > 0)
                                {
                                    bool _cuitOK = true;
                                    bool _rSocialOK = true;
                                    bool _hasChange = false;

                                    // Vemos si debemos validar el cuit.
                                    if (_cab.regfacturacioncliente != 4)
                                    {

                                        int _digitoValidador = Hasar1raGen.FuncionesVarias.CalcularDigitoCuit(_cab.nrodoccliente.Replace("-", ""));
                                        int _digitorecibido = Convert.ToInt16(_cab.nrodoccliente.Substring(_cab.nrodoccliente.Length - 1));
                                        if (_digitorecibido == _digitoValidador)
                                            _cuitOK = true;
                                        else
                                        {
                                            frmCuit _frm = new frmCuit();
                                            _frm.ShowDialog(this);
                                            if (String.IsNullOrEmpty(_frm._cuit))
                                                _cuitOK = false;
                                            else
                                            {
                                                _cab.nrodoccliente = _frm._cuit;
                                                _cuitOK = true;
                                            }
                                            _frm.Dispose();
                                            _hasChange = true;
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(_cab.nrodoccliente))
                                            _cab.nrodoccliente = "11111111";
                                    }
                                    // Validamos el campo direccion del cliente.
                                    if (String.IsNullOrEmpty(_cab.direccioncliente))
                                        _cab.direccioncliente = ".";
                                    // Validamos la Razon Social.
                                    if (String.IsNullOrEmpty(_cab.nombrecliente))
                                    {
                                        frmRazonSocial _frm = new frmRazonSocial();
                                        _frm.ShowDialog(this);
                                        if (String.IsNullOrEmpty(_frm._rSocial))
                                            _rSocialOK = false;
                                        else
                                        {
                                            _cab.nombrecliente = _frm._rSocial;
                                            _rSocialOK = true;
                                        }
                                        _frm.Dispose();
                                        _hasChange = true;
                                    }

                                    switch (_cab.descripcionticket.Substring(0, 3))
                                    {
                                        case "003":
                                        case "008":
                                            {
                                                if (_cuitOK)
                                                {
                                                    if (_rSocialOK)
                                                    {
                                                        // Busco documentos Asociados.
                                                        List<Hasar1raGen.DocumentoAsociado> _docAsoc = Hasar1raGen.DocumentoAsociado.GetDocumentoAsociado(_items[0]._abonoDeNumseie, _items[0]._abonoDe_N, _items[0]._abonoDeNumAlbaran, _conn);
                                                        bool _faltaPapel;
                                                        bool _necesitaCierreZ = false;

                                                        //Vemos si tenemos items con 
                                                        var _it = from element in _items where element._cantidad > 0 select element;
                                                        if (_it.Count() == 0)
                                                        {

                                                            string _respImpre = Hasar1raGen.Impresiones.ImprimirNotaCreditoAB(_cab, _items, _pagos, _oTributos,
                                                            _recargos, _docAsoc, _conn, out _faltaPapel, out _necesitaCierreZ, out _nroComprobante);

                                                            if (!String.IsNullOrEmpty(_respImpre))
                                                                MessageBox.Show(_respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                    MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                            else
                                                            {
                                                                // Grabamos en Rem_Transacciones para que la mande de nuevo a manager.
                                                                GraboRemTransacciones(_z, _numserie, _numFactura, _N);
                                                                //Interfaz Shopping IRSA
                                                                if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                                                    Hasar1raGen.Irsa.LanzarTrancomp(_cab, _pagos, _infoIrsa, "C", _nroComprobante, _conn);
                                                                //Interfaz Shopping Caballito
                                                                if (!String.IsNullOrEmpty(_pathCaballito))
                                                                    Hasar1raGen.ShoppingCaballito.LanzarShoppingCaballito(_cab, _pagos, _pathCaballito, "C", _nroComprobante, _conn);
                                                                //Interfaz Olmos
                                                                if (!String.IsNullOrEmpty(_pathOlmos))
                                                                {
                                                                    try
                                                                    {
                                                                        decimal _cantidad = _items.Sum(i => i._cantidad);
                                                                        Hasar1raGen.ShoppingOlmos.LanzarShoppingOlmos(_cab, _pagos, _pathOlmos, _nroComprobante, _nroClienteOlmos, _nroPosOlmos,
                                                                            _nroRubroOlmos, Convert.ToInt32(_cantidad), _cuit, _procesoOlmos, true, _conn);
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                                                                        {
                                                                            Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                                                                        }
                                                                        using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.log", true))
                                                                        {
                                                                            sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                                                                            sw.Flush();
                                                                        }
                                                                    }
                                                                }
                                                                //Informamos la falta de Papel en la Controladora fiscal.
                                                                if (_faltaPapel)
                                                                    MessageBox.Show("La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //MessageBox.Show(new Form() { TopMost = true }, "Las notas de credito no pueden tener unidades positivas.",
                                                            //      "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                            string _respImpre = Hasar1raGen.Impresiones.ImprimirNotaCreditoABMixta(_cab, _items, _pagos, _oTributos, _recargos, _docAsoc, _conn, out _faltaPapel, out _necesitaCierreZ, out _nroComprobante);
                                                            //_tipoComprobante = "C";

                                                            // NECESITA CIERRE Z? 
                                                            if (!String.IsNullOrEmpty(_respImpre))
                                                                MessageBox.Show(_respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                    MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                            else
                                                            {
                                                                // Grabamos en Rem_Transacciones para que la mande de nuevo a manager.
                                                                GraboRemTransacciones(_z, _numserie, _numFactura, _N);
                                                                //Interfaz Shopping IRSA
                                                                if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                                                    Hasar1raGen.Irsa.LanzarTrancomp(_cab, _pagos, _infoIrsa, "C", _nroComprobante, _conn);
                                                                //Interfaz Shopping Caballito
                                                                if (!String.IsNullOrEmpty(_pathCaballito))
                                                                    Hasar1raGen.ShoppingCaballito.LanzarShoppingCaballito(_cab, _pagos, _pathCaballito, "C", _nroComprobante, _conn);
                                                                //Interfaz Shoppping Caballito.
                                                                if (!String.IsNullOrEmpty(_pathOlmos))
                                                                {
                                                                    try
                                                                    {
                                                                        decimal _cantidad = _items.Sum(i => i._cantidad);
                                                                        Hasar1raGen.ShoppingOlmos.LanzarShoppingOlmos(_cab, _pagos, _pathOlmos, _nroComprobante, _nroClienteOlmos, _nroPosOlmos,
                                                                            _nroRubroOlmos, Convert.ToInt32(_cantidad), _cuit, _procesoOlmos, true, _conn);
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                                                                        {
                                                                            Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                                                                        }
                                                                        using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.log", true))
                                                                        {
                                                                            sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                                                                            sw.Flush();
                                                                        }
                                                                    }
                                                                }
                                                                //Informamos la falta de Papel en la Controladora fiscal.
                                                                if (_faltaPapel)
                                                                    MessageBox.Show("La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                            }
                                                        }
                                                    }
                                                    else
                                                        MessageBox.Show("El Cliente no posee Razon Social. Por favor ingresela y luego reimprimar el comprobante.",
                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                }
                                                else
                                                    MessageBox.Show("El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                break;
                                            }
                                        case "001":
                                        case "006":
                                            {
                                                if (_cuitOK)
                                                {
                                                    if (_rSocialOK)
                                                    {
                                                        // Impresiones.ImprimirFacturasAB(_cab, _items, _pagos, _oTributos, _recargos, _connection);
                                                        bool _faltaPapel;
                                                        bool _necesitaCierreZ = false;
                                                        
                                                        string _respImpre = Hasar1raGen.Impresiones.ImprimirFacturasAB(_cab, _items, _pagos, _oTributos,
                                                            _recargos, _conn, _textoAjuste, out _faltaPapel, out _necesitaCierreZ, out _nroComprobante);

                                                        string _msg = "Desea imprimir el ticket de regalo?.";
                                                        if (MessageBox.Show(_msg, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                        {
                                                            Hasar1raGen.Impresiones.ImprimirTicketRegalo(_cab, _items, _textoRegalo1, _textoRegalo2, _textoRegalo3, _nroComprobante, out _faltaPapel);
                                                            // puesto + nro 4 + 8
                                                        }

                                                        if (!String.IsNullOrEmpty(_respImpre))
                                                        {
                                                            MessageBox.Show(_respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        }
                                                        else
                                                        {
                                                            // Grabamos en Rem_Transacciones para que la mande de nuevo a manager.
                                                            GraboRemTransacciones(_z, _numserie, _numFactura, _N);
                                                            //Lanzamos el proceso que informa ventas para IRSA.
                                                            if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                                                Hasar1raGen.Irsa.LanzarTrancomp(_cab, _pagos, _infoIrsa, "D", _nroComprobante, _conn);
                                                            //Interfaz Shopping Caballito
                                                            if (!String.IsNullOrEmpty(_pathCaballito))
                                                                Hasar1raGen.ShoppingCaballito.LanzarShoppingCaballito(_cab, _pagos, _pathCaballito, "V", _nroComprobante, _conn);
                                                            //Interfaz Shoppping Caballito.
                                                            if (!String.IsNullOrEmpty(_pathOlmos))
                                                            {
                                                                try
                                                                {
                                                                    decimal _cantidad = _items.Sum(i => i._cantidad);
                                                                    Hasar1raGen.ShoppingOlmos.LanzarShoppingOlmos(_cab, _pagos, _pathOlmos, _nroComprobante, _nroClienteOlmos, _nroPosOlmos,
                                                                        _nroRubroOlmos, Convert.ToInt32(_cantidad), _cuit, _procesoOlmos, true, _conn);
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                                                                    {
                                                                        Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                                                                    }
                                                                    using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.log", true))
                                                                    {
                                                                        sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                                                                        sw.Flush();
                                                                    }
                                                                }
                                                            }
                                                            //Vemos si falta papel en la impresora.
                                                            if (_faltaPapel)
                                                                MessageBox.Show("La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        }
                                                    }
                                                    else
                                                        MessageBox.Show("El Cliente no posee Razon Social. Por favor ingresela y luego reimprimar el comprobante.",
                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                }
                                                else
                                                    MessageBox.Show("El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                break;
                                            }
                                        case "002":
                                        case "007":
                                            {
                                                // Notas de Debito.
                                                MessageBox.Show("Notas de Debito. Aún no estan configuradas.",
                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                break;
                                            }
                                        default:
                                            {
                                                MessageBox.Show("Tipo de Documento no configurado para su fiscalización.",
                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                break;
                                            }
                                    }
                                    // vemos si tenemos modificacion.
                                    if (_hasChange)
                                    {
                                        Hasar1raGen.Clientes.UpdateCliente(_cab.nombrecliente, _cab.nrodoccliente, _cab.codcliente, _conn);
                                        Hasar1raGen.RemTransacciones.GraboRemTransacciones(_terminal, _caja, _z, _tipo, _accion, "", _cab.codcliente.ToString(), _N, _conn);
                                    }
                                    // Enviamos los cambios a central.
                                    Hasar1raGen.RemTransacciones.GraboRemTransacciones(_terminal, _caja, _z, 0, 1, _numserie, _numFactura, _N, _conn);
                                    // Refrescamos al grid.
                                    GetDataset(_conn);
                                }
                                else
                                {
                                    MessageBox.Show("No existen Pagos registrados, verifique que el importe sea mayor a cero. Por favor revise el comprobante.",
                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                }
                            }
                            else
                            {
                                MessageBox.Show("No existen Items registrados. Por favor revise el comprobante.",
                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show("Se produjo el siguiente error: " + ex.Message,
                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
            }
            else
                MessageBox.Show("La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btTicketRegalo_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                if (grImpresiones.RowCount > 0)
                {
                    if (grImpresiones.SelectedRows.Count > 0)
                    {
                        if (grImpresiones.SelectedRows[0].Cells[4].Value.ToString().ToUpper().Contains("FACTURAS")
                            || grImpresiones.SelectedRows[0].Cells[4].Value.ToString().ToUpper().Contains("TIQUET"))
                        {
                            string _numserie = grImpresiones.SelectedRows[0].Cells[0].Value.ToString();
                            string _N = grImpresiones.SelectedRows[0].Cells[8].Value.ToString();
                            string _numFactura = grImpresiones.SelectedRows[0].Cells[1].Value.ToString();
                            string _ptoVta = grImpresiones.SelectedRows[0].Cells[2].Value.ToString();
                            string _nroFiscal = grImpresiones.SelectedRows[0].Cells[3].Value.ToString();
                            string _compro = _ptoVta + "-" + _nroFiscal.PadLeft(8, '0');
                            Hasar1raGen.Cabecera _cabecera = Hasar1raGen.Cabecera.GetCabecera(_numserie, Convert.ToInt32(_numFactura), _N, _conn);
                            List<Hasar1raGen.Items> _lstItems = Hasar1raGen.Items.GetItems(_numserie, _N, _cabecera.numalbaran, _conn);

                            bool _faltaPapel;
                            Hasar1raGen.Impresiones.ImprimirTicketRegalo(_cabecera, _lstItems, _textoRegalo1, _textoRegalo2, _textoRegalo3, 
                                _compro, out _faltaPapel);

                            if (_faltaPapel)
                                MessageBox.Show("La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                        else
                        {
                            MessageBox.Show("Los tickets de cambio solo se pueden imprimir sobre una factura A o B.",
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
            }
            else
                MessageBox.Show("La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void gbTipoB_CheckedChanged(object sender, EventArgs e)
        {
            if(rbTipoB.Checked)
                rbTipoN.Checked = false;
            // Cambio variable.
            _N = "B";
            // Cargo Dataset.
            GetDataset(_conn);

        }

        private void rbTipoN_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTipoN.Checked)
                rbTipoB.Checked = false;
            // Cambio Variable.
            _N = "N";
            // Cargo Dataset.
            GetDataset(_conn);
        }

        private void rbImpresas_CheckedChanged(object sender, EventArgs e)
        {
            if (rbImpresas.Checked)
                rbSinImprimir.Checked = false;

            GetDataset(_conn);
        }

        private void rbSinImprimir_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSinImprimir.Checked)
                rbImpresas.Checked = false;

            GetDataset(_conn);
        }
        #endregion

        private void GraboRemTransacciones(string _z, string _serie, string _numero, string _N)
        {
            string _sql = "INSERT INTO REM_TRANSACCIONES(TERMINAL, CAJA, CAJANUM, Z, TIPO, ACCION, SERIE, NUMERO, N, FECHA, HORA, FO, IDCENTRAL, TALLA, COLOR, CODIGOSTR) " +
                    "VALUES(@terminal, @caja, 0, @Z, 0, 1, @serie, @numero, @N, @date, @hora, 0, 1, '.', '.', '')";

            using (SqlCommand _cmd = new SqlCommand(_sql, _conn))
            {
                _cmd.Parameters.AddWithValue("@terminal", _terminal);
                _cmd.Parameters.AddWithValue("@caja", _caja);
                _cmd.Parameters.AddWithValue("@Z", Convert.ToInt32(_z));
                _cmd.Parameters.AddWithValue("@serie", _serie);
                _cmd.Parameters.AddWithValue("@numero", Convert.ToInt32(_numero));
                _cmd.Parameters.AddWithValue("@N", _N);
                _cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                _cmd.Parameters.AddWithValue("@hora", DateTime.Now.Date);

                int rta =_cmd.ExecuteNonQuery();
                
            }
        }

        #region Cierres
        private void btInvocar_Click(object sender, EventArgs e)
        {
            try
            {
                if (_KeyIsOk)
                {
                    bool _rtaComprobantesOk = false;

                    _rtaComprobantesOk = Hasar1raGen.FuncionesVarias.TengoComprobantesSinImprimmir(_conn, _caja);

                    if (!_rtaComprobantesOk)
                    {
                        Process p = Process.GetProcessesByName("FrontRetail").FirstOrDefault();
                        if (p != null)
                        {
                            IntPtr h = p.MainWindowHandle;
                            SetForegroundWindow(h);
                            SendKeys.SendWait("%(z)");
                            SendKeys.Flush();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Posee comprobantes sin fiscalizar. Por favor fiscalize o anule los comprobantes para poder realizar el cierre Z.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                }
                else
                    MessageBox.Show("La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error al intentar Realizar el Cierre. " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btReimpresionZ_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                frmReimpresion frm = new frmReimpresion(_caja, "Z","", strConnection);
                frm.ShowDialog(this);
                frm.Dispose();
            }
            else
                MessageBox.Show("La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btCierreX_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                DialogResult result = MessageBox.Show("Seguro que desea realizar un Cierre X?.", "ICG - Argentina",
                    MessageBoxButtons.YesNo);
                switch (result)
                {
                    case DialogResult.Yes:
                        {
                           
                            string _msj;

                            try
                            {
                                _msj = Hasar1raGen.Cierres.ImprimirCierreX();
                                MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                            }
                            catch (Exception ex)
                            {
                                Hasar1raGen.Acciones.Cancelar();
                                MessageBox.Show("Se produjo el siguiente Error : " + ex.Message, 
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            }
                            
                            break;
                        }
                    case DialogResult.No:
                        {
                            this.Text = "[Cancel]";
                            break;
                        }
                }
            }
            else
                MessageBox.Show("La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btCierreZ_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                frmCierreZ frm = new frmCierreZ("", strConnection, _caja, false);
                frm.ShowDialog(this);
                frm.Dispose();
            }
            else
                MessageBox.Show("La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btForzarZ_Click(object sender, EventArgs e)
        {
            string _msjCbte;

            if (_KeyIsOk)
            {
                // Vemos si tenemos comprobantes sin fiscalizar y armamos el MSJ.
                if (Hasar1raGen.FuncionesVarias.TengoComprobantesSinImprimmir(_conn, _caja))
                    _msjCbte = "POSEE COMPROBANTES SIN FISCALIZAR. " + Environment.NewLine + "Desea continuar con la impresión del Z fiscal?.";
                else
                    _msjCbte = "Desea continuar con la impresión del Z fiscal?.";

                if (MessageBox.Show(_msjCbte, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    // instanciamos el controlador Fiscal.
                    /*
                    hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                    hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                    hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
                    */
                    string _msj;

                    try
                    {

                        string NumeroZ;
                        string DF_CantidadEmitidos;
                        string DF_Total;
                        _msj = Hasar1raGen.Cierres.ImprimirCierreZ(out NumeroZ, out DF_CantidadEmitidos, out  DF_Total);
                        MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                        // Modificamos la tabla Arqueos.
                        string _dato = NumeroZ + ";" + DF_CantidadEmitidos  + ";" + DF_Total;

                        int _rta = Hasar1raGen.Arqueo.UpdateArqueosOK("Z", _dato, _conn);
                    }
                    catch (Exception ex)
                    {
                        Hasar1raGen.Acciones.Cancelar();
                        MessageBox.Show("Se produjo el siguiente Error : " + ex.Message, 
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                }
            }
            else
                MessageBox.Show("La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        #endregion

        private void btKey_Click(object sender, EventArgs e)
        {
            frmKeyDisplay _frm = new frmKeyDisplay();
            _frm.ShowDialog(this);
            _frm.Dispose();
           
        }
    }
}
