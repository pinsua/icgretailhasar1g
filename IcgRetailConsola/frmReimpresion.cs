﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace IcgRetailConsolaH2G
{
    public partial class frmReimpresion : Form
    {
        string _caja;
        string _tipo;
        string _ip;
        string _strConn;
        
        public SqlConnection _conn = new SqlConnection();

        // public static hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
         

        public frmReimpresion(string pcaja, string ptipo, string pIp, string pConnection)
        {
            InitializeComponent();
            _caja = pcaja;
            _tipo = ptipo;
            _ip = pIp;
            _strConn = pConnection;
        }

        private void frmReimpresion_Load(object sender, EventArgs e)
        {
            try
            {
                // Conectamos.
                _conn.ConnectionString = _strConn; 

                _conn.Open();

                GetArqueo(dtsReimpresion, "Reimpresion", "Z", _caja, _conn);

                if (grCierre.Rows.Count == 0)
                {
                    btCierreZ.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguente error: " + ex.Message + ". Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private static void GetArqueo(DataSet _dts, string _table, string _tipo, string _caja, SqlConnection _con)
        {
            string _sql = "SELECT FO, ARQUEO, CAJA, NUMERO, FECHA, CLEANCASHCONTROLCODE1 FROM ARQUEOS " +
                "WHERE ARQUEO = @tipo AND CAJA = @caja ORDER BY NUMERO";

            // Limpio el dataset
            _dts.Tables[_table].Clear();

            using (SqlCommand _cmd = new SqlCommand(_sql, _con))
            {
                _cmd.Parameters.AddWithValue("@tipo", _tipo);
                _cmd.Parameters.AddWithValue("@caja", _caja);

                using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                {
                    _sda.Fill(_dts, _table);
                }
            }

            foreach (DataRow dr in _dts.Tables[_table].Rows)
            {
                try
                {
                    string _dato = dr["CLEANCASHCONTROLCODE1"].ToString();

                    if (String.IsNullOrEmpty(_dato))
                        dr["NumeroHasar"] = 0;
                    else
                    {
                        dr["NumeroHasar"] = _dato.Split(';')[0];
                    }
                }
                catch
                {
                    dr["NumeroHasar"] = 0;
                }
            }
        }

        private void btCierreZ_Click(object sender, EventArgs e)
        {
            if (grCierre.SelectedRows.Count > 0)
            {
                // if (Hasar2daGen.FuncionesVarias.TengoComprobantesSinImprimmir(_conn, _caja))
                //{
                //    MessageBox.Show("No se puede realizar un Cierre Z porque existen comprobates sin fiscalizar.",
                //       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                //}
                // else
                //{
                // Recupero los datos del grid.
                string _fo = grCierre.SelectedRows[0].Cells[0].Value.ToString();
                string _arqueo = grCierre.SelectedRows[0].Cells[1].Value.ToString();
                string _fecha = grCierre.SelectedRows[0].Cells[3].Value.ToString();
                string _numero = grCierre.SelectedRows[0].Cells[4].Value.ToString();
                string _numeroHasar = grCierre.SelectedRows[0].Cells[5].Value.ToString();
                int _nroZeta = Convert.ToInt32(_numeroHasar);
                /*
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarAcumuladosCierreZeta _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarAcumuladosCierreZeta();
                hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
                */
                string _msj;

                try
                {
                    // Conectamos.
                    // Hasar2daGen.Impresiones.Conectar(hasar, false, _ip);
                    // hasar.conectar(_ip);
                    // ejecutamos el cierre Z.
                    // hasar.ReportarZetasPorNumeroZeta(_nroZeta, _nroZeta, hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteAuditoria.REPORTE_AUDITORIA_GLOBAL);
                    _msj = Hasar1raGen.Cierres.ImprimirZPorNumero(_nroZeta);

                }
                catch (Exception ex)
                {
                    Hasar1raGen.Acciones.Cancelar();
                    MessageBox.Show("Se produjo el siguiente Error : " + ex.Message,
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
                //}
                //// Arqueo.GetArqueo(dtsCierres, "Cierres", "Z", _Caja, _conn);
            }
            else
                MessageBox.Show("Debe seleccionar un Cierre de la tabla.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
