﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgRetailConsolaH2G
{
    public partial class frmKeyDisplay : Form
    {
        public frmKeyDisplay()
        {
            InitializeComponent();
        }

        private void frmKeyDisplay_Load(object sender, EventArgs e)
        {
            textBox1.Text = IcgVarios.LicenciaIcg.Value();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
