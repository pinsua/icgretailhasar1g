﻿namespace IcgRetailConsolaH2G
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtsImpresiones = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dtpSeleccion = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.gbAcciones = new System.Windows.Forms.GroupBox();
            this.btTicketRegalo = new System.Windows.Forms.Button();
            this.btImprimir = new System.Windows.Forms.Button();
            this.btReimprimir = new System.Windows.Forms.Button();
            this.btExit = new System.Windows.Forms.Button();
            this.grImpresiones = new System.Windows.Forms.DataGridView();
            this.numSerieDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nUMFACTURADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sERIEFISCAL1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nUMEROFISCALDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dESCRIPCIONDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nOMBRECLIENTEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tOTALBRUTODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tOTALNETODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sERIEFISCAL2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fECHADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nUMALBARANDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Z = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbTipo = new System.Windows.Forms.GroupBox();
            this.rbTipoN = new System.Windows.Forms.RadioButton();
            this.rbTipoB = new System.Windows.Forms.RadioButton();
            this.gbImpresas = new System.Windows.Forms.GroupBox();
            this.rbSinImprimir = new System.Windows.Forms.RadioButton();
            this.rbImpresas = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btInvocar = new System.Windows.Forms.Button();
            this.btCierreX = new System.Windows.Forms.Button();
            this.btForzarZ = new System.Windows.Forms.Button();
            this.btReimpresionZ = new System.Windows.Forms.Button();
            this.btCierreZ = new System.Windows.Forms.Button();
            this.btKey = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtsImpresiones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            this.gbAcciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grImpresiones)).BeginInit();
            this.gbTipo.SuspendLayout();
            this.gbImpresas.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtsImpresiones
            // 
            this.dtsImpresiones.DataSetName = "NewDataSet";
            this.dtsImpresiones.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13});
            this.dataTable1.TableName = "Impresiones";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "NumSerie";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "NUMALBARAN";
            this.dataColumn2.DataType = typeof(int);
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "N";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "NUMFACTURA";
            this.dataColumn4.DataType = typeof(int);
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "TOTALBRUTO";
            this.dataColumn5.DataType = typeof(double);
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "TOTALNETO";
            this.dataColumn6.DataType = typeof(double);
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "NOMBRECLIENTE";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "SERIEFISCAL1";
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "SERIEFISCAL2";
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "NUMEROFISCAL";
            this.dataColumn10.DataType = typeof(int);
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "DESCRIPCION";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "FECHA";
            this.dataColumn12.DataType = typeof(System.DateTime);
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "Z";
            this.dataColumn13.DataType = typeof(int);
            // 
            // dtpSeleccion
            // 
            this.dtpSeleccion.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpSeleccion.Location = new System.Drawing.Point(134, 290);
            this.dtpSeleccion.Name = "dtpSeleccion";
            this.dtpSeleccion.Size = new System.Drawing.Size(127, 20);
            this.dtpSeleccion.TabIndex = 1;
            this.dtpSeleccion.ValueChanged += new System.EventHandler(this.dtpSeleccion_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 293);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Fecha Facturación";
            // 
            // gbAcciones
            // 
            this.gbAcciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbAcciones.Controls.Add(this.btTicketRegalo);
            this.gbAcciones.Controls.Add(this.btImprimir);
            this.gbAcciones.Controls.Add(this.btReimprimir);
            this.gbAcciones.Location = new System.Drawing.Point(12, 334);
            this.gbAcciones.Name = "gbAcciones";
            this.gbAcciones.Size = new System.Drawing.Size(733, 58);
            this.gbAcciones.TabIndex = 3;
            this.gbAcciones.TabStop = false;
            this.gbAcciones.Text = "TICKETS";
            // 
            // btTicketRegalo
            // 
            this.btTicketRegalo.Location = new System.Drawing.Point(298, 19);
            this.btTicketRegalo.Name = "btTicketRegalo";
            this.btTicketRegalo.Size = new System.Drawing.Size(118, 23);
            this.btTicketRegalo.TabIndex = 2;
            this.btTicketRegalo.Text = "Ticket de Cambio";
            this.btTicketRegalo.UseVisualStyleBackColor = true;
            this.btTicketRegalo.Click += new System.EventHandler(this.btTicketRegalo_Click);
            // 
            // btImprimir
            // 
            this.btImprimir.Location = new System.Drawing.Point(149, 19);
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(118, 23);
            this.btImprimir.TabIndex = 1;
            this.btImprimir.Text = "Imprimir";
            this.btImprimir.UseVisualStyleBackColor = true;
            this.btImprimir.Click += new System.EventHandler(this.btImprimir_Click);
            // 
            // btReimprimir
            // 
            this.btReimprimir.Enabled = false;
            this.btReimprimir.Location = new System.Drawing.Point(6, 19);
            this.btReimprimir.Name = "btReimprimir";
            this.btReimprimir.Size = new System.Drawing.Size(118, 23);
            this.btReimprimir.TabIndex = 0;
            this.btReimprimir.Text = "Reimprimir";
            this.btReimprimir.UseVisualStyleBackColor = true;
            this.btReimprimir.Visible = false;
            this.btReimprimir.Click += new System.EventHandler(this.btReimprimir_Click);
            // 
            // btExit
            // 
            this.btExit.Location = new System.Drawing.Point(576, 468);
            this.btExit.Name = "btExit";
            this.btExit.Size = new System.Drawing.Size(117, 23);
            this.btExit.TabIndex = 3;
            this.btExit.Text = "Salir";
            this.btExit.UseVisualStyleBackColor = true;
            this.btExit.Click += new System.EventHandler(this.btExit_Click);
            // 
            // grImpresiones
            // 
            this.grImpresiones.AllowUserToAddRows = false;
            this.grImpresiones.AllowUserToDeleteRows = false;
            this.grImpresiones.AutoGenerateColumns = false;
            this.grImpresiones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grImpresiones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numSerieDataGridViewTextBoxColumn,
            this.nUMFACTURADataGridViewTextBoxColumn,
            this.sERIEFISCAL1DataGridViewTextBoxColumn,
            this.nUMEROFISCALDataGridViewTextBoxColumn,
            this.dESCRIPCIONDataGridViewTextBoxColumn,
            this.nOMBRECLIENTEDataGridViewTextBoxColumn,
            this.tOTALBRUTODataGridViewTextBoxColumn,
            this.tOTALNETODataGridViewTextBoxColumn,
            this.nDataGridViewTextBoxColumn,
            this.sERIEFISCAL2DataGridViewTextBoxColumn,
            this.fECHADataGridViewTextBoxColumn,
            this.nUMALBARANDataGridViewTextBoxColumn,
            this.Z});
            this.grImpresiones.DataMember = "Impresiones";
            this.grImpresiones.DataSource = this.dtsImpresiones;
            this.grImpresiones.Dock = System.Windows.Forms.DockStyle.Top;
            this.grImpresiones.Location = new System.Drawing.Point(0, 0);
            this.grImpresiones.MultiSelect = false;
            this.grImpresiones.Name = "grImpresiones";
            this.grImpresiones.ReadOnly = true;
            this.grImpresiones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grImpresiones.Size = new System.Drawing.Size(757, 268);
            this.grImpresiones.TabIndex = 4;
            this.grImpresiones.SelectionChanged += new System.EventHandler(this.grImpresiones_SelectionChanged);
            // 
            // numSerieDataGridViewTextBoxColumn
            // 
            this.numSerieDataGridViewTextBoxColumn.DataPropertyName = "NumSerie";
            this.numSerieDataGridViewTextBoxColumn.HeaderText = "Serie";
            this.numSerieDataGridViewTextBoxColumn.Name = "numSerieDataGridViewTextBoxColumn";
            this.numSerieDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nUMFACTURADataGridViewTextBoxColumn
            // 
            this.nUMFACTURADataGridViewTextBoxColumn.DataPropertyName = "NUMFACTURA";
            this.nUMFACTURADataGridViewTextBoxColumn.HeaderText = "Factura";
            this.nUMFACTURADataGridViewTextBoxColumn.Name = "nUMFACTURADataGridViewTextBoxColumn";
            this.nUMFACTURADataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sERIEFISCAL1DataGridViewTextBoxColumn
            // 
            this.sERIEFISCAL1DataGridViewTextBoxColumn.DataPropertyName = "SERIEFISCAL1";
            this.sERIEFISCAL1DataGridViewTextBoxColumn.HeaderText = "Pto. Vta.";
            this.sERIEFISCAL1DataGridViewTextBoxColumn.Name = "sERIEFISCAL1DataGridViewTextBoxColumn";
            this.sERIEFISCAL1DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nUMEROFISCALDataGridViewTextBoxColumn
            // 
            this.nUMEROFISCALDataGridViewTextBoxColumn.DataPropertyName = "NUMEROFISCAL";
            this.nUMEROFISCALDataGridViewTextBoxColumn.HeaderText = "Comprobante";
            this.nUMEROFISCALDataGridViewTextBoxColumn.Name = "nUMEROFISCALDataGridViewTextBoxColumn";
            this.nUMEROFISCALDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dESCRIPCIONDataGridViewTextBoxColumn
            // 
            this.dESCRIPCIONDataGridViewTextBoxColumn.DataPropertyName = "DESCRIPCION";
            this.dESCRIPCIONDataGridViewTextBoxColumn.HeaderText = "Tipo";
            this.dESCRIPCIONDataGridViewTextBoxColumn.Name = "dESCRIPCIONDataGridViewTextBoxColumn";
            this.dESCRIPCIONDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nOMBRECLIENTEDataGridViewTextBoxColumn
            // 
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.DataPropertyName = "NOMBRECLIENTE";
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.HeaderText = "Cliente";
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.Name = "nOMBRECLIENTEDataGridViewTextBoxColumn";
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tOTALBRUTODataGridViewTextBoxColumn
            // 
            this.tOTALBRUTODataGridViewTextBoxColumn.DataPropertyName = "TOTALBRUTO";
            this.tOTALBRUTODataGridViewTextBoxColumn.HeaderText = "Bruto";
            this.tOTALBRUTODataGridViewTextBoxColumn.Name = "tOTALBRUTODataGridViewTextBoxColumn";
            this.tOTALBRUTODataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tOTALNETODataGridViewTextBoxColumn
            // 
            this.tOTALNETODataGridViewTextBoxColumn.DataPropertyName = "TOTALNETO";
            this.tOTALNETODataGridViewTextBoxColumn.HeaderText = "Neto";
            this.tOTALNETODataGridViewTextBoxColumn.Name = "tOTALNETODataGridViewTextBoxColumn";
            this.tOTALNETODataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nDataGridViewTextBoxColumn
            // 
            this.nDataGridViewTextBoxColumn.DataPropertyName = "N";
            this.nDataGridViewTextBoxColumn.HeaderText = "N";
            this.nDataGridViewTextBoxColumn.Name = "nDataGridViewTextBoxColumn";
            this.nDataGridViewTextBoxColumn.ReadOnly = true;
            this.nDataGridViewTextBoxColumn.Visible = false;
            // 
            // sERIEFISCAL2DataGridViewTextBoxColumn
            // 
            this.sERIEFISCAL2DataGridViewTextBoxColumn.DataPropertyName = "SERIEFISCAL2";
            this.sERIEFISCAL2DataGridViewTextBoxColumn.HeaderText = "SERIEFISCAL2";
            this.sERIEFISCAL2DataGridViewTextBoxColumn.Name = "sERIEFISCAL2DataGridViewTextBoxColumn";
            this.sERIEFISCAL2DataGridViewTextBoxColumn.ReadOnly = true;
            this.sERIEFISCAL2DataGridViewTextBoxColumn.Visible = false;
            // 
            // fECHADataGridViewTextBoxColumn
            // 
            this.fECHADataGridViewTextBoxColumn.DataPropertyName = "FECHA";
            this.fECHADataGridViewTextBoxColumn.HeaderText = "FECHA";
            this.fECHADataGridViewTextBoxColumn.Name = "fECHADataGridViewTextBoxColumn";
            this.fECHADataGridViewTextBoxColumn.ReadOnly = true;
            this.fECHADataGridViewTextBoxColumn.Visible = false;
            // 
            // nUMALBARANDataGridViewTextBoxColumn
            // 
            this.nUMALBARANDataGridViewTextBoxColumn.DataPropertyName = "NUMALBARAN";
            this.nUMALBARANDataGridViewTextBoxColumn.HeaderText = "NUMALBARAN";
            this.nUMALBARANDataGridViewTextBoxColumn.Name = "nUMALBARANDataGridViewTextBoxColumn";
            this.nUMALBARANDataGridViewTextBoxColumn.ReadOnly = true;
            this.nUMALBARANDataGridViewTextBoxColumn.Visible = false;
            // 
            // Z
            // 
            this.Z.DataPropertyName = "Z";
            this.Z.HeaderText = "Z";
            this.Z.Name = "Z";
            this.Z.ReadOnly = true;
            this.Z.Visible = false;
            // 
            // gbTipo
            // 
            this.gbTipo.Controls.Add(this.rbTipoN);
            this.gbTipo.Controls.Add(this.rbTipoB);
            this.gbTipo.Location = new System.Drawing.Point(288, 274);
            this.gbTipo.Name = "gbTipo";
            this.gbTipo.Size = new System.Drawing.Size(138, 51);
            this.gbTipo.TabIndex = 5;
            this.gbTipo.TabStop = false;
            this.gbTipo.Text = "Tipo";
            // 
            // rbTipoN
            // 
            this.rbTipoN.AutoSize = true;
            this.rbTipoN.Location = new System.Drawing.Point(82, 17);
            this.rbTipoN.Name = "rbTipoN";
            this.rbTipoN.Size = new System.Drawing.Size(31, 17);
            this.rbTipoN.TabIndex = 1;
            this.rbTipoN.Text = "2";
            this.rbTipoN.UseVisualStyleBackColor = true;
            this.rbTipoN.CheckedChanged += new System.EventHandler(this.rbTipoN_CheckedChanged);
            // 
            // rbTipoB
            // 
            this.rbTipoB.AutoSize = true;
            this.rbTipoB.Checked = true;
            this.rbTipoB.Location = new System.Drawing.Point(18, 17);
            this.rbTipoB.Name = "rbTipoB";
            this.rbTipoB.Size = new System.Drawing.Size(31, 17);
            this.rbTipoB.TabIndex = 0;
            this.rbTipoB.TabStop = true;
            this.rbTipoB.Text = "1";
            this.rbTipoB.UseVisualStyleBackColor = true;
            this.rbTipoB.CheckedChanged += new System.EventHandler(this.gbTipoB_CheckedChanged);
            // 
            // gbImpresas
            // 
            this.gbImpresas.Controls.Add(this.rbSinImprimir);
            this.gbImpresas.Controls.Add(this.rbImpresas);
            this.gbImpresas.Location = new System.Drawing.Point(442, 274);
            this.gbImpresas.Name = "gbImpresas";
            this.gbImpresas.Size = new System.Drawing.Size(251, 51);
            this.gbImpresas.TabIndex = 6;
            this.gbImpresas.TabStop = false;
            this.gbImpresas.Text = "Impresas";
            // 
            // rbSinImprimir
            // 
            this.rbSinImprimir.AutoSize = true;
            this.rbSinImprimir.Checked = true;
            this.rbSinImprimir.Location = new System.Drawing.Point(89, 19);
            this.rbSinImprimir.Name = "rbSinImprimir";
            this.rbSinImprimir.Size = new System.Drawing.Size(78, 17);
            this.rbSinImprimir.TabIndex = 1;
            this.rbSinImprimir.TabStop = true;
            this.rbSinImprimir.Text = "Sin Imprimir";
            this.rbSinImprimir.UseVisualStyleBackColor = true;
            this.rbSinImprimir.CheckedChanged += new System.EventHandler(this.rbSinImprimir_CheckedChanged);
            // 
            // rbImpresas
            // 
            this.rbImpresas.AutoSize = true;
            this.rbImpresas.Location = new System.Drawing.Point(6, 19);
            this.rbImpresas.Name = "rbImpresas";
            this.rbImpresas.Size = new System.Drawing.Size(67, 17);
            this.rbImpresas.TabIndex = 0;
            this.rbImpresas.Text = "Impresas";
            this.rbImpresas.UseVisualStyleBackColor = true;
            this.rbImpresas.CheckedChanged += new System.EventHandler(this.rbImpresas_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btInvocar);
            this.groupBox1.Controls.Add(this.btCierreX);
            this.groupBox1.Controls.Add(this.btForzarZ);
            this.groupBox1.Controls.Add(this.btReimpresionZ);
            this.groupBox1.Controls.Add(this.btCierreZ);
            this.groupBox1.Location = new System.Drawing.Point(12, 398);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(733, 64);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CIERRES";
            // 
            // btInvocar
            // 
            this.btInvocar.Location = new System.Drawing.Point(9, 19);
            this.btInvocar.Name = "btInvocar";
            this.btInvocar.Size = new System.Drawing.Size(116, 23);
            this.btInvocar.TabIndex = 4;
            this.btInvocar.Text = "Cierre de Caja";
            this.btInvocar.UseVisualStyleBackColor = true;
            this.btInvocar.Click += new System.EventHandler(this.btInvocar_Click);
            // 
            // btCierreX
            // 
            this.btCierreX.Location = new System.Drawing.Point(437, 19);
            this.btCierreX.Name = "btCierreX";
            this.btCierreX.Size = new System.Drawing.Size(116, 23);
            this.btCierreX.TabIndex = 3;
            this.btCierreX.Text = "Cierre X";
            this.btCierreX.UseVisualStyleBackColor = true;
            this.btCierreX.Click += new System.EventHandler(this.btCierreX_Click);
            // 
            // btForzarZ
            // 
            this.btForzarZ.Location = new System.Drawing.Point(299, 19);
            this.btForzarZ.Name = "btForzarZ";
            this.btForzarZ.Size = new System.Drawing.Size(116, 23);
            this.btForzarZ.TabIndex = 2;
            this.btForzarZ.Text = "Forzar Z";
            this.btForzarZ.UseVisualStyleBackColor = true;
            this.btForzarZ.Click += new System.EventHandler(this.btForzarZ_Click);
            // 
            // btReimpresionZ
            // 
            this.btReimpresionZ.Location = new System.Drawing.Point(150, 19);
            this.btReimpresionZ.Name = "btReimpresionZ";
            this.btReimpresionZ.Size = new System.Drawing.Size(118, 23);
            this.btReimpresionZ.TabIndex = 1;
            this.btReimpresionZ.Text = "Reimprimir Z";
            this.btReimpresionZ.UseVisualStyleBackColor = true;
            this.btReimpresionZ.Click += new System.EventHandler(this.btReimpresionZ_Click);
            // 
            // btCierreZ
            // 
            this.btCierreZ.Location = new System.Drawing.Point(564, 19);
            this.btCierreZ.Name = "btCierreZ";
            this.btCierreZ.Size = new System.Drawing.Size(118, 23);
            this.btCierreZ.TabIndex = 0;
            this.btCierreZ.Text = "Cierres";
            this.btCierreZ.UseVisualStyleBackColor = true;
            this.btCierreZ.Click += new System.EventHandler(this.btCierreZ_Click);
            // 
            // btKey
            // 
            this.btKey.Location = new System.Drawing.Point(311, 468);
            this.btKey.Name = "btKey";
            this.btKey.Size = new System.Drawing.Size(115, 23);
            this.btKey.TabIndex = 15;
            this.btKey.Text = "Generar Key";
            this.btKey.UseVisualStyleBackColor = true;
            this.btKey.Click += new System.EventHandler(this.btKey_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 510);
            this.Controls.Add(this.btKey);
            this.Controls.Add(this.btExit);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbImpresas);
            this.Controls.Add(this.gbTipo);
            this.Controls.Add(this.grImpresiones);
            this.Controls.Add(this.gbAcciones);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpSeleccion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG Argentina";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtsImpresiones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            this.gbAcciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grImpresiones)).EndInit();
            this.gbTipo.ResumeLayout(false);
            this.gbTipo.PerformLayout();
            this.gbImpresas.ResumeLayout(false);
            this.gbImpresas.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Data.DataSet dtsImpresiones;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Windows.Forms.DateTimePicker dtpSeleccion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbAcciones;
        private System.Windows.Forms.Button btTicketRegalo;
        private System.Windows.Forms.Button btImprimir;
        private System.Windows.Forms.Button btReimprimir;
        private System.Windows.Forms.DataGridView grImpresiones;
        private System.Windows.Forms.GroupBox gbTipo;
        private System.Windows.Forms.RadioButton rbTipoN;
        private System.Windows.Forms.RadioButton rbTipoB;
        private System.Windows.Forms.Button btExit;
        private System.Windows.Forms.GroupBox gbImpresas;
        private System.Windows.Forms.RadioButton rbSinImprimir;
        private System.Windows.Forms.RadioButton rbImpresas;
        private System.Data.DataColumn dataColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn numSerieDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nUMFACTURADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sERIEFISCAL1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nUMEROFISCALDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dESCRIPCIONDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nOMBRECLIENTEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tOTALBRUTODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tOTALNETODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sERIEFISCAL2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fECHADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nUMALBARANDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Z;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btInvocar;
        private System.Windows.Forms.Button btCierreX;
        private System.Windows.Forms.Button btForzarZ;
        private System.Windows.Forms.Button btReimpresionZ;
        private System.Windows.Forms.Button btCierreZ;
        private System.Windows.Forms.Button btKey;
    }
}

