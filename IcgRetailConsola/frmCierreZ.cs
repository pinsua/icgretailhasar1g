﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace IcgRetailConsolaH2G
{
    public partial class frmCierreZ : Form
    {
        /// <summary>
        /// Variable con la direccion IP.
        /// </summary>
        private string _ip;

        private string _Caja;

        private string _connection;

        private bool _habilitoPassword;

        public SqlConnection _conn = new SqlConnection();

        /// <summary>
        /// Instancia de la Libreria de Hasar.
        /// </summary>
        // public static hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
        // FiscalPrinterLib.HASAR hasar = new FiscalPrinterLib.HASAR();

        public frmCierreZ(string sIp, string sConnection, string sCaja, bool sHabilitoPassword)
        {
            InitializeComponent();
            // paso las variables
            _ip = sIp;
            _connection = sConnection;
            _Caja = sCaja;
            _habilitoPassword = sHabilitoPassword;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            try
            {
                // Conectamos.
                _conn.ConnectionString = _connection;

                _conn.Open();

                // Hasar2daGen.Arqueo.GetArqueo(dtsCierres, "Cierres", "Z", _Caja, _conn);
                Hasar1raGen.Arqueo.GetArqueo(dtsCierres, "Cierres", "Z", _Caja, _conn);
                // Hasar1raGen.Arqueo.GetArqueo(dtsCierres, "Cierres", "Z", _Caja, _conn);

                if (grCierre.Rows.Count == 0)
                {
                    btCierreZ.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                DeshabilitarBotones();
                MessageBox.Show("Se produjo el siguente error: " + ex.Message + ". Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void DeshabilitarBotones()
        {
            btCierreZ.Enabled = !btCierreZ.Enabled;
            // btCierreX.Enabled = !btCierreX.Enabled;
        }

        private void Conectar()
        {
            // hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            try
            {                
                // Abrimos el log.
                // hasar.archivoRegistro("HasarLog.log");
                // Conectamos con la hasar.
                // hasar.conectar(_ip);

            }
            catch (Exception )
            {
                throw new Exception("No se pudo conectar con la Controladora Fiscal.");
            }
        }

        private void Cancelar()
        {          
                try
                {
                    Hasar1raGen.Acciones.Cancelar();
                }
                catch
                {
                    throw new Exception("Se produjo un error al cancelar un documento.");
                }     
        }

        private void btCierreZ_Click(object sender, EventArgs e)
        {
            if (grCierre.SelectedRows.Count > 0)
            {
                if (Hasar1raGen.FuncionesVarias.TengoComprobantesSinImprimmir(_conn, _Caja))
                {
                    if (MessageBox.Show("Existen ventas sin fiscalizar. Desea continuar con la impresión del Z fiscal?.",
                       "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        // Vemos si tengo habilitado el password;
                        if (_habilitoPassword)
                        {
                            frmPassword frm = new frmPassword();
                            frm.ShowDialog(this);
                            string _pp = frm._Text;
                            frm.Dispose();
                            if (Form1._password == _pp)
                            {
                                // Recupero los datos del grid.
                                string _cierreTipo = grCierre.SelectedRows[0].Cells[0].Value.ToString();
                                string _numero = grCierre.SelectedRows[0].Cells[1].Value.ToString();
                                string _fecha = grCierre.SelectedRows[0].Cells[2].Value.ToString();
                                string _hora = grCierre.SelectedRows[0].Cells[3].Value.ToString();

                                // hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                                // hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
                                string _msj;

                                try
                                {
                                    // Conectamos.
                                    // Conectar();
                                    string NumeroZ = null;
                                    string DF_CantidadEmitidos = null;
                                    string DF_Total = null;
                                    // ejecutamos el cierre Z.
                                    /* 
                                    _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);
                                    _zeta = _cierre.Z;

                                    _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                                      "Cierre 'Z' Nº            =[" + _zeta.getNumero() + "]" + Environment.NewLine +
                                      "Fecha del Cierre         =[" + _zeta.getFecha() + "]" + Environment.NewLine +
                                      "DF Emitidos              =[" + _zeta.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                                      "DF Cancelados            =[" + _zeta.getDF_CantidadCancelados() + "]" + Environment.NewLine +
                                      "DF Total                 =[" + string.Format("{0:0.00}", _zeta.getDF_Total() / 100) + "]" + Environment.NewLine +
                                      "DF Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getDF_TotalGravado() / 100) + "]" + Environment.NewLine +
                                      "DF Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getDF_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                                      "DF Total Exento          =[" + string.Format("{0:0.00}", _zeta.getDF_TotalExento() / 100) + "]" + Environment.NewLine +
                                      "DF Total IVA             =[" + string.Format("{0:0.00}", _zeta.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                                      "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                                      "NC Emitidas              =[" + _zeta.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                                      "NC Canceladas            =[" + _zeta.getNC_CantidadCancelados() + "]" + Environment.NewLine +
                                      "NC Total                 =[" + string.Format("{0:0.00}", _zeta.getNC_Total() / 100) + "]" + Environment.NewLine +
                                      "NC Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getNC_TotalGravado() / 100) + "]" + Environment.NewLine +
                                      "NC Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getNC_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                                      "NC Total Exento          =[" + string.Format("{0:0.00}", _zeta.getNC_TotalExento() / 100) + "]" + Environment.NewLine +
                                      "NC Total IVA             =[" + string.Format("{0:0.00}", _zeta.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                                      "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                                      "DNFH Emitidos            =[" + _zeta.getDNFH_CantidadEmitidos() + "]" + Environment.NewLine +
                                      "DNFH Total               =[" + _zeta.getDNFH_Total() + "]";
                                    */
                                    _msj = Hasar1raGen.Cierres.ImprimirCierreZ(out NumeroZ, out DF_CantidadEmitidos, out DF_Total);
                                    MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                                    // Modificamos la tabla Arqueos.

                                    string _dato = NumeroZ + ";" + DF_CantidadEmitidos + ";" + DF_Total;

                                    int _rta = Hasar1raGen.Arqueo.UpdateArqueos("Z", Convert.ToInt32(_numero), Convert.ToDateTime(_fecha), _hora, _dato, _Caja, _conn);
                                }
                                catch (Exception ex)
                                {
                                    Cancelar();
                                    MessageBox.Show("Se produjo el siguiente Error : " + ex.Message,
                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Posee comprobantes sin fiscalizar. Por favor fiscalize o anule los comprobantes para poder realizar el cierre Z.",
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
                else
                {
                    // Recupero los datos del grid.

                    string _cierreTipo = grCierre.SelectedRows[0].Cells[0].Value.ToString();
                    string _numero = grCierre.SelectedRows[0].Cells[1].Value.ToString();
                    string _fecha = grCierre.SelectedRows[0].Cells[2].Value.ToString();
                    string _hora = grCierre.SelectedRows[0].Cells[3].Value.ToString();
                    /*
                    hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                    hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
                    */
                    string _msj;

                    try
                    {
                        // Conectamos.
                        Conectar();
                        // ejecutamos el cierre Z.
                        /*
                        _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);
                        _zeta = _cierre.Z;

                        _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                          "Cierre 'Z' Nº            =[" + _zeta.getNumero() + "]" + Environment.NewLine +
                          "Fecha del Cierre         =[" + _zeta.getFecha() + "]" + Environment.NewLine +
                          "DF Emitidos              =[" + _zeta.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                          "DF Cancelados            =[" + _zeta.getDF_CantidadCancelados() + "]" + Environment.NewLine +
                          "DF Total                 =[" + string.Format("{0:0.00}", _zeta.getDF_Total() / 100) + "]" + Environment.NewLine +
                          "DF Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getDF_TotalGravado() / 100) + "]" + Environment.NewLine +
                          "DF Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getDF_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                          "DF Total Exento          =[" + string.Format("{0:0.00}", _zeta.getDF_TotalExento() / 100) + "]" + Environment.NewLine +
                          "DF Total IVA             =[" + string.Format("{0:0.00}", _zeta.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                          "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                          "NC Emitidas              =[" + _zeta.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                          "NC Canceladas            =[" + _zeta.getNC_CantidadCancelados() + "]" + Environment.NewLine +
                          "NC Total                 =[" + string.Format("{0:0.00}", _zeta.getNC_Total() / 100) + "]" + Environment.NewLine +
                          "NC Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getNC_TotalGravado() / 100) + "]" + Environment.NewLine +
                          "NC Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getNC_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                          "NC Total Exento          =[" + string.Format("{0:0.00}", _zeta.getNC_TotalExento() / 100) + "]" + Environment.NewLine +
                          "NC Total IVA             =[" + string.Format("{0:0.00}", _zeta.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                          "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                          "DNFH Emitidos            =[" + _zeta.getDNFH_CantidadEmitidos() + "]" + Environment.NewLine +
                          "DNFH Total               =[" + _zeta.getDNFH_Total() + "]";
                        */

                        string DF_CantidadEmitidos = null;
                        string NumeroZ = null;
                        string DF_Total;
                        _msj = Hasar1raGen.Cierres.ImprimirCierreZ(out NumeroZ, out DF_CantidadEmitidos, out DF_Total);

                        MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                        // Modificamos la tabla Arqueos.

                        string _dato = NumeroZ + ";" + DF_CantidadEmitidos + ";" + DF_Total;

                        int _rta = Hasar1raGen.Arqueo.UpdateArqueos("Z", Convert.ToInt32(_numero), Convert.ToDateTime(_fecha), _hora, _dato, _Caja, _conn);
                    }
                    catch (Exception ex)
                    {
                        Cancelar();
                        MessageBox.Show("Se produjo el siguiente Error : " + ex.Message,
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                }
                // Arqueo.GetArqueo(dtsCierres, "Cierres", "Z", _Caja, _conn);
                Hasar1raGen.Arqueo.GetArqueo(dtsCierres, "Cierres", "Z", _Caja, _conn);
            }
            else
                MessageBox.Show("Debe seleccionar un Cierre de la tabla.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            // vemos la conexion
            if (_conn.State == ConnectionState.Open)
                _conn.Close();

            this.Close();
        }



        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // vemos la conexion
            if (_conn.State == ConnectionState.Open)
                _conn.Close();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            if (!btCierreZ.Enabled)
            {
                MessageBox.Show("Debe realizar un cierre Z en el sistema.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }

        private bool TengoComprobantesSinImprimmir(SqlConnection _conn, string _caja)
        {
            bool _rta = true;

            string _sql = "SELECT count(*) FROM FACTURASVENTASERIESRESOL RIGHT JOIN ALBVENTACAB on " +
                "FACTURASVENTASERIESRESOL.NUMSERIE = ALBVENTACAB.NUMSERIE AND FACTURASVENTASERIESRESOL.NUMFACTURA = ALBVENTACAB.NUMFAC " +
                "AND FACTURASVENTASERIESRESOL.N = ALBVENTACAB.N INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                "WHERE(FACTURASVENTASERIESRESOL.NUMEROFISCAL is null or FACTURASVENTASERIESRESOL.NUMEROFISCAL = '') " +
                "AND ALBVENTACAB.TIQUET = 'T' AND ALBVENTACAB.N = 'B' AND ALBVENTACAB.CAJA = @caja AND ALBVENTACAB.FECHA = @date";

            using (SqlCommand _cmd = new SqlCommand(_sql, _conn))
            {
                _cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                _cmd.Parameters.AddWithValue("@caja", _caja);

                object _resp = _cmd.ExecuteScalar();

                if (_resp == null)
                    _rta = false;
                else
                {
                    if (Convert.ToInt32(_resp) > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
            }

            return _rta;
        }
    }
}
