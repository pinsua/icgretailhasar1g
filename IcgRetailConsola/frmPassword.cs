﻿using System;
using System.Windows.Forms;

namespace IcgRetailConsolaH2G
{
    public partial class frmPassword : Form
    {
        public  string _Text;

        public frmPassword()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _Text = textBox1.Text;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _Text = "";
            this.Close();
        }
    }
}
