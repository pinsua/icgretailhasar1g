﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Xml;

namespace Hasar1raGen
{
    public class FuncionesVarias
    {
        /// <summary>
        /// Calcula el dígito verificador dado un CUIT completo o sin él.
        /// </summary>
        /// <param name="cuit">El CUIT como String sin guiones</param>
        /// <returns>El valor del dígito verificador calculado.</returns>
        public static int CalcularDigitoCuit(string cuit)
        {
            int[] mult = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
            char[] nums = cuit.ToCharArray();
            int total = 0;
            for (int i = 0; i < mult.Length; i++)
            {
                total += int.Parse(nums[i].ToString()) * mult[i];
            }
            int resto = total % 11;
            return resto == 0 ? 0 : resto == 1 ? 9 : 11 - resto;
        }

        public static bool FechaOk(FiscalPrinterLib.HASAR _cf, Cabecera _cab)
        {
            try
            {
                bool _rta = false;
                
                DateTime _fecha = _cf.FechaHoraFiscal;

                if (_cab.fecha.Date == _fecha.Date)
                    _rta = true;
                else
                {
                    if (_cab.fecha.Date >= _fecha.Date.AddDays(-3) && _cab.fecha.Date < _fecha.Date)
                        _rta = true;
                }

                return _rta;
            }
            catch (Exception exc) {
                throw new Exception("Error en FechaOk() + " + exc.Message);
            }
            
        }

        public static bool TengoComprobantesSinImprimmir(SqlConnection _conn, string _caja)
        {
            bool _rta = true;

            string _sql = "SELECT count(*) FROM FACTURASVENTASERIESRESOL RIGHT JOIN ALBVENTACAB on " +
                "FACTURASVENTASERIESRESOL.NUMSERIE = ALBVENTACAB.NUMSERIE AND FACTURASVENTASERIESRESOL.NUMFACTURA = ALBVENTACAB.NUMFAC " +
                "AND FACTURASVENTASERIESRESOL.N = ALBVENTACAB.N INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                "WHERE(FACTURASVENTASERIESRESOL.NUMEROFISCAL is null or FACTURASVENTASERIESRESOL.NUMEROFISCAL = '') " +
                "AND ALBVENTACAB.N = 'B' AND ALBVENTACAB.CAJA = @caja AND ALBVENTACAB.FECHA = @date";

            using (SqlCommand _cmd = new SqlCommand(_sql, _conn))
            {
                _cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                _cmd.Parameters.AddWithValue("@caja", _caja);

                object _resp = _cmd.ExecuteScalar();

                if (_resp == null)
                    _rta = false;
                else
                {
                    if (Convert.ToInt32(_resp) > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
            }

            return _rta;
        }

        public static bool FaltaPapel(FiscalPrinterLib.HASAR _cf)
        {
            if (_cf.HuboFaltaPapel)
            {
                return true;
            }
            else
                return false;
        }

        public static string QueComprobanteEs(string serie, string numero, string n, string vendedor,
           SqlConnection _con)
        {
            string _sql = @"select  TIPOSDOC.DESCRIPCION from FACTURASVENTA inner join TIPOSDOC on FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC
                    WHERE FACTURASVENTA.NUMSERIE = @serie AND FACTURASVENTA.NUMFACTURA = @numero AND FACTURASVENTA.N = @n
                    AND FACTURASVENTA.CODVENDEDOR = @codVendedor";

            string _rta = "";

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@serie", serie);
                _cmd.Parameters.AddWithValue("@n", n);
                _cmd.Parameters.AddWithValue("@numero", numero);
                _cmd.Parameters.AddWithValue("@codVendedor", vendedor);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _rta = _reader["DESCRIPCION"].ToString();
                        }
                    }
                }
            }

            return _rta;
        }

        public static bool ComparoSubtotal(FiscalPrinterLib.HASAR _cf, double _totalNeto)
        {
            bool _rta = false;
            
            object items;
            object total;
            object iva;
            object pagado;
            object ivanoi;
            object impint;

            _cf.Subtotal(false, out items, out total, out iva, out pagado, out ivanoi, out impint);

            double _subTotal = (double)total / 100;
            if (_subTotal != Math.Abs(_totalNeto))
            {
                double _dif = _subTotal - Math.Abs(_totalNeto);
                //double _pipo = double.Parse(".02", CultureInfo.InvariantCulture);
                double _pipo = 0.02;
                if (_dif > _pipo)
                    _rta = false;
                else
                    _rta = true;
            }
            else
                _rta = true;
                return _rta;
        }

        public static bool LeerXmlConfiguracion(out string _caja, out string _tktregalo1, out string _tktregalo2, out string _tktregalo3,
            out string _terminal, out string _keyICG, out string _textoAjuste,
            out string _server, out string _user, out string _catalog, InfoIrsa _infoIrsa)
        {
            bool _rta = false;
            
            _caja = "";
            _tktregalo1 = "";
            _tktregalo2 = "";
            _tktregalo3 = "";
            _terminal = "";
            _keyICG = "";
            _textoAjuste = "";

            _server = "";
            _user = "";
            _catalog = "";
            
            try
            {
                if (File.Exists("Config2daGeneracion.xml"))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load("Config2daGeneracion.xml");

                    XmlNodeList _List1 = xDoc.SelectNodes("/config");
                    foreach (XmlNode xn in _List1)
                    {
                        _caja = xn["caja"].InnerText;
                        _tktregalo1 = xn["tktregalo1"].InnerText;
                        _tktregalo2 = xn["tktregalo2"].InnerText;
                        _tktregalo3 = xn["tktregalo3"].InnerText;
                        _textoAjuste = xn["textoajuste"].InnerText;
                        _terminal = xn["terminal"].InnerText;
                        _keyICG = xn["keyICG"].InnerText;
                    }

                    XmlNodeList _Listdb = xDoc.SelectNodes("/config/bd");
                    foreach (XmlNode xn in _Listdb)
                    {
                        _server = xn["server"].InnerText;
                        _user = xn["user"].InnerText;
                        _catalog = xn["database"].InnerText;
                    }

                    XmlNodeList _ListIrsa = xDoc.SelectNodes("/config/Irsa");
                    foreach (XmlNode xn in _ListIrsa)
                    {
                        _infoIrsa.contrato = xn["Contrato"].InnerText;
                        _infoIrsa.local = xn["Local"].InnerText;
                        _infoIrsa.pathSalida = xn["PathSalida"].InnerText;
                        _infoIrsa.pos = xn["Pos"].InnerText;
                        _infoIrsa.rubro = xn["Rubro"].InnerText;
                    }

                    _rta = true;
                }
                else
                    _rta = false;
            }
            catch (Exception)
            {
                throw new Exception("Se produjo un error leyendo el archivo de configuracion." + Environment.NewLine + "Por favor revise el archivo.");
            }
            return _rta;
        }

        public static SeriesResol GetContador(string serieresol, string numresol, SqlConnection _con)
        {
            string _sql = @"select SERIERESOL,NUMRESOL, FECHA, NUMEROINICIAL, NUMEROFINAL, ACTIVO, CONTADOR, FECHAINGRESO, FECHAVENCIMIENTO 
                        from SERIESRESOLUCION Where SERIERESOL = @SerieResol and NUMRESOL = @NumResol";

            SeriesResol _cls = new SeriesResol();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@SerieResol", serieresol);
                _cmd.Parameters.AddWithValue("@NumResol", numresol);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _cls.activo = Convert.ToInt32(_reader["ACTIVO"]);
                            _cls.contador = Convert.ToInt32(_reader["CONTADOR"]);
                            _cls.fecha = _reader["FECHA"] == null ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(_reader["FECHA"]);
                            _cls.fechaingreso = _reader["FECHAINGRESO"] == null ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(_reader["FECHAINGRESO"]);
                            _cls.fechavencimiento = _reader["FECHAVENCIMIENTO"] == null ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(_reader["FECHAVENCIMIENTO"]);
                            _cls.numerofinal = Convert.ToInt32(_reader["NUMEROFINAL"]);
                            _cls.numeroinicial = Convert.ToInt32(_reader["NUMEROINICIAL"]);
                            _cls.numresol = _reader["NUMRESOL"].ToString();
                            _cls.serieresol = _reader["SERIERESOL"].ToString();
                        }
                    }
                }
            }

            return _cls;
        }

        public static bool UpdateContador(string serieresol, string numresol, SqlConnection _con)
        {
            string _sql = @"UPDATE SERIESRESOLUCION SET CONTADOR = CONTADOR + 1 
                            Where SERIERESOL = @SerieResol and NUMRESOL = @NumResol";

            bool _rta = false;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@SerieResol", serieresol);
                _cmd.Parameters.AddWithValue("@NumResol", numresol);

                try
                {
                    _cmd.ExecuteNonQuery();
                    _rta = true;
                }
                catch
                {
                    _rta = false;
                }
            }

            return _rta;
        }

        public static bool UpdateContador(string serieresol, string numresol, int _nroCbte, SqlConnection _con)
        {
            string _sql = @"UPDATE SERIESRESOLUCION SET CONTADOR = @Contador Where SERIERESOL = @SerieResol and NUMRESOL = @NumResol";

            bool _rta = false;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@Contador", _nroCbte);
                _cmd.Parameters.AddWithValue("@SerieResol", serieresol);
                _cmd.Parameters.AddWithValue("@NumResol", numresol);

                try
                {
                    _cmd.ExecuteNonQuery();
                    _rta = true;
                }
                catch
                {
                    _rta = false;
                }
            }

            return _rta;
        }
    }

    public class SeriesResol
    {
        public string serieresol { get; set; }
        public string numresol { get; set; }
        public DateTime? fecha { get; set; }
        public int numeroinicial { get; set; }
        public int numerofinal { get; set; }
        public int activo { get; set; }
        public int contador { get; set; }
        public DateTime? fechaingreso { get; set; }
        public DateTime? fechavencimiento { get; set; }
    }

    public class KeyGen
    {
        //public static bool ValidoKeySoftware(string keyIcg)
        //{
        //    string _key = IcgVarios.NumerosSerie.GetCPUId();

        //    string _cod = IcgVarios.Licencia.GenerarLicenciaICG(_key);

        //    if (keyIcg == _cod)
        //        return true;
        //    else
        //        return false;
        //}

        ///// <summary>
        ///// Encrypts the string.
        ///// </summary>
        ///// <param name="clearText">The clear text.</param>
        ///// <param name="Key">The key.</param>
        ///// <param name="IV">The IV.</param>
        ///// <returns></returns>
        //public static byte[] EncryptString(byte[] clearText, byte[] Key, byte[] IV)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    Rijndael alg = Rijndael.Create();
        //    alg.Key = Key;
        //    alg.IV = IV;
        //    CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);
        //    cs.Write(clearText, 0, clearText.Length);
        //    cs.Close();
        //    byte[] encryptedData = ms.ToArray();
        //    return encryptedData;
        //}

    }
}
