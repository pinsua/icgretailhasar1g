﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Hasar1raGen
{
    public class Recargos
    {
        public string _codigo { get; set; }
        public double _base { get; set; }
        public double _importe { get; set; }
        public string _texto { get; set; }
        public double _recargo { get; set; }
        public double _bruto { get; set; }
        public double _iva { get; set; }

        public static List<Recargos> GetRecargos(string _numSerie, string _n, int _numero, SqlConnection _con)
        {
            string _sql = "SELECT FACTURASVENTADTOS.BASE, FACTURASVENTADTOS.IMPORTE, CARGOSDTOS.NOMBRE, FACTURASVENTADTOS.DTOCARGO, " +
                "(FACTURASVENTATOT.BRUTO - FACTURASVENTATOT.TOTDTOCOMERC - FACTURASVENTATOT.TOTDTOPP) as BRUTO, FACTURASVENTATOT.IVA " +
                "FROM FACTURASVENTATOT inner JOIN FACTURASVENTADTOS  ON FACTURASVENTATOT.SERIE = FACTURASVENTADTOS.NUMSERIE " +
                "AND FACTURASVENTATOT.NUMERO = FACTURASVENTADTOS.NUMERO AND FACTURASVENTATOT.N = FACTURASVENTADTOS.N " +
                "AND FACTURASVENTATOT.CODDTO = FACTURASVENTADTOS.CODDTO " +
                "INNER JOIN CARGOSDTOS ON FACTURASVENTADTOS.CODDTO = CARGOSDTOS.CODIGO " +
                "WHERE FACTURASVENTATOT.SERIE = @NumSerie AND FACTURASVENTATOT.NUMERO = @Numero AND FACTURASVENTATOT.N = @N AND CARGOSDTOS.CODIGOFISCAL <> '005'";

            List<Recargos> _items = new List<Recargos>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@NumSerie", _numSerie);
                _cmd.Parameters.AddWithValue("@N", _n);
                _cmd.Parameters.AddWithValue("@Numero", _numero);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            Recargos _cls = new Recargos();
                            _cls._base = _reader.IsDBNull(0) ? 0 : Convert.ToDouble(_reader["BASE"]);
                            _cls._importe = _reader.IsDBNull(1) ? 0 : Convert.ToDouble(_reader["IMPORTE"]);
                            _cls._texto = _reader["NOMBRE"] == null ? "" : _reader["NOMBRE"].ToString();
                            _cls._iva = _reader.IsDBNull(5) ? 0 : Convert.ToDouble(_reader["IVA"]);
                            _cls._recargo = _reader.IsDBNull(3) ? 0 : Convert.ToDouble(_reader["DTOCARGO"]);
                            _cls._bruto = _reader.IsDBNull(4) ? 0 : Convert.ToDouble(_reader["BRUTO"]);

                            _items.Add(_cls);
                        }
                    }
                }
            }

            return _items;
        }        
    }
}
