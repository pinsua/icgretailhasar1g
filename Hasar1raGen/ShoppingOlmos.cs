﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasar1raGen
{
    public class ShoppingOlmos
    {
        public static void LanzarShoppingOlmos(Cabecera _cabecera, List<Pagos> _pagos, string _pathOut,
            string _nroComprobante, int _nroClienteOlmos, int _nroPosOlmos, int _rubroOlmos ,
            int _cantidad, Int64 _cuit, string _procesoOlmos, bool _EnvioInfo, SqlConnection _conexion)
        {
            try
            {
                //Recupero los dato de impresion del comprobante.
                FacturasVentaSerieResol _fsr = FacturasVentaSerieResol.GetTiquet(_cabecera.numserie, Convert.ToInt32(_cabecera.numfac), _cabecera.n, _conexion);
                //Cabecera
                GenerarDatosCabecera(_fsr, _cabecera, _nroClienteOlmos, _cuit, _nroPosOlmos, _procesoOlmos, _EnvioInfo);
                //Detalle
                GenerarDatosDetalle(_fsr,_cabecera,_nroClienteOlmos,_rubroOlmos,_cantidad, _procesoOlmos, _EnvioInfo);
                //Pagos
                GenerarDatosPagos(_fsr, _pagos, _nroClienteOlmos, _rubroOlmos, _procesoOlmos, _EnvioInfo);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static void GenerarDatosCabecera(FacturasVentaSerieResol _fvsr, Cabecera _cab, int _nroCliente, Int64 _cuitEmisor, int _posOlmos, string _ProcesoOlmos, bool _envioInfo)
        {
            try
            {
                string _fecha = _cab.fecha.Year.ToString().PadLeft(4, '0') + _cab.fecha.Month.ToString().PadLeft(2, '0') + _cab.fecha.Day.ToString().PadLeft(2, '0');
                //string _hora = _cab.hora.Hour.ToString().PadLeft(2, '0') + _cab.hora.Minute.ToString().PadLeft(2, '0') + _cab.hora.Second.ToString().PadLeft(2, '0');
                string _hora = _cab.hora.Hour.ToString().PadLeft(2, '0') + _cab.hora.Minute.ToString().PadLeft(2, '0');

                string _tipoDocCliente = "";
                if (_cab.regfacturacioncliente == 1)
                    _tipoDocCliente = "CUIT";
                else
                    _tipoDocCliente = "DNI";

                string _datos = "record:C|" + _nroCliente.ToString() + "|" + Convert.ToInt32(_fvsr.serieFiscal2.Substring(0, 3)).ToString() +
                    "|" + Convert.ToInt32(_fvsr.serieFiscal1).ToString() + "|" + _fvsr.numeroFiscal.ToString() + "|" + _fecha + "|" + _hora + "|CUIT" +
                    "|" + _cuitEmisor.ToString() + "|" + _tipoDocCliente + "|" + _cab.nrodoccliente + "|N|" + _posOlmos.ToString() + "|" + _fecha;

                if (_envioInfo)
                {
                    EnvioInfo(_ProcesoOlmos, _datos);
                }
                else
                {
                    if (!String.IsNullOrEmpty(_datos))
                    {
                        //Mando la informacion de pago.
                        if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                        {
                            Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                        }
                        using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.txt", true))
                        {
                            sw.WriteLine(_datos);
                            sw.Flush();
                        }
                    }
                }
            }
            catch(Exception ex)
            { throw new Exception("GenerarDatosCabecera - " + ex.Message); }
        }

        private static void GenerarDatosDetalle(FacturasVentaSerieResol _fvsr, Cabecera _cab, int _nroCliente, int _rubro, int _cantidad, string _ProcesoOlmos, bool _envioInfo)
        {
            try
            {
                double _total = 0;
                if (_cab.totdtopp < 0)
                    _total = _cab.totalbruto + (_cab.totdtopp * -1);
                else
                {
                    _total = _cab.totalbruto;
                }

                string _datos = "record:D|" + _nroCliente.ToString() + "|" + Convert.ToInt32(_fvsr.serieFiscal2.Substring(0, 3)).ToString() +
                    "|" + Convert.ToInt32(_fvsr.serieFiscal1).ToString() + "|" + _fvsr.numeroFiscal.ToString() + "|21.00|" +
                    Math.Round(Math.Abs(_total), 2).ToString().Replace(',', '.') + "|" + Math.Round(Math.Abs(_cab.totalimpuestos), 2).ToString().Replace(',', '.') +
                    "|0.00|" + _rubro.ToString() + "|" + Math.Abs(_cantidad).ToString();

                if (_envioInfo)
                {
                    EnvioInfo(_ProcesoOlmos, _datos);
                }
                else
                {
                    if (!String.IsNullOrEmpty(_datos))
                    {
                        //Mando la informacion de pago.
                        if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                        {
                            Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                        }
                        using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.txt", true))
                        {
                            sw.WriteLine(_datos);
                            sw.Flush();
                        }
                    }
                }
            }
            catch (Exception ex)
            { throw new Exception("GenerarDatosDetalle - " + ex.Message); }
        }

        private static void GenerarDatosPagos(FacturasVentaSerieResol _fvsr, List<Pagos> _pagos, int _nroCliente, int _rubro, string _procesoOlmos, bool _envioInfo)
        {
            try
            {
                foreach (Pagos pg in _pagos)
                {
                    int _formaPago = 0;
                    if (String.IsNullOrEmpty(pg.olmosTipoPago))
                        _formaPago = 0;
                    else
                        _formaPago = Convert.ToInt32(pg.olmosTipoPago);
                    int _codTarjeta = 0;
                    if (String.IsNullOrEmpty(pg.olmosTarjeta))
                        _codTarjeta = 0;
                    else
                        _codTarjeta = Convert.ToInt32(pg.olmosTarjeta);

                    //switch (pg.formapago)
                    //{
                    //    case "E":
                    //        {
                    //            if (pg.tipopago.ToUpper() != "DOLAR")
                    //                _formaPago = 1;
                    //            else
                    //                _formaPago = 4;
                    //            break;
                    //        }
                    //    case "T":
                    //        {
                    //            _formaPago = 2;
                    //            break;
                    //        }
                    //    case "O":
                    //        {
                    //            _formaPago = 6;
                    //            break;
                    //        }
                    //}

                    //switch (pg.codigotarjeta)
                    //{
                    //    case "AM":
                    //        {
                    //            _codTarjeta = 4;
                    //            break;
                    //        }
                    //    case "VI":
                    //        {
                    //            _codTarjeta = 2;
                    //            break;
                    //        }
                    //    case "MC":
                    //        {
                    //            _codTarjeta = 1;
                    //            break;
                    //        }
                    //    case "EL":
                    //        {
                    //            _codTarjeta = 3;
                    //            break;
                    //        }
                    //    case "DI":
                    //        {
                    //            _codTarjeta = 5;
                    //            break;
                    //        }
                    //    case "TN":
                    //        {
                    //            _codTarjeta = 6;
                    //            break;
                    //        }
                    //    case "MA":
                    //        {
                    //            _codTarjeta = 9;
                    //            break;
                    //        }
                    //    case "CA":
                    //        {
                    //            _codTarjeta = 12;
                    //            break;
                    //        }
                    //    case "FR":
                    //        {
                    //            _codTarjeta = 13;
                    //            break;
                    //        }
                    //    case "IC":
                    //        {
                    //            _codTarjeta = 15;
                    //            break;
                    //        }
                    //    case "TF":
                    //        {
                    //            _codTarjeta = 19;
                    //            break;
                    //        }

                    //}

                    //Cuotas
                    int _cuotas = 0;
                    if (!String.IsNullOrEmpty(pg.cuotas))
                    {
                        string[] _lst = pg.cuotas.Split(' ');
                        _cuotas = Convert.ToInt16(_lst[0]);
                    }

                    string _datos = "";
                    if (_formaPago != 0)
                    {
                        if (_codTarjeta != 0)
                            _datos = "record:P|" + _nroCliente.ToString() + "|" + Convert.ToInt32(_fvsr.serieFiscal2.Substring(0, 3)).ToString() +
                                "|" + Convert.ToInt32(_fvsr.serieFiscal1).ToString() + "|" + _fvsr.numeroFiscal.ToString() + "|" + _formaPago.ToString() +
                                "|" + _codTarjeta.ToString() + "|" + _cuotas.ToString() + "|" + Math.Round(Math.Abs(pg.monto), 2).ToString().Replace(',', '.');
                        else
                            _datos = "record:P|" + _nroCliente.ToString() + "|" + Convert.ToInt32(_fvsr.serieFiscal2.Substring(0, 3)).ToString() +
                                "|" + Convert.ToInt32(_fvsr.serieFiscal1).ToString() + "|" + _fvsr.numeroFiscal.ToString() + "|" + _formaPago.ToString() +
                                "|" + "||" + Math.Round(Math.Abs(pg.monto), 2).ToString().Replace(',', '.');
                    }

                    if (_envioInfo)
                    {
                        EnvioInfo(_procesoOlmos, _datos);
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(_datos))
                        {
                            //Mando la informacion de pago.
                            if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                            {
                                Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                            }
                            using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.Txt", true))
                            {
                                sw.WriteLine(_datos);
                                sw.Flush();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { throw new Exception("GenerarDatosPagos - " + ex.Message); }
        }

        private static void EnvioInfo(string _proceso, string _record)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(_proceso, _record);
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.UseShellExecute = false; //No utiliza RunDLL32 para lanzarlo
                                               //Redirige las salidas y los errores
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            Process proc = Process.Start(startInfo); //Ejecuta el proceso
            proc.WaitForExit(); // Espera a que termine el proceso
            string error = proc.StandardError.ReadToEnd();
            if (error != null && error != "") //Error
                throw new Exception("Se ha producido un error al ejecutar el proceso '" + _proceso + "'\n" + "Detalles:\n" + "Error: " + error);
            //else //Éxito
            //    return proc.StandardOutput.ReadToEnd(); //Devuelve el resultado 
        }
    }
}
