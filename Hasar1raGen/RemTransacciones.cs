﻿using System;
using System.Data.SqlClient;

namespace Hasar1raGen
{
    public class RemTransacciones
    {
        public static void GraboRemTransacciones(string _terminal, string _caja, string _z, int _tipo, int _accion, 
            string _serie, string _numero, string _N, SqlConnection _con)
        {
            try
            {
                string _sql = "INSERT INTO REM_TRANSACCIONES(TERMINAL, CAJA, CAJANUM, Z, TIPO, ACCION, SERIE, NUMERO, N, FECHA, HORA, FO, IDCENTRAL, TALLA, COLOR, CODIGOSTR) " +
                    "VALUES(@terminal, @caja, 0, @Z, @tipo, @accion, @serie, @numero, @N, @date, @hora, 0, 1, '.', '.', '')";

                using (SqlCommand _cmd = new SqlCommand(_sql, _con))
                {
                    _cmd.Parameters.AddWithValue("@terminal", _terminal);
                    _cmd.Parameters.AddWithValue("@caja", _caja);
                    _cmd.Parameters.AddWithValue("@Z", Convert.ToInt32(_z));
                    _cmd.Parameters.AddWithValue("@tipo", _tipo);
                    _cmd.Parameters.AddWithValue("@accion", _accion);
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", Convert.ToInt32(_numero));
                    _cmd.Parameters.AddWithValue("@N", _N);
                    _cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                    _cmd.Parameters.AddWithValue("@hora", DateTime.Now.Date);

                    int rta = _cmd.ExecuteNonQuery();
                }
            }
            catch { }
        }
    }
}
