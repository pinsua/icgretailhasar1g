﻿using System;
using System.Data.SqlClient;

namespace Hasar1raGen
{
    public class Cabecera
    {
        public string numserie { get; set; }
        public int numalbaran { get; set; }
        public string n { get; set; }
        public int numfac { get; set; }
        public int codcliente { get; set; }
        public int codvendedor { get; set; }
        public double dtocomercial { get; set; }
        public double totdtocomercial { get; set; }
        public double totalbruto { get; set; }
        public double totalimpuestos { get; set; }
        public double totalneto { get; set; }
        public int tipodoc { get; set; }
        public string nombrecliente { get; set; }
        public string direccioncliente { get; set; }
        public string nrodoccliente { get; set; }
        public string codpostalcliente { get; set; }
        public string ciudadcliente { get; set; }
        public string provinciacliente { get; set; }
        public int regfacturacioncliente { get; set; }
        public string descripcionticket { get; set; }
        public double dtopp { get; set; }
        public double totdtopp { get; set; }
        public double entregado { get; set; }
        public DateTime fecha { get; set; }
        public int z { get; set; }
        public string telefono { get; set; }
        public string nomvendedor { get; set; }
        public DateTime hora { get; set; }

        public static Cabecera GetCabecera(string _serie, int _numero, string _N, SqlConnection _con)
        {
            string _sql = "SELECT ALBVENTACAB.NUMSERIE, ALBVENTACAB.NUMALBARAN, ALBVENTACAB.N, ALBVENTACAB.NUMFAC, ALBVENTACAB.CODCLIENTE, ALBVENTACAB.CODVENDEDOR, " +
                "ALBVENTACAB.DTOCOMERCIAL, ALBVENTACAB.TOTDTOCOMERCIAL, ALBVENTACAB.TOTALBRUTO, ALBVENTACAB.TOTALIMPUESTOS, ALBVENTACAB.TOTALNETO, ALBVENTACAB.TIPODOC, ALBVENTACAB.DTOPP, ALBVENTACAB.TOTDTOPP, " +
                "CLIENTES.NOMBRECLIENTE, CLIENTES.DIRECCION1, CLIENTES.NIF20, CLIENTES.CODPOSTAL, CLIENTES.POBLACION, CLIENTES.PROVINCIA, CLIENTES.REGIMFACT, " +
                "TIPOSDOC.DESCRIPCION, FACTURASVENTA.ENTREGADO, ALBVENTACAB.FECHA, ALBVENTACAB.Z, CLIENTES.TELEFONO1, VENDEDORES.NOMVENDEDOR, ALBVENTACAB.HORA " +
                "FROM FACTURASVENTA INNER JOIN  ALBVENTACAB on FACTURASVENTA.NUMSERIE = ALBVENTACAB.NUMSERIE AND FACTURASVENTA.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTA.N = ALBVENTACAB.N " +
                "INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                "INNER JOIN VENDEDORES on ALBVENTACAB.CODVENDEDOR = VENDEDORES.CODVENDEDOR " +
                "WHERE FACTURASVENTA.NUMSERIE = @Numserie AND facturasventa.NUMFACTURA = @NumFact AND FACTURASVENTA.N = @N";

            Cabecera _cabecera = new Cabecera();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@Numserie", _serie);
                _cmd.Parameters.AddWithValue("@N", _N);
                _cmd.Parameters.AddWithValue("@NumFact", _numero);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _cabecera.ciudadcliente = _reader["POBLACION"].ToString();
                            _cabecera.codcliente = Convert.ToInt32(_reader["CODCLIENTE"]);
                            _cabecera.codpostalcliente = _reader["CODPOSTAL"].ToString();
                            _cabecera.codvendedor = Convert.ToInt32(_reader["CODVENDEDOR"]);
                            _cabecera.descripcionticket = _reader["DESCRIPCION"].ToString();
                            _cabecera.direccioncliente = _reader["DIRECCION1"].ToString();
                            _cabecera.dtocomercial = Convert.ToDouble(_reader["DTOCOMERCIAL"]);
                            _cabecera.n = _reader["N"].ToString();
                            _cabecera.nombrecliente = _reader["NOMBRECLIENTE"].ToString();
                            _cabecera.nrodoccliente = _reader["NIF20"].ToString();
                            _cabecera.numalbaran = Convert.ToInt32(_reader["NUMALBARAN"]);
                            _cabecera.numfac = Convert.ToInt32(_reader["NUMFAC"]);
                            _cabecera.numserie = _reader["NUMSERIE"].ToString();
                            _cabecera.provinciacliente = _reader["PROVINCIA"].ToString();
                            _cabecera.regfacturacioncliente = Convert.ToInt32(_reader["REGIMFACT"]);
                            _cabecera.tipodoc = Convert.ToInt32(_reader["TIPODOC"]);
                            _cabecera.totalbruto = Convert.ToDouble(_reader["TOTALBRUTO"]);
                            _cabecera.totalimpuestos = Convert.ToDouble(_reader["TOTALIMPUESTOS"]);
                            _cabecera.totalneto = Convert.ToDouble(_reader["TOTALNETO"]);
                            _cabecera.totdtocomercial = Convert.ToDouble(_reader["TOTDTOCOMERCIAL"]);
                            _cabecera.dtopp = Convert.ToDouble(_reader["DTOPP"]);
                            _cabecera.totdtopp = Convert.ToDouble(_reader["TOTDTOPP"]);
                            _cabecera.entregado = Convert.ToDouble(_reader["ENTREGADO"]);
                            _cabecera.fecha = Convert.ToDateTime(_reader["FECHA"]);
                            _cabecera.z = Convert.ToInt32(_reader["Z"]);
                            _cabecera.telefono = _reader["TELEFONO1"].ToString();
                            _cabecera.nomvendedor = _reader["NOMVENDEDOR"].ToString();
                            _cabecera.hora = Convert.ToDateTime(_reader["HORA"]);
                        }
                    }
                    else {
                        return null;
                    }
                }
            }

            return _cabecera;
        }        
    }
}
