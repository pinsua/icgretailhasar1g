﻿using System;

namespace Hasar1raGen
{
    public class Cierres
    {
        public static string ImprimirCierreX()
        {
            FiscalPrinterLib.HASAR hasar = new FiscalPrinterLib.HASAR();
            RespuestaCerrarJornadaFiscal _cierre = new RespuestaCerrarJornadaFiscal();            
            CerrarJornadaFiscalX x = new CerrarJornadaFiscalX();

            string _msj;

            try
            {
                // Conectamos.
                Acciones.Conectar(hasar, Acciones.modelo, Acciones.puerto, Acciones.baudios);

                hasar.ReporteX(
                    out x.NumeroX,
                    out x.CantidadDFCancelados,
                    out x.CantidadDNFHEmitidos,
                    out x.CantidadDNFEmitidos,
                    out x.CantidadDFEmitidos,
                    out x.UltimoDocFiscalBC,
                    out x.UltimoDocFiscalA,
                    out x.MontoVentasDocFiscal,
                    out x.MontoIVADocFiscal,
                    out x.MontoImpInternosDocFiscal,
                    out x.MontoPercepcionesDocFiscal,
                    out x.MontoIVANoInscriptoDocFiscal,
                    out x.UltimaNotaCreditoBC,
                    out x.UltimaNotaCreditoA,
                    out x.MontoVentasNotaCredito,
                    out x.MontoIVANotaCredito,
                    out x.MontoImpInternosNotaCredito,
                    out x.MontoPercepcionesNotaCredito,
                    out x.MontoIVANoInscriptoNotaCredito,
                    out x.UltimoRemito,
                    out x.CantidadNCCanceladas,
                    out x.CantidadDFBCEmitidos,
                    out x.CantidadDFAEmitidos,
                    out x.CantidadNCBCEmitidas,
                    out x.CantidadNCAEmitidas
                    );
                
                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine;
                _msj += "Cierre 'Z' Nº                =[" + x.NumeroX + "]" + Environment.NewLine;
                _msj += "Fecha del Cierre             =[" + DateTime.Now.ToShortDateString() + "]" + Environment.NewLine;
                _msj += "DF Emitidos                  =[" + x.CantidadDFEmitidos + "]" + Environment.NewLine;
                _msj += "DF Cancelados                =[" + x.CantidadDFCancelados + "]" + Environment.NewLine;
                _msj += "DFA Emitidos                 =[" + x.CantidadDFAEmitidos + "]" + Environment.NewLine;
                _msj += "DFBC Emitidos                =[" + x.CantidadDFBCEmitidos + "]" + Environment.NewLine;
                _msj += "DF Cancelados                =[" + x.CantidadDFCancelados + "]" + Environment.NewLine;
                _msj += "DF Impuestos Internos        =[" + string.Format("{0:0.00}", Convert.ToDecimal(x.MontoImpInternosDocFiscal)) + "]" + Environment.NewLine;
                _msj += "DF Monto IVA                 =[" + string.Format("{0:0.00}", Convert.ToDecimal(x.MontoIVADocFiscal)) + "]" + Environment.NewLine;
                _msj += "DF Monto IVA No Inscripto    =[" + string.Format("{0:0.00}", Convert.ToDecimal(x.MontoIVANoInscriptoDocFiscal)) + "]" + Environment.NewLine;
                _msj += "DF Monto Percepciones        =[" + string.Format("{0:0.00}", Convert.ToDecimal(x.MontoPercepcionesDocFiscal)) + "]" + Environment.NewLine;
                _msj += "DF Monto Ventas              =[" + string.Format("{0:0.00}", Convert.ToDecimal(x.MontoVentasDocFiscal)) + "]" + Environment.NewLine;
                _msj += "Ultimo DF A                  =[" + string.Format("{0}", Convert.ToInt32(x.UltimoDocFiscalA)) + "]" + Environment.NewLine;
                _msj += "Ultimo DF BC                 =[" + string.Format("{0}", Convert.ToInt32(x.UltimoDocFiscalBC)) + "]" + Environment.NewLine;
                _msj += "NCA Emitidas                 =[" + x.CantidadNCAEmitidas + "]" + Environment.NewLine;
                _msj += "NCBC Emitidas                =[" + x.CantidadNCBCEmitidas + "]" + Environment.NewLine;
                _msj += "NCC Canceladas               =[" + x.CantidadNCCanceladas + "]" + Environment.NewLine;
                _msj += "NC Impuestos Internos        =[" + string.Format("{0:0.00}", Convert.ToDecimal(x.MontoImpInternosNotaCredito)) + "]" + Environment.NewLine;
                _msj += "NC Monto IVA                 =[" + string.Format("{0:0.00}", Convert.ToDecimal(x.MontoIVANotaCredito)) + "]" + Environment.NewLine;
                _msj += "NC Monto IVA No Inscripto    =[" + string.Format("{0:0.00}", Convert.ToDecimal(x.MontoIVANoInscriptoNotaCredito)) + "]" + Environment.NewLine;
                _msj += "NC Monto Percepciones        =[" + string.Format("{0:0.00}", Convert.ToDecimal(x.MontoPercepcionesNotaCredito)) + "]" + Environment.NewLine;
                _msj += "NC Monto Ventas              =[" + string.Format("{0:0.00}", Convert.ToDecimal(x.MontoVentasNotaCredito)) + "]" + Environment.NewLine;
                _msj += "Ultimo Remito                =[" + string.Format("{0}", (int)x.UltimoRemito) + "]" + Environment.NewLine;

                return _msj;

            }
            catch (Exception ex)
            {
                // Cancelamos
                Acciones.Cancelar(hasar);
                // Mandamos la Excepcion
                throw new Exception(ex.Message);
            }


        }

        public static string ImprimirCierreZ(out string NumeroZ, out string DF_CantidadEmitidos, out string DF_Total)
        {
            FiscalPrinterLib.HASAR hasar = new FiscalPrinterLib.HASAR();
            RespuestaCerrarJornadaFiscal _cierre = new RespuestaCerrarJornadaFiscal();            
            CerrarJornadaFiscalZ _zeta = new CerrarJornadaFiscalZ();
            string _msj;

            try
            {
                // Conectamos.
                Acciones.Conectar(hasar, Acciones.modelo, Acciones.puerto, Acciones.baudios);
                // ejecutamos el cierre Z.
                
                hasar.ReporteZ(
                    out _zeta.NumeroZeta,
                    out _zeta.CantidadDFCancelados,
                    out _zeta.CantidadDNFHEmitidos,
                    out _zeta.CantidadDNFEmitidos,
                    out _zeta.CantidadDFEmitidos,
                    out _zeta.UltimoDocFiscalBC,
                    out _zeta.UltimoDocFiscalA,
                    out _zeta.MontoVentasDocFiscal,
                    out _zeta.MontoIVADocFiscal,
                    out _zeta.MontoImpInternosDocFiscal,
                    out _zeta.MontoPercepcionesDocFiscal,
                    out _zeta.MontoIVANoInscriptoDocFiscal,
                    out _zeta.UltimaNotaCreditoBC,
                    out _zeta.UltimaNotaCreditoA,
                    out _zeta.MontoVentasNotaCredito,
                    out _zeta.MontoIVANotaCredito,
                    out _zeta.MontoImpInternosNotaCredito,
                    out _zeta.MontoPercepcionesNotaCredito,
                    out _zeta.MontoIVANoInscriptoNotaCredito,
                    out _zeta.UltimoRemito,
                    out _zeta.CantidadNCCanceladas,
                    out _zeta.CantidadDFBCEmitidos,
                    out _zeta.CantidadDFAEmitidos,
                    out _zeta.CantidadNCBCEmitidas,
                    out _zeta.CantidadNCAEmitidas
                    );

                NumeroZ = _zeta.NumeroZeta.ToString();
                DF_CantidadEmitidos = _zeta.CantidadDFEmitidos.ToString();
                DF_Total = _zeta.MontoVentasDocFiscal.ToString();

                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine;
                _msj += "Cierre 'Z' Nº                =[" + _zeta.NumeroZeta + "]" + Environment.NewLine;
                _msj += "Fecha del Cierre             =[" + DateTime.Now.ToShortDateString() + "]" + Environment.NewLine;
                _msj += "DF Emitidos                  =[" + _zeta.CantidadDFEmitidos + "]" + Environment.NewLine;
                _msj += "DF Cancelados                =[" + _zeta.CantidadDFCancelados + "]" + Environment.NewLine;
                _msj += "DFA Emitidos                 =[" + _zeta.CantidadDFAEmitidos + "]" + Environment.NewLine;
                _msj += "DFBC Emitidos                =[" + _zeta.CantidadDFBCEmitidos + "]" + Environment.NewLine;
                _msj += "DF Cancelados                =[" + _zeta.CantidadDFCancelados + "]" + Environment.NewLine;
                _msj += "DF Impuestos Internos        =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoImpInternosDocFiscal)) + "]" + Environment.NewLine;
                _msj += "DF Monto IVA                 =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoIVADocFiscal)) + "]" + Environment.NewLine;
                _msj += "DF Monto IVA No Inscripto    =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoIVANoInscriptoDocFiscal)) + "]" + Environment.NewLine;
                _msj += "DF Monto Percepciones        =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoPercepcionesDocFiscal)) + "]" + Environment.NewLine;
                _msj += "DF Monto Ventas              =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoVentasDocFiscal)) + "]" + Environment.NewLine;
                _msj += "Ultimo DF A                  =[" + string.Format("{0}", Convert.ToInt32(_zeta.UltimoDocFiscalA)) + "]" + Environment.NewLine;
                _msj += "Ultimo DF BC                 =[" + string.Format("{0}", Convert.ToInt32(_zeta.UltimoDocFiscalBC)) + "]" + Environment.NewLine;
                _msj += "NCA Emitidas                 =[" + _zeta.CantidadNCAEmitidas + "]" + Environment.NewLine;
                _msj += "NCBC Emitidas                =[" + _zeta.CantidadNCBCEmitidas + "]" + Environment.NewLine;
                _msj += "NCC Canceladas               =[" + _zeta.CantidadNCCanceladas + "]" + Environment.NewLine;
                _msj += "NC Impuestos Internos        =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoImpInternosNotaCredito)) + "]" + Environment.NewLine;
                _msj += "NC Monto IVA                 =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoIVANotaCredito)) + "]" + Environment.NewLine;
                _msj += "NC Monto IVA No Inscripto    =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoIVANoInscriptoNotaCredito)) + "]" + Environment.NewLine;
                _msj += "NC Monto Percepciones        =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoPercepcionesNotaCredito)) + "]" + Environment.NewLine;
                _msj += "NC Monto Ventas              =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoVentasNotaCredito)) + "]" + Environment.NewLine;
                _msj += "Ultimo Remito                =[" + string.Format("{0}", (int)_zeta.UltimoRemito) + "]" + Environment.NewLine;

                return _msj;

            }
            catch (Exception ex)
            {
                // Cancelamos
                Acciones.Cancelar(hasar);
                // Mandamos la Excepcion
                throw new Exception(ex.Message);
            }
        }

        public static string ImprimirZPorNumero(long _nroZeta ) {
            FiscalPrinterLib.HASAR hasar = new FiscalPrinterLib.HASAR();
            RespuestaCerrarJornadaFiscal _cierre = new RespuestaCerrarJornadaFiscal();
            CerrarJornadaFiscalZPorNumero _zeta = new CerrarJornadaFiscalZPorNumero();
            string _msj;

            try
            {
                // Conectamos.
                Acciones.Conectar(hasar, Acciones.modelo, Acciones.puerto, Acciones.baudios);
                // ejecutamos el cierre Z.
                
                hasar.ReporteZIndividualPorNumero(
                    (int)_nroZeta,
                    out _zeta.FechaZ,
                    out _zeta.NumeroJornada,
                    out _zeta.UltimoDocFiscalBC,
                    out _zeta.UltimoDocFiscalA,
                    out _zeta.MontoVentasDocFiscal,
                    out _zeta.MontoIVADocFiscal,
                    out _zeta.MontoImpInternosDocFiscal,
                    out _zeta.MontoPercepcionesDocFiscal,
                    out _zeta.MontoIVANoInscriptoDocFiscal,
                    out _zeta.UltimaNotaCreditoBC,
                    out _zeta.UltimaNotaCreditoA,
                    out _zeta.MontoVentasNotaCredito,
                    out _zeta.MontoIVANotaCredito,
                    out _zeta.MontoImpInternosNotaCredito,
                    out _zeta.MontoPercepcionesNotaCredito,
                    out _zeta.MontoIVANoInscriptoNotaCredito,
                    out _zeta.UltimoRemito
                    );        

                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine;
                _msj += "Cierre 'Z' Nº                =[" + _zeta.NumeroZeta + "]" + Environment.NewLine;
                _msj += "Fecha del Cierre             =[" + _zeta.FechaZ.ToString() + "]" + Environment.NewLine;
                _msj += "DF Impuestos Internos        =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoImpInternosDocFiscal)) + "]" + Environment.NewLine;
                _msj += "DF Monto IVA                 =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoIVADocFiscal)) + "]" + Environment.NewLine;
                _msj += "DF Monto IVA No Inscripto    =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoIVANoInscriptoDocFiscal)) + "]" + Environment.NewLine;
                _msj += "DF Monto Percepciones        =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoPercepcionesDocFiscal)) + "]" + Environment.NewLine;
                _msj += "DF Monto Ventas              =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoVentasDocFiscal)) + "]" + Environment.NewLine;
                _msj += "Ultimo DF A                  =[" + string.Format("{0}", Convert.ToInt32(_zeta.UltimoDocFiscalA)) + "]" + Environment.NewLine;
                _msj += "Ultimo DF BC                 =[" + string.Format("{0}", Convert.ToInt32(_zeta.UltimoDocFiscalBC)) + "]" + Environment.NewLine;
                _msj += "NC Impuestos Internos        =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoImpInternosNotaCredito)) + "]" + Environment.NewLine;
                _msj += "NC Monto IVA                 =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoIVANotaCredito)) + "]" + Environment.NewLine;
                _msj += "NC Monto IVA No Inscripto    =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoIVANoInscriptoNotaCredito)) + "]" + Environment.NewLine;
                _msj += "NC Monto Percepciones        =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoPercepcionesNotaCredito)) + "]" + Environment.NewLine;
                _msj += "NC Monto Ventas              =[" + string.Format("{0:0.00}", Convert.ToDecimal(_zeta.MontoVentasNotaCredito)) + "]" + Environment.NewLine;
                _msj += "Ultimo Remito                =[" + string.Format("{0}", (int)_zeta.UltimoRemito) + "]" + Environment.NewLine;

                return _msj;

            }
            catch (Exception ex)
            {
                // Cancelamos
                Acciones.Cancelar(hasar);
                // Mandamos la Excepcion
                throw new Exception(ex.Message);
            }
        }
    }
}
