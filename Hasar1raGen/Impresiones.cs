﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Linq;

namespace Hasar1raGen
{
    public class Impresiones
    {
        
        public static string ImprimirNotaCreditoAB(Cabecera _cab, List<Items> _items, List<Pagos> _pagos, List<OtrosTributos> _oTributos,
                List<Recargos> _recargos, List<DocumentoAsociado> _docAsoc, SqlConnection _sqlConn, out bool _faltaPapel, out bool _necesitaCierreZ,
                out string _nroComprobante)
        {
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            string _nombreVendedor = "";
            _nroComprobante = "";

            _necesitaCierreZ = false;
            _faltaPapel = false;

            FiscalPrinterLib.HASAR hasar = new FiscalPrinterLib.HASAR();

            hasar.ErrorFiscal += new FiscalPrinterLib._FiscalEvents_ErrorFiscalEventHandler(ofh_ErrorFiscal);
            hasar.ErrorImpresora += new FiscalPrinterLib._FiscalEvents_ErrorImpresoraEventHandler(ofh_ErrorImpresora);
            hasar.FaltaPapel += new FiscalPrinterLib._FiscalEvents_FaltaPapelEventHandler(ofh_FaltaPapel);
            hasar.ImpresoraNoResponde += new FiscalPrinterLib._FiscalEvents_ImpresoraNoRespondeEventHandler(ofh_ImpresoraNoResponde);
            hasar.ProgresoDeteccion += new FiscalPrinterLib._FiscalEvents_ProgresoDeteccionEventHandler(ofh_ProgresoDeteccion);

            try
            {
                try
                {
                    Acciones.Conectar(hasar, Acciones.modelo, Acciones.puerto, Acciones.baudios);
                }
                catch (Exception exc)
                {
                    throw new Exception("Error conectando: " + exc.Message);
                }

                if (FuncionesVarias.FechaOk(hasar, _cab))
                {                    
                    try
                    {
                        if (_cab.regfacturacioncliente != 1)
                        {
                            if (_cab.regfacturacioncliente == 6)
                            {
                                hasar.DatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    FiscalPrinterLib.TiposDeDocumento.TIPO_CUIT,
                                    FiscalPrinterLib.TiposDeResponsabilidades.MONOTRIBUTO,
                                    _cab.direccioncliente);
                            }
                            else
                            {
                                hasar.DatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    FiscalPrinterLib.TiposDeDocumento.TIPO_DNI,
                                    FiscalPrinterLib.TiposDeResponsabilidades.CONSUMIDOR_FINAL,
                                    _cab.direccioncliente);
                            }
                            
                            // Documentos Asociados.
                            int _counter = 1;
                            if (_docAsoc.Count > 0)
                            {
                                foreach (DocumentoAsociado _doc in _docAsoc)
                                {
                                    switch (_doc._seriefiscal2.Substring(0, 3).ToUpper())
                                    {
                                        case "001":
                                            {
                                                hasar.set_DocumentoDeReferencia(1, "FA " + _doc._seriefiscal1 + _doc._numerofiscal);
                                                break;
                                            }
                                        case "006":
                                            {
                                                hasar.set_DocumentoDeReferencia(1, "FB " + _doc._seriefiscal1 + _doc._numerofiscal);
                                                break;
                                            }
                                        default:
                                            {
                                                hasar.set_DocumentoDeReferencia(1, "GE " + _doc._seriefiscal1 + _doc._numerofiscal);
                                                break;
                                            }
                                    }
                                    _counter++;
                                }
                            }
                            else
                            {
                                hasar.set_DocumentoDeReferencia(1, "GE 000000000000");
                            }
                            
                            // Documento de Asociado. Debe haber alguno.
                            if (_counter == 0) {
                                hasar.set_DocumentoDeReferencia(1, "TICKET NO DISPONIBLE");
                            }
                            //Abro NCB.
                            hasar.AbrirDNFH(FiscalPrinterLib.DocumentosNoFiscales.TICKET_NOTA_CREDITO_B);
                            _serieFiscal2 = "008_Credito_B";

                        }
                        else
                        {
                            hasar.DatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                FiscalPrinterLib.TiposDeDocumento.TIPO_CUIT,
                                FiscalPrinterLib.TiposDeResponsabilidades.RESPONSABLE_INSCRIPTO,
                                _cab.direccioncliente);

                            // Documentos Asociados.
                            int _counter = 0;
                            foreach (DocumentoAsociado _doc in _docAsoc)
                            {
                                switch (_doc._seriefiscal2.Substring(0, 3).ToUpper())
                                {
                                    case "001":
                                        {
                                            hasar.set_DocumentoDeReferencia(1, "FA " + _doc._seriefiscal1 + _doc._numerofiscal);
                                            break;
                                        }
                                    case "006":
                                        {
                                            hasar.set_DocumentoDeReferencia(1, "FB " + _doc._seriefiscal1 + _doc._numerofiscal);
                                            break;
                                        }
                                    default:
                                        {
                                            hasar.set_DocumentoDeReferencia(1, "GE " + _doc._seriefiscal1 + _doc._numerofiscal);
                                            break;
                                        }
                                }
                                _counter++;
                            }

                            // Documento de Asociado. Debe haber alguno.
                            if (_counter == 0)
                            {
                                hasar.set_DocumentoDeReferencia(1, "TICKET NO DISPONIBLE");
                            }
                            //Abro NCA.
                            hasar.AbrirDNFH(FiscalPrinterLib.DocumentosNoFiscales.TICKET_NOTA_CREDITO_A);
                            _serieFiscal2 = "003_Credito_A";
                        }

                        var lengths = from element in _items
                                      orderby element._cantidad ascending
                                      select element;

                        //foreach (Items _it in _items)
                        foreach (Items _it in lengths)
                        {
                            if (!String.IsNullOrEmpty(_it._codBarra))
                                hasar.ImprimirTextoFiscal("SKU " + _it._codBarra);

                            if (String.IsNullOrEmpty(_it._descripcion))
                                _it._descripcion = "Articulo sin Descripcion";

                            // si tenemos precio = 0 o DTO2 = 100 entonces imprimo el texto de la promo.
                            if (_it._precioDefecto == 0 && _it._dto2 == 100)
                            {
                                string _txt = _it._textoPromo + " Importe: " + _it._precioDefecto.ToString();
                                hasar.ImprimirTextoFiscal(_txt);
                            }

                            //Imprimo el Item.
                            if (_it._cantidad > 0)
                            {
                                //ayer
                                if (_it._descuento == 100)
                                {
                                    hasar.ImprimirItem(_it._descripcion,
                                     Math.Abs(Convert.ToDouble(_it._cantidad)),
                                         Convert.ToDouble(0),
                                     Math.Abs(Convert.ToDouble(_it._iva)),
                                     Math.Abs(Convert.ToDouble(0)));
                                }
                                else
                                {
                                    hasar.ImprimirItem(_it._descripcion,
                                     Math.Abs(Convert.ToDouble(_it._cantidad)),
                                         Convert.ToDouble(_it._precioDefecto * -1),
                                     Math.Abs(Convert.ToDouble(_it._iva)),
                                     Math.Abs(Convert.ToDouble(0)));
                                }
                                //Fin ayer
                            }
                            else
                            {
                                hasar.ImprimirItem(_it._descripcion,
                                    Math.Abs(Convert.ToDouble(_it._cantidad)),
                                        Math.Abs(Convert.ToDouble(_it._precioDefecto)),
                                    Math.Abs(Convert.ToDouble(_it._iva)),
                                    Math.Abs(Convert.ToDouble(0)));
                            }
                            // Ver como aplicarlo.
                            // Veo si tengo dto en el item.
                            if (_it._descuento > 0)
                            {
                                double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precioDefecto) * _it._descuento / 100)));
                                // Valido que tengo texto descuento.
                                if (String.IsNullOrEmpty(_it._textoPromo))
                                {
                                    if (_it._descuento == 0)
                                        _it._textoPromo = "Descuento ";
                                    else
                                        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                }
                                hasar.DescuentoUltimoItem(_it._textoPromo, _dto, true);
                            }
                            //Asingo nombre
                            _nombreVendedor = _it._nomVendedor;
                        }

                        //Recupero el total del CF
                        object items;
                        object ventas;
                        object iva;
                        object pagado;
                        object ivanoi;
                        object impint;
                        hasar.Subtotal(false, out items, out ventas, out iva, out pagado, out ivanoi, out impint);

                        // Vemos si tenemos dto en la cabecera.
                        if (_cab.dtocomercial > 0)
                        {
                            var _ventas = (double)ventas;
                            if (_cab.totalneto == 0)
                                _ventas = _ventas - 0.01;
                            hasar.DescuentoGeneral("Bonificación Gral. (comercial) " + _cab.dtocomercial.ToString(), (_cab.dtocomercial / 100) * _ventas, true);
//                            hasar.DescuentoGeneral("Bonificación Gral. (comercial) " + _cab.dtocomercial.ToString(),
//                                Math.Abs(_cab.totdtocomercial), true);
                        }

                        if (_cab.dtopp != 0)
                        {
                            if (_cab.dtopp > 0)
                            {
                                double _impdto = (_cab.dtopp * (double)ventas) / 100;
                                hasar.DescuentoGeneral("Bonificación ",
                                    (_cab.dtopp / 100) * (double)ventas,
                                    true);
                            }
                            else
                            {
                                double _dto = Math.Abs(_cab.dtopp);
                                double _impdto = (_dto * (double)ventas) / 100;
                                hasar.DescuentoGeneral("Recargo ",
                                   _impdto,
                                    false);
                            }
                        }

                        // Recargos
                        foreach (Recargos _rec in _recargos)
                        {
                            //double _importeRecargo = (_rec._bruto * _rec._recargo / 100);
                            hasar.DescuentoGeneral(_rec._texto, Math.Abs(_rec._importe), false);
                        }
                       
                         // Recargosutos
                        foreach (OtrosTributos _ot in _oTributos)
                        {
                            double _impor = Math.Abs(_ot._importe);
                            double _bs = Math.Abs(_ot._base);
                            //hasar.DescuentoGeneral(_ot._nombre, _ot._importe, false);
                            hasar.EspecificarPercepcionGlobal(_ot._nombre, _impor);

                        }

                        // Valido los importes.
                        if (!FuncionesVarias.ComparoSubtotal(hasar, _cab.totalneto))
                        {
                            throw new Exception("Los totales no coinciden. Revise el comprobante y luego reintente imprimirlo.");
                        }
                        
                        // Pagos.
                        foreach (Pagos _pag in _pagos)
                        {
                            object resto = null;
                            switch (_pag.tipopago.ToUpper())
                            {
                                case "TARJETA CRÉDITO":
                                case "TARJETA CREDITO":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    "",
                                    out resto);
                                        break;
                                    }
                                case "PENDIENTE":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    "", out resto);
                                        break;
                                    }
                                case "EFECTIVO":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                            Math.Abs(Convert.ToDouble(_pag.monto)),
                                            "",
                                            out resto);
                                        _abrirCajon = true;
                                        break;
                                    }
                                case "CHEQUE":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    "",
                                    out resto);
                                        break;
                                    }
                                case "TARJETA DEBITO":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    "",
                                    out resto);
                                        break;
                                    }
                                default:
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    "",
                                    out resto);
                                        break;
                                    }
                            }
                        }

                        object _numeroTicket = 0;

                        //Imprimo Vendedor.
                        hasar.Encabezado[11] = "Vendedor: " + _nombreVendedor;
                        hasar.Encabezado[12] = "Serie / Nro: " + _cab.numserie + "/" + _cab.numfac.ToString();
                        //Cierro comprobante.
                        hasar.CerrarDNFH(1, out _numeroTicket);
                        //Limpio Vendedor.
                        hasar.Encabezado[11] = ((char)127).ToString();
                        hasar.Encabezado[12] = ((char)127).ToString();

                        object cuit = null;
                        object rsocial = null;
                        object serie = null;
                        object fechaini = null;
                        object ptovta = null;
                        object iniact = null;
                        object ingbr = null;
                        object categ = null;

                        hasar.ObtenerDatosDeInicializacion(out cuit, out rsocial, out serie, out fechaini, out ptovta, out iniact, out ingbr, out categ);

                        string _numeroPos = ptovta.ToString().PadLeft(4, '0');

                        bool _ok = FacturasVentaSerieResol.GrabarTiquet(_cab.numserie, _cab.numfac, _cab.n, _numeroPos, _serieFiscal2, (int)_numeroTicket, _sqlConn);

                        _nroComprobante = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');

                        if (!_ok)
                        {
                            string _numeroError = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                            _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                        }

                        if (hasar.HuboFaltaPapel)
                            _faltaPapel = true;

                        if (_abrirCajon)
                            hasar.AbrirCajonDeDinero();
                    }
                    catch (Exception ex)
                    {
                        // Vemos si el error fue porque necesita el Cierre Z.
                        if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                        {
                            _necesitaCierreZ = true;
                            _rta = "Cierre Z. ";
                        }
                        Acciones.Cancelar(hasar);
                        throw new Exception(_rta + ex.Message);
                    }
                }
                else
                    _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";
            }
            catch (SystemException exc)  // Tratamiento de las excepciones disparadas...
            {
                throw new Exception(exc.Source + "  :::  " + exc.Message);
            }

            return _rta;
        }

        public static string ImprimirNotaCreditoABMixta(Cabecera _cab, List<Items> _items, List<Pagos> _pagos, List<OtrosTributos> _oTributos,
        List<Recargos> _recargos, List<DocumentoAsociado> _docAsoc, SqlConnection _sqlConn, out bool _faltaPapel, out bool _necesitaCierreZ,
        out string _nroComprobante)
        {
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            string _nombreVendedor = "";
            _nroComprobante = "";

            _necesitaCierreZ = false;
            _faltaPapel = false;

            FiscalPrinterLib.HASAR hasar = new FiscalPrinterLib.HASAR();

            hasar.ErrorFiscal += new FiscalPrinterLib._FiscalEvents_ErrorFiscalEventHandler(ofh_ErrorFiscal);
            hasar.ErrorImpresora += new FiscalPrinterLib._FiscalEvents_ErrorImpresoraEventHandler(ofh_ErrorImpresora);
            hasar.FaltaPapel += new FiscalPrinterLib._FiscalEvents_FaltaPapelEventHandler(ofh_FaltaPapel);
            hasar.ImpresoraNoResponde += new FiscalPrinterLib._FiscalEvents_ImpresoraNoRespondeEventHandler(ofh_ImpresoraNoResponde);
            hasar.ProgresoDeteccion += new FiscalPrinterLib._FiscalEvents_ProgresoDeteccionEventHandler(ofh_ProgresoDeteccion);

            try
            {
                try
                {
                    Acciones.Conectar(hasar, Acciones.modelo, Acciones.puerto, Acciones.baudios);
                }
                catch (Exception exc)
                {
                    throw new Exception("Error conectando: " + exc.Message);
                }

                if (FuncionesVarias.FechaOk(hasar, _cab))
                {
                    try
                    {
                        if (_cab.regfacturacioncliente != 1)
                        {
                            if (_cab.regfacturacioncliente == 6)
                            {
                                hasar.DatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    FiscalPrinterLib.TiposDeDocumento.TIPO_CUIT,
                                    FiscalPrinterLib.TiposDeResponsabilidades.MONOTRIBUTO,
                                    _cab.direccioncliente);
                            }
                            else
                            {
                                hasar.DatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    FiscalPrinterLib.TiposDeDocumento.TIPO_DNI,
                                    FiscalPrinterLib.TiposDeResponsabilidades.CONSUMIDOR_FINAL,
                                    _cab.direccioncliente);
                            }

                            // Documentos Asociados.
                            int _counter = 1;
                            if (_docAsoc.Count > 0)
                            {
                                foreach (DocumentoAsociado _doc in _docAsoc)
                                {
                                    switch (_doc._seriefiscal2.Substring(0, 3).ToUpper())
                                    {
                                        case "001":
                                            {
                                                hasar.set_DocumentoDeReferencia(1, "FA " + _doc._seriefiscal1 + _doc._numerofiscal);
                                                break;
                                            }
                                        case "006":
                                            {
                                                hasar.set_DocumentoDeReferencia(1, "FB " + _doc._seriefiscal1 + _doc._numerofiscal);
                                                break;
                                            }
                                        default:
                                            {
                                                hasar.set_DocumentoDeReferencia(1, "GE " + _doc._seriefiscal1 + _doc._numerofiscal);
                                                break;
                                            }
                                    }
                                    _counter++;
                                }
                            }
                            else
                            {
                                hasar.set_DocumentoDeReferencia(1, "GE 000000000000");
                            }

                            // Documento de Asociado. Debe haber alguno.
                            if (_counter == 0)
                            {
                                hasar.set_DocumentoDeReferencia(1, "TICKET NO DISPONIBLE");
                            }
                            //Abro NCB.
                            hasar.AbrirDNFH(FiscalPrinterLib.DocumentosNoFiscales.TICKET_NOTA_CREDITO_B);
                            _serieFiscal2 = "008_Credito_B";

                        }
                        else
                        {
                            hasar.DatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                FiscalPrinterLib.TiposDeDocumento.TIPO_CUIT,
                                FiscalPrinterLib.TiposDeResponsabilidades.RESPONSABLE_INSCRIPTO,
                                _cab.direccioncliente);

                            // Documentos Asociados.
                            int _counter = 0;
                            foreach (DocumentoAsociado _doc in _docAsoc)
                            {
                                switch (_doc._seriefiscal2.Substring(0, 3).ToUpper())
                                {
                                    case "001":
                                        {
                                            hasar.set_DocumentoDeReferencia(1, "FA " + _doc._seriefiscal1 + _doc._numerofiscal);
                                            break;
                                        }
                                    case "006":
                                        {
                                            hasar.set_DocumentoDeReferencia(1, "FB " + _doc._seriefiscal1 + _doc._numerofiscal);
                                            break;
                                        }
                                    default:
                                        {
                                            hasar.set_DocumentoDeReferencia(1, "GE " + _doc._seriefiscal1 + _doc._numerofiscal);
                                            break;
                                        }
                                }
                                _counter++;
                            }

                            // Documento de Asociado. Debe haber alguno.
                            if (_counter == 0)
                            {
                                hasar.set_DocumentoDeReferencia(1, "TICKET NO DISPONIBLE");
                            }
                            //Abro NCA.
                            hasar.AbrirDNFH(FiscalPrinterLib.DocumentosNoFiscales.TICKET_NOTA_CREDITO_A);
                            _serieFiscal2 = "003_Credito_A";
                        }

                        var lengths = from element in _items
                                      orderby element._cantidad ascending
                                      select element;

                        decimal _importeDescGralItems = 0;

                        foreach (Items _it in lengths)
                        {
                            if (!String.IsNullOrEmpty(_it._codBarra))
                                hasar.ImprimirTextoFiscal("SKU " + _it._codBarra);

                            if (String.IsNullOrEmpty(_it._descripcion))
                                _it._descripcion = "Articulo sin Descripcion";

                            // si tenemos precio = 0 o DTO2 = 100 entonces imprimo el texto de la promo.
                            if (_it._precioDefecto == 0 && _it._dto2 == 100)
                            {
                                string _txt = _it._textoPromo + " Importe: " + _it._precioDefecto.ToString();
                                hasar.ImprimirTextoFiscal(_txt);
                            }

                            //Imprimo el Item.
                            if (_it._cantidad > 0)
                            {
                                    hasar.ImprimirItem(_it._descripcion,
                                     Math.Abs(Convert.ToDouble(_it._cantidad)),
                                     Convert.ToDouble(0.01),
                                     Math.Abs(Convert.ToDouble(_it._iva)),
                                     Math.Abs(Convert.ToDouble(0)));

                                if (_it._descuento > 0)
                                {
                                    decimal _totItem = Math.Abs(_it._cantidad) * _it._precioDefecto;
                                    decimal _dto = Math.Abs(((_it._cantidad * _it._precioDefecto) * _it._descuento / 100));
                                    decimal _impDto = _totItem - _dto;
                                    _importeDescGralItems = _importeDescGralItems + _impDto + Convert.ToDecimal(0.01);
                                }
                                else
                                {
                                    _importeDescGralItems = _importeDescGralItems + _it._precioDefecto + Convert.ToDecimal(0.01);
                                }
                            }
                            else
                            {
                                hasar.ImprimirItem(_it._descripcion,
                                    Math.Abs(Convert.ToDouble(_it._cantidad)),
                                        Math.Abs(Convert.ToDouble(_it._precioDefecto)),
                                    Math.Abs(Convert.ToDouble(_it._iva)),
                                    Math.Abs(Convert.ToDouble(0)));
                            }
                            // Ver como aplicarlo.
                            // Veo si tengo dto en el item.
                            if (_it._descuento > 0)
                            {
                                if (_it._cantidad < 0)
                                {
                                    double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precioDefecto) * _it._descuento / 100)));
                                    // Valido que tengo texto descuento.
                                    if (String.IsNullOrEmpty(_it._textoPromo))
                                    {
                                        if (_it._descuento == 0)
                                            _it._textoPromo = "Descuento ";
                                        else
                                            _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                    }
                                    //imprimo el descuento.
                                    hasar.DescuentoUltimoItem(_it._textoPromo, _dto, true);
                                }
                            }
                            //Asingo nombre
                            _nombreVendedor = _it._nomVendedor;
                        }

                        //Descontamos los items positivos.
                        if (_importeDescGralItems > 0)
                        {
                            hasar.DescuentoGeneral("Por Cambios", Convert.ToDouble(_importeDescGralItems), true);
                        }

                        //Recupero el subtotal del CF.
                        object items;
                        object ventas;
                        object iva;
                        object pagado;
                        object ivanoi;
                        object impint;
                        hasar.Subtotal(false, out items, out ventas, out iva, out pagado, out ivanoi, out impint);

                        // Vemos si tenemos dto en la cabecera.
                        if (_cab.dtocomercial > 0)
                        {
                            var _ventas = (double)ventas;
                            if (_cab.totalneto == 0)
                                _ventas = _ventas - 0.01;
                            hasar.DescuentoGeneral("Bonificación Gral. (comercial) " + _cab.dtocomercial.ToString(), (_cab.dtocomercial / 100) * _ventas, true);
//                            hasar.DescuentoGeneral("Bonificación Gral. (comercial) " + _cab.dtocomercial.ToString(),
//                                Math.Abs(_cab.totdtocomercial), true);
                        }                        

                        if (_cab.dtopp != 0)
                        {
                            if (_cab.dtopp > 0)
                            {
                                double _impdto = (_cab.dtopp * (double)ventas) / 100;
                                hasar.DescuentoGeneral("Bonificación ",
                                    (_cab.dtopp / 100) * (double)ventas,
                                    true);
                            }
                            else
                            {
                                double _dto = Math.Abs(_cab.dtopp);
                                double _impdto = (_dto * (double)ventas) / 100;
                                hasar.DescuentoGeneral("Recargo ",
                                   _impdto,
                                    false);
                            }
                        }

                        // Recargos
                        foreach (Recargos _rec in _recargos)
                        {
                            //double _importeRecargo = (_rec._bruto * _rec._recargo / 100);
                            hasar.DescuentoGeneral(_rec._texto, Math.Abs(_rec._importe), false);
                        }

                        // Recargosutos
                        foreach (OtrosTributos _ot in _oTributos)
                        {
                            double _impor = Math.Abs(_ot._importe);
                            double _bs = Math.Abs(_ot._base);
                            //hasar.DescuentoGeneral(_ot._nombre, _ot._importe, false);
                            hasar.EspecificarPercepcionGlobal(_ot._nombre, _impor);

                        }

                        // Valido los importes.
                        if (!FuncionesVarias.ComparoSubtotal(hasar, _cab.totalneto))
                        {
                            throw new Exception("Los totales no coinciden. Revise el comprobante y luego reintente imprimirlo.");
                        }

                        // Pagos.
                        foreach (Pagos _pag in _pagos)
                        {
                            object resto = null;
                            switch (_pag.tipopago.ToUpper())
                            {
                                case "TARJETA CRÉDITO":
                                case "TARJETA CREDITO":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    "",
                                    out resto);
                                        break;
                                    }
                                case "PENDIENTE":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    "", out resto);
                                        break;
                                    }
                                case "EFECTIVO":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                            Math.Abs(Convert.ToDouble(_pag.monto)),
                                            "",
                                            out resto);
                                        _abrirCajon = true;
                                        break;
                                    }
                                case "CHEQUE":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    "",
                                    out resto);
                                        break;
                                    }
                                case "TARJETA DEBITO":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    "",
                                    out resto);
                                        break;
                                    }
                                default:
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    "",
                                    out resto);
                                        break;
                                    }
                            }
                        }

                        object _numeroTicket = 0;

                        //Imprimo Vendedor.
                        hasar.Encabezado[11] = "Vendedor: " + _nombreVendedor;
                        hasar.Encabezado[12] = "Serie / Nro: " + _cab.numserie + "/" + _cab.numfac.ToString();
                        //Cierro comprobante.
                        hasar.CerrarDNFH(1, out _numeroTicket);
                        //Limpio Vendedor.
                        hasar.Encabezado[11] = ((char)127).ToString();
                        hasar.Encabezado[12] = ((char)127).ToString();

                        object cuit = null;
                        object rsocial = null;
                        object serie = null;
                        object fechaini = null;
                        object ptovta = null;
                        object iniact = null;
                        object ingbr = null;
                        object categ = null;

                        hasar.ObtenerDatosDeInicializacion(out cuit, out rsocial, out serie, out fechaini, out ptovta, out iniact, out ingbr, out categ);

                        string _numeroPos = ptovta.ToString().PadLeft(4, '0');

                        bool _ok = FacturasVentaSerieResol.GrabarTiquet(_cab.numserie, _cab.numfac, _cab.n, _numeroPos, _serieFiscal2, (int)_numeroTicket, _sqlConn);

                        _nroComprobante = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');

                        if (!_ok)
                        {
                            string _numeroError = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                            _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                        }

                        if (hasar.HuboFaltaPapel)
                            _faltaPapel = true;

                        if (_abrirCajon)
                            hasar.AbrirCajonDeDinero();
                    }
                    catch (Exception ex)
                    {
                        // Vemos si el error fue porque necesita el Cierre Z.
                        if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                        {
                            _necesitaCierreZ = true;
                            _rta = "Cierre Z. ";
                        }
                        Acciones.Cancelar(hasar);
                        throw new Exception(_rta + ex.Message);
                    }
                }
                else
                    _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";
            }
            catch (SystemException exc)  // Tratamiento de las excepciones disparadas...
            {
                throw new Exception(exc.Source + "  :::  " + exc.Message);
            }

            return _rta;
        }

        public static string ImprimirFacturasAB(Cabecera _cab, List<Items> _items, List<Pagos> _pagos, List<OtrosTributos> _tributos,
            List<Recargos> _recargos, SqlConnection _sqlConn, string _textoAjuste,  out bool _faltaPapel, out bool _necesitaCierreZ, out string _nroComprobante)
        {
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            string _nombreVendedor = "";
            _faltaPapel = false;
            _necesitaCierreZ = false;
            _nroComprobante = "";

            // aca Comienzo
            FiscalPrinterLib.HASAR hasar = new FiscalPrinterLib.HASAR();

            hasar.ErrorFiscal += new FiscalPrinterLib._FiscalEvents_ErrorFiscalEventHandler(ofh_ErrorFiscal);
            hasar.ErrorImpresora += new FiscalPrinterLib._FiscalEvents_ErrorImpresoraEventHandler(ofh_ErrorImpresora);
            hasar.FaltaPapel += new FiscalPrinterLib._FiscalEvents_FaltaPapelEventHandler(ofh_FaltaPapel);
            hasar.ImpresoraNoResponde += new FiscalPrinterLib._FiscalEvents_ImpresoraNoRespondeEventHandler(ofh_ImpresoraNoResponde);
            hasar.ProgresoDeteccion += new FiscalPrinterLib._FiscalEvents_ProgresoDeteccionEventHandler(ofh_ProgresoDeteccion);
            
            try
            {
                // Conectamos con la hasar.
                Acciones.Conectar(hasar);
            
                if (FuncionesVarias.FechaOk(hasar, _cab))
                {
                    if (_cab.regfacturacioncliente != 1)
                    {
                        if (_cab.regfacturacioncliente == 6)
                        {
                            hasar.DatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                FiscalPrinterLib.TiposDeDocumento.TIPO_CUIT,
                                FiscalPrinterLib.TiposDeResponsabilidades.MONOTRIBUTO,
                                _cab.direccioncliente);
                        }
                        else
                        {
                            hasar.DatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                FiscalPrinterLib.TiposDeDocumento.TIPO_DNI,
                                FiscalPrinterLib.TiposDeResponsabilidades.CONSUMIDOR_FINAL,
                                _cab.direccioncliente);
                        }
                        hasar.AbrirComprobanteFiscal(FiscalPrinterLib.DocumentosFiscales.TICKET_FACTURA_B);
                        _serieFiscal2 = "006_Factura_B";
                    }
                    else
                    {
                        hasar.DatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                            FiscalPrinterLib.TiposDeDocumento.TIPO_CUIT,
                            FiscalPrinterLib.TiposDeResponsabilidades.RESPONSABLE_INSCRIPTO,
                            _cab.direccioncliente);

                        hasar.AbrirComprobanteFiscal(FiscalPrinterLib.DocumentosFiscales.TICKET_FACTURA_A);
                        _serieFiscal2 = "001_Factura_A";
                    }

                    // Validamos por la total si es cero, imprimos un ajuste.
                    if (_cab.totalneto == 0)
                    {
                        hasar.ImprimirItem(_textoAjuste, 1.0, 0.01, 21.0, 0.0);
                    }

                    // Imprimo los items de compra
                    foreach (Items _it in _items)
                    {
                        //MessageBox.Show("item");
                        if (_it._cantidad >= 0)
                        {
                            if(_it._precioDefecto > 0)
                                ImprimirItem(hasar, _it);
                            else
                            {
                                double _imp = Convert.ToDouble(_it._cantidad * _it._precioDefecto);
                                //MessageBox.Show(_imp.ToString());
                                hasar.DevolucionDescuento("SKU " + _it._codBarra + " " + _it._descripcion + " " + _it._cantidad, _imp,
                                    (double)_it._iva, 0.0,
                                    false,
                                    FiscalPrinterLib.TiposDeDescuentos.DESCUENTO_RECARGO);
                            }
                        }
                        else
                        {
                            if (_it._cantidad < 0)
                            {
                                hasar.kIVA = false;
                                hasar.ImpuestoInternoFijo = false;
                                hasar.ImpuestoInternoPorMonto = false;
                                if (_it._descuento > 0)
                                {
                                    double _dto;
                                    double _imp;
                                    _dto = Convert.ToDouble(((Math.Abs(_it._cantidad) * _it._precioDefecto) * _it._descuento / 100));
                                    _imp = Convert.ToDouble((Math.Abs(_it._cantidad) * _it._precioDefecto)) - _dto;
                                    hasar.DevolucionDescuento("SKU " + _it._codBarra + " " + _it._descripcion + " " + _it._cantidad, _imp,
                                    (double)_it._iva, 0.0,
                                    true,
                                    FiscalPrinterLib.TiposDeDescuentos.DESCUENTO_RECARGO);
                                }
                                else
                                {
                                    double _imp = Convert.ToDouble(Math.Abs(_it._cantidad) * _it._precioDefecto);
                                    hasar.DevolucionDescuento("SKU " + _it._codBarra + " " + _it._descripcion + " " + _it._cantidad, _imp,
                                        (double)_it._iva, 0.0,
                                        true,
                                        FiscalPrinterLib.TiposDeDescuentos.DESCUENTO_RECARGO);
                                }
                            }                            
                        }
                        _nombreVendedor = _it._nomVendedor;
                    }

                    foreach (var recargo in _recargos)
                    {
                        switch (recargo._codigo)
                        {
                            case "1":
                            case "2":
                                hasar.DescuentoGeneral(recargo._texto, recargo._importe, true);
                                break;
                            case "7":
                            case "9":
                                hasar.DescuentoGeneral(recargo._texto, recargo._importe, false);
                                break;
                        }
                    }

                    object items;
                    object ventas;
                    object iva;
                    object pagado;
                    object ivanoi;
                    object impint;
                    hasar.Subtotal(false, out items, out ventas, out iva, out pagado, out ivanoi, out impint);

                    // Vemos si tenemos dto en la cabecera.
                    if (_cab.dtocomercial > 0)
                    {
                        var _ventas = (double)ventas;
                        if (_cab.totalneto == 0)
                            _ventas = _ventas - 0.01;
                        //MessageBox.Show("DtoComercial");
                        //MessageBox.Show("Subtotal de ventas: " + _ventas.ToString());
                        //MessageBox.Show("DtoComercial: " + (_cab.dtocomercial / 100) * _ventas).ToString();
                        hasar.DescuentoGeneral("Bonificación Gral. (comercial) " + _cab.dtocomercial.ToString(), (_cab.dtocomercial / 100) * _ventas, true);
                    }

                    if (_cab.dtopp != 0)
                    {
                        //Consulto el subtotal y parseo las ventas
                        hasar.Subtotal(false, out items, out ventas, out iva, out pagado, out ivanoi, out impint);
                        var _ventas = (double)ventas;
                        //Vemos si agregamos un centavo y lo restamos.
                        if (_cab.totalneto == 0)
                            _ventas = _ventas - 0.01;

                        if (_cab.dtopp > 0)
                        {
                            //double _impdto = (_cab.dtopp * (double)ventas) / 100;
                            double _impdto = (_cab.dtopp * _ventas) / 100;
                            hasar.DescuentoGeneral("Bonificación " ,
                                (_cab.dtopp/100) * (double)ventas,
                                true);
                        }
                        else
                        {
                            double _dto = Math.Abs(_cab.dtopp);
                            //double _impdto = (_dto * (double)ventas) / 100;
                            double _impdto = (_dto * _ventas) / 100;
                            hasar.DescuentoGeneral("Recargo " ,
                               _impdto,
                                false);
                        }
                    }

                    // Recargos                    
                    foreach (Recargos _rec in _recargos)
                    {
                        hasar.DescuentoGeneral(_rec._texto, _rec._importe, false);
                    }

                    // Otros Tributos.
                    foreach (OtrosTributos _ot in _tributos)
                    {
                        double _impor = Math.Abs(_ot._importe);
                        hasar.EspecificarPercepcionGlobal(_ot._nombre, _impor);
                    }
                    
                    if (!FuncionesVarias.ComparoSubtotal(hasar, _cab.totalneto))
                    {
                        throw new Exception("Los totales no coinciden. Revise el comprobante y luego reintente imprimirlo.");
                    }
                    // Pagos.
                    //Flag para entregado.
                    bool _flagEntregado = false;
                    foreach (Pagos _pag in _pagos)
                    {
                        //Si el monto es menor a cero no lo imprimo
                        if (_pag.monto > 0)
                        {
                            object resto = null;
                            switch (_pag.tipopago.ToUpper())
                            {
                                case "TARJETA CRÉDITO":
                                case "TARJETA CREDITO":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.monto),
                                        "",
                                        out resto);
                                        break;
                                    }
                                case "PENDIENTE":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.monto),
                                        "",
                                        out resto);
                                        break;
                                    }
                                case "EFECTIVO":
                                    {
                                        //cambio 1/4/2019
                                        if (_pag.monto > 0)
                                        {
                                            if (_cab.entregado == 0)
                                            {
                                                hasar.ImprimirPago(_pag.descripcion,
                                                    Convert.ToDouble(_pag.monto),
                                                    "",
                                                    out resto);
                                            }
                                            else
                                            {
                                                if (_flagEntregado == false)
                                                {
                                                    hasar.ImprimirPago(_pag.descripcion,
                                                        Convert.ToDouble(_cab.entregado),
                                                        "",
                                                        out resto);
                                                    _flagEntregado = true;
                                                }
                                                else
                                                {
                                                    hasar.ImprimirPago(_pag.descripcion,
                                                    Convert.ToDouble(_pag.monto),
                                                    "",
                                                    out resto);
                                                }
                                            }
                                            _abrirCajon = true;
                                        }
                                        break;
                                    }
                                case "CHEQUE":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.monto),
                                        "",
                                        out resto);
                                        break;
                                    }
                                case "TARJETA DEBITO":
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.monto),
                                        "",
                                        out resto);
                                        break;
                                    }
                                default:
                                    {
                                        hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.monto),
                                        "",
                                        out resto);

                                        break;
                                    }
                            }
                        }
                    }

                    object _numeroTicket = 0;

                    //Ponemos el vendedor:
                    hasar.Encabezado[11] = "Vendedor: " + _nombreVendedor;
                    hasar.Encabezado[12] = "Serie / Nro.: " + _cab.numserie + "/" + _cab.numfac.ToString();

                    hasar.CerrarComprobanteFiscal(1, out _numeroTicket);

                    //Limpio el vendedor.
                    hasar.Encabezado[11] = ((char)127).ToString();
                    hasar.Encabezado[12] = ((char)127).ToString();

                    object cuit = null;
                    object rsocial = null;
                    object serie = null;
                    object fechaini = null;
                    object ptovta = null;
                    object iniact = null;
                    object ingbr = null;
                    object categ = null;
                    hasar.ObtenerDatosDeInicializacion(out cuit, out rsocial, out serie, out fechaini, out ptovta, out iniact, out ingbr, out categ);

                    string _numeroPos = ptovta.ToString().PadLeft(4, '0');

                    _nroComprobante = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8,'0');

                    bool _Ok = FacturasVentaSerieResol.GrabarTiquet(_cab.numserie, _cab.numfac, _cab.n, _numeroPos, _serieFiscal2, (int)_numeroTicket, _sqlConn);

                    if (!_Ok)
                    {
                        string _numeroError = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                    }

                    if (hasar.HuboFaltaPapel)
                    {
                        _faltaPapel = true;
                    }

                    // Vemos si abrimos el cajon
                    if (_abrirCajon)
                        hasar.AbrirCajonDeDinero();
                }
                else
                {
                    _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";
                }

            }

            catch (SystemException exc)  // Tratamiento de las excepciones disparadas...
            {
                MessageBox.Show(exc.Source + "  :::  " + exc.Message);
            }

            return _rta;
        }        

        private static void ImprimirItem(FiscalPrinterLib.HASAR hasar, Items _it)
        {
            if (!String.IsNullOrEmpty(_it._codBarra)) hasar.ImprimirTextoFiscal("SKU " + _it._codBarra);

            if (String.IsNullOrEmpty(_it._descripcion)) _it._descripcion = "Articulo Sin Descripcion";

            // si tenemos precio = 0 o DTO2 = 100 entonces imprimo el texto de la promo.
            if (_it._precioDefecto == 0 && _it._dto2 == 100)
            {
                string _txt = _it._textoPromo + " Importe: " + _it._precioDefecto.ToString();
                hasar.ImprimirTextoFiscal(_txt);
            }
            // Imprimo el item
            hasar.ImprimirItem(_it._descripcion,
                Convert.ToDouble(Math.Truncate(_it._cantidad)),
                Convert.ToDouble(Math.Truncate(_it._precioDefecto * 100) / 100),
                Convert.ToDouble(_it._iva),
                0.0 
                );

            // Si el Precio es 0 no imprimo el descuento del item.
            if (_it._precioDefecto > 0 && (_it._descuento > 0))
            {
                double _dto;
                _dto = Convert.ToDouble((_it._cantidad * _it._precioDefecto) * _it._descuento / 100);
                // Valido que tengo texto descuento.
                if (String.IsNullOrEmpty(_it._textoPromo))
                {
                    if (_it._descuento == 0)
                        _it._textoPromo = "Descuento ";
                    else
                        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                }
                hasar.DescuentoUltimoItem(_it._textoPromo, _dto, true);
            }
        }

        public static void ImprimirTicketRegalo(Cabecera _cab, List<Items> _items
            , string _txtRegalo1, string _txtRegalo2, string _txtRegalo3,
            string _comprobante, out bool _faltaPapel)
        {
            _faltaPapel = false;
            // aca Comienzo
            FiscalPrinterLib.HASAR hasar = new FiscalPrinterLib.HASAR();

            try
            {
                Acciones.Conectar(hasar);

                // Abro el documento.
                hasar.AbrirComprobanteNoFiscal();
                hasar.ImprimirTextoNoFiscal("Ticket de Cambio");
                hasar.ImprimirTextoNoFiscal("Comprobante: " + _comprobante);

                // Cabecera de Items.
                hasar.ImprimirTextoNoFiscal("Descripcion Articulo");
                // Linea separadora.
                hasar.ImprimirTextoNoFiscal("----------------------------------------------------------------");

                // Recorro los items.
                decimal _cantidad = 0;
                foreach (Items _it in _items)
                {
                    if (_it._cantidad >= 0)
                    {
                        // Imprimo items
                        //string aux1 = String.Format("{0} {1}", _it._codBarra, _it._descripcion);
                        hasar.ImprimirTextoNoFiscal("SKU " + _it._codBarra);
                        string txt1 = String.Format("{0} {1}", _it._descripcion, _it._cantidad.ToString().PadLeft(35 - _it._descripcion.Length, ' '));
                        hasar.ImprimirTextoNoFiscal(txt1);
                        _cantidad = _cantidad + _it._cantidad; 
                    }
                }

                // Linea separadora.
                hasar.ImprimirTextoNoFiscal("----------------------------------------------------------------");
                hasar.ImprimirTextoNoFiscal("Total unidades: " + Convert.ToInt32(_cantidad).ToString());

                // Imprimo texto de regalo.
                if (!String.IsNullOrEmpty(_txtRegalo1)) hasar.ImprimirTextoNoFiscal(_txtRegalo1);
                if (!String.IsNullOrEmpty(_txtRegalo2)) hasar.ImprimirTextoNoFiscal(_txtRegalo2);
                if (!String.IsNullOrEmpty(_txtRegalo3)) hasar.ImprimirTextoNoFiscal(_txtRegalo3);
                hasar.ImprimirTextoNoFiscal("----------------------------------------------------------------");
                //Imprimo Vendedor.
                hasar.ImprimirTextoNoFiscal("Vendedor: " + _cab.nomvendedor);
                hasar.ImprimirTextoNoFiscal("Serie / Nro: " + _cab.numserie + "/" + _cab.numfac.ToString());
                //_estilo.setDobleAncho(false);

                hasar.CerrarComprobanteNoFiscal();

                if (hasar.HuboFaltaPapel) _faltaPapel = true;
            }
            catch (Exception ex)
            {
                Acciones.Cancelar(hasar);
                throw new Exception(ex.Message);
            }
        }

        public static void ImprimirAnticipo(Clientes _cliente, List<ItemsAnticipo> _items,
            string _codCliente, decimal _precio, out bool _faltaPapel)
        {
            _faltaPapel = false;
            // aca Comienzo
            FiscalPrinterLib.HASAR hasar = new FiscalPrinterLib.HASAR();

            try
            {
                Acciones.Conectar(hasar);
                //Obtengo el vendedor.
                string _nomVendedor = _items.Select(x => x.nomVendedor).FirstOrDefault();
                // Abro el documento.
                hasar.AbrirComprobanteNoFiscal();
                hasar.ImprimirTextoNoFiscal("Recibo de Anticipo");
                hasar.ImprimirTextoNoFiscal("Recibimos de: " + _cliente.nombre);
                hasar.ImprimirTextoNoFiscal("Telefono: " + _cliente.telefono);
                hasar.ImprimirTextoNoFiscal("Codigo Cliente: " + _codCliente);
                hasar.ImprimirTextoNoFiscal("El importe de: " + _precio);
                hasar.ImprimirTextoNoFiscal("Vendedor: " + _nomVendedor);

                // Cabecera de Items.
                hasar.ImprimirTextoNoFiscal("En concepto de los siguientes articulos");
                // Linea separadora.
                hasar.ImprimirTextoNoFiscal("----------------------------------------------------------------");

                // Recorro los items.
                decimal _cantidad = 0;
                foreach (ItemsAnticipo _it in _items)
                {
                    if (_it.unidades >= 0)
                    {
                        // Imprimo items
                        //string aux1 = String.Format("{0} {1}", _it._codBarra, _it._descripcion);
                        hasar.ImprimirTextoNoFiscal("SKU " + _it.codbarras);
                        string txt1 = String.Format("{0} {1}", _it.descripcion, _it.unidades.ToString().PadLeft(35 - _it.descripcion.Length, ' '));
                        hasar.ImprimirTextoNoFiscal(txt1);
                        _cantidad = _cantidad + _it.unidades;
                    }
                }

                // Linea separadora.
                hasar.ImprimirTextoNoFiscal("----------------------------------------------------------------");
                hasar.ImprimirTextoNoFiscal("Total unidades: " + Convert.ToInt32(_cantidad).ToString());
                hasar.ImprimirTextoNoFiscal(" ");
                hasar.ImprimirTextoNoFiscal(" ");
                hasar.ImprimirTextoNoFiscal("Firma: ----------------------------------------------------------");
                hasar.ImprimirTextoNoFiscal(" ");
                hasar.ImprimirTextoNoFiscal(" ");
                hasar.ImprimirTextoNoFiscal("Aclaracion: -----------------------------------------------------");
                hasar.ImprimirTextoNoFiscal(" ");
                hasar.ImprimirTextoNoFiscal("        Valido por 15 dias");


                hasar.CerrarComprobanteNoFiscal();

                if (hasar.HuboFaltaPapel) _faltaPapel = true;
            }
            catch (Exception ex)
            {
                Acciones.Cancelar(hasar);
                throw new Exception(ex.Message);
            }
        }

        public static string Reimprimir(int _nro, string _descrip, string _ip, out bool _faltaPapel)
        {
            throw new NotImplementedException("Metodo no disponible en Impresoras de Primera Generación");
        }
        
        private static void ofh_ErrorFiscal(int Flags)
        {
            string msj = "";      // Mensaje asociado al evento

            switch (Flags)
            {
                case (int)FiscalPrinterLib.FiscalBits.F_INVALID_COMMAND:
                    msj = "OCX Fiscal HASAR ::: Comando Inválido para el estado fiscal actual";
                    break;
                case (int)FiscalPrinterLib.FiscalBits.F_INVALID_FIELD_DATA:
                    msj = "OCX Fiscal HASAR ::: Campo de datos inválido";
                    break;
                case (int)FiscalPrinterLib.FiscalBits.F_TOTAL_OVERFLOW:
                    msj = "OCX Fiscal HASAR ::: Se excede monto límite comprobante";
                    break;
                case (int)FiscalPrinterLib.FiscalBits.F_UNRECOGNIZED_COMMAND:
                    msj = "OCX Fiscal HASAR ::: Comando desconocido";
                    break;
                default:
                    msj = "OCX Fiscal HASAR ::: Comando rechazado - Evento: ErrorFiscal( )";
                    break;
            };

            MessageBox.Show(msj);
        }
        //===============================================================================================================================
        // Este evento se dispara cuando la impresora fiscal reporta falta de papel.
        //===============================================================================================================================
        private static void ofh_FaltaPapel()
        {
            MessageBox.Show("Evento: FaltaPapel");
        }

        //===============================================================================================================================
        // Este evento se dispara ante inconvenientes en la comunicación con la impresora fiscal.
        //===============================================================================================================================
        private static void ofh_ImpresoraNoResponde(int CantidadReintentos)
        {
            MessageBox.Show("Evento: ImpresoraNoResponde");
        }

        //===============================================================================================================================
        // Este evento se dispara cuando la impresora (como estándar) reporta su estado.
        //===============================================================================================================================

        private static void ofh_ErrorImpresora(int Flags)
        {
            MessageBox.Show("Evento: ErrorImpresora");
        }
        //===============================================================================================================================
        // Este evento se dispara cuando el OCX está intentando comunicarse con la impresora fiscal, probando a diferentes baudios.
        //===============================================================================================================================
        private static void ofh_ProgresoDeteccion(int Puerto, int Velocidad)
        {
            // throw new NotImplementedException();
            MessageBox.Show("Evento: ProgesoDetección");
        }

    }
}
