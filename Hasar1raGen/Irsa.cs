﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasar1raGen
{
    public class Irsa
    {
        public static bool ImprimoCabecera(Cabecera _cab, string _pathOut, string _local, string _contrato,
            string _pos, string _letra, string _tipoComprobante, string _ptovta, string _nrocomprobante,
            string _tipoMovimiento, string _rubro)
        {
            bool _rta = false;
            string _otrosImp = "0.00";

            string _line = "";

            try
            {
                //Calculamos el Neto.
                double _totalNeto = _cab.totalbruto - _cab.totdtocomercial - _cab.totdtopp;
                string _vendedor = _cab.codvendedor.ToString().Length > 2 ? _cab.codvendedor.ToString().Substring(0, 2) : _cab.codvendedor.ToString().PadLeft(2, '0');

                _line = _local.PadRight(10, ' ');
                _line = _line + _contrato.PadLeft(10, '0');
                _line = _line + _pos.PadLeft(2, '0');
                _line = _line + _cab.fecha.Year.ToString().PadLeft(4, '0') + _cab.fecha.Month.ToString().PadLeft(2, '0') + _cab.fecha.Day.ToString().PadLeft(2, '0');
                _line = _line + _cab.hora.Hour.ToString().PadLeft(2, '0') + _cab.hora.Minute.ToString().PadLeft(2, '0') + _cab.hora.Second.ToString().PadLeft(2, '0');
                _line = _line + _letra;
                _line = _line + _tipoComprobante;
                _line = _line + _ptovta.PadLeft(4, '0');
                _line = _line + _nrocomprobante.PadLeft(8, '0');
                _line = _line + _tipoMovimiento;
                //_line = _line + _cab.codvendedor.ToString().PadLeft(2, '0');
                _line = _line + _vendedor;
                _line = _line + "DNI";          //tipo documento.
                _line = _line + "000000000";    //Nro de documento.
                _line = _line + "C";
                _line = _line + _rubro.PadLeft(4, '0');
                //_line = _line + Math.Abs(_cab.totalbruto).ToString().Replace(",", ".").PadLeft(9, '0');
                _line = _line + Math.Round(Math.Abs(_totalNeto), 2).ToString().Replace(",", ".").PadLeft(9, '0');
                _line = _line + Math.Round(Math.Abs(_cab.totalimpuestos), 2).ToString().Replace(",", ".").PadLeft(9, '0');
                _line = _line + _otrosImp.Replace(",", ".").PadLeft(9, '0');

                using (StreamWriter sw = new StreamWriter(_pathOut, true))
                {
                    sw.WriteLine(_line);
                    sw.Flush();
                }

                _rta = true;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }


            return _rta;
        }

        public static bool ImprimoPagos(Cabecera _cab, List<Pagos> _pagos, string _pathOut, string _local, string _contrato,
            string _pos, string _letra, string _tipoComprobante, string _ptovta, string _nrocomprobante,
            string _tipoMovimiento, string _rubro)
        {
            bool _rta = false;
            
            string _line = "";
            string _numerotarjeta = "0";            

            List<Pagos> _new = new List<Pagos>();

            foreach(Pagos x in _pagos)
            {
                if (x.formapago.ToUpper() != "E")
                {
                    if (x.descripcion.ToUpper() != "ANTICIPO")
                    {
                        Pagos _p = new Pagos()
                        {
                            descripcion = x.descripcion,
                            tipopago = x.tipopago,
                            monto = x.monto,
                            cuotas = x.cuotas,
                            cupon = x.cupon,
                            formapago = x.formapago,
                            codigotarjeta = x.codigotarjeta
                        };

                        _new.Add(_p);
                    }
                    else
                    {
                        x.formapago = "E";
                    }
                }
            }
            //Calculamos el total del efectivo.
            decimal _totEfectivo = _pagos.Where(x => x.formapago.ToUpper() == "E").Sum(x => x.monto);
            //vemos si tenemos efectivo.
            if (Math.Abs(_totEfectivo) > 0)
            {
                Pagos _p = new Pagos()
                {
                    descripcion = "EFECTIVO",
                    tipopago = "EFECTIVO",
                    monto = Math.Round(Math.Abs(_totEfectivo), 2),
                    cuotas = "",
                    cupon = "",
                    formapago = "E",
                    codigotarjeta = ""
                };
                _new.Add(_p);
            }

            try
            {
                foreach (Pagos pg in _new)
                {
                    string _vendedor = _cab.codvendedor.ToString().Length > 2 ? _cab.codvendedor.ToString().Substring(0, 2) : _cab.codvendedor.ToString().PadLeft(2, '0');
                    //Comun a todos
                    _line = _local.PadRight(10, ' ');
                    _line = _line + _contrato.PadLeft(10, '0');
                    _line = _line + _pos.PadLeft(2, '0');
                    _line = _line + _cab.fecha.Year.ToString().PadLeft(4, '0') + _cab.fecha.Month.ToString().PadLeft(2, '0') + _cab.fecha.Day.ToString().PadLeft(2, '0');
                    _line = _line + _cab.hora.Hour.ToString().PadLeft(2, '0') + _cab.hora.Minute.ToString().PadLeft(2, '0') + _cab.hora.Second.ToString().PadLeft(2, '0');
                    _line = _line + _letra;
                    _line = _line + _tipoComprobante;
                    _line = _line + _ptovta.PadLeft(4, '0');
                    _line = _line + _nrocomprobante.PadLeft(8, '0');
                    _line = _line + _tipoMovimiento;
                    //_line = _line + _cab.codvendedor.ToString().PadLeft(2, '0');
                    _line = _line + _vendedor;
                    _line = _line + "DNI";          //tipo documento.
                    _line = _line + "000000000";    //Nro de documento.
                    //Por tipo de pago.
                    _line = _line + "P";
                    _line = _line + pg.formapago.PadLeft(1, ' ');
                    _line = _line + pg.codigotarjeta.PadLeft(2, ' ');
                    _line = _line + _numerotarjeta.PadLeft(22, '0');
                    _line = _line + Math.Round(Math.Abs(pg.monto), 2).ToString("N2").Replace(".","").Replace(",", ".").PadLeft(9, '0');                    

                    using (StreamWriter sw = new StreamWriter(_pathOut, true))
                    {
                        sw.WriteLine(_line);
                        sw.Flush();
                    }
                }

                _rta = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


            return _rta;
        }

        public static bool ImprimoCancelado(Cabecera _cab, string _pathOut, string _local, string _contrato,
            string _pos, string _letra, string _tipoComprobante, string _ptovta, string _nrocomprobante,
            string _tipoMovimiento, string _rubro)
        {
            bool _rta = false;
            string _otrosImp = "0.00";
            string _impNeto = "0.00";
            string _impIVA = "0.00";

            string _line = "";

            try
            {
                //Calculamos el Neto.
                double _totalNeto = _cab.totalbruto - _cab.totdtocomercial - _cab.totdtopp;
                string _vendedor = _cab.codvendedor.ToString().Length > 2 ? _cab.codvendedor.ToString().Substring(0, 2) : _cab.codvendedor.ToString().PadLeft(2, '0');

                _line = _local.PadRight(10, ' ');
                _line = _line + _contrato.PadLeft(10, '0');
                _line = _line + _pos.PadLeft(2, '0');
                _line = _line + _cab.fecha.Year.ToString().PadLeft(4, '0') + _cab.fecha.Month.ToString().PadLeft(2, '0') + _cab.fecha.Day.ToString().PadLeft(2, '0');
                _line = _line + _cab.fecha.Hour.ToString().PadLeft(2, '0') + _cab.fecha.Minute.ToString().PadLeft(2, '0') + _cab.fecha.Second.ToString().PadLeft(2, '0');
                _line = _line + _letra;
                _line = _line + _tipoComprobante;
                _line = _line + _ptovta.PadLeft(4, '0');
                _line = _line + _nrocomprobante.PadLeft(8, '0');
                _line = _line + _tipoMovimiento;
                //_line = _line + _cab.codvendedor.ToString().PadLeft(2, '0');
                _line = _line + _vendedor;
                _line = _line + "DNI";          //tipo documento.
                _line = _line + "000000000";    //Nro de documento.
                _line = _line + "C";
                _line = _line + _rubro.PadLeft(4, '0');
                _line = _line + _impNeto.Replace(",", ".").PadLeft(9, '0');
                _line = _line + _impIVA.Replace(",", ".").PadLeft(9, '0');
                _line = _line + _otrosImp.Replace(",", ".").PadLeft(9, '0');

                using (StreamWriter sw = new StreamWriter(_pathOut, true))
                {
                    sw.WriteLine(_line);
                    sw.Flush();
                }

                _rta = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


            return _rta;
        }

        public static void ExisteComprobante(SqlConnection _con, 
            Cabecera _cab, string _pathOut, string _local, string _contrato,
            string _pos, string _letra, string _tipoComprobante, string _ptovta, string _nrocomprobante,
            string _tipoMovimiento, string _rubro)
        {
            string _sql = @"Select NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL from FACTURASVENTASERIESRESOL 
                Where (SERIEFISCAL1 = @PtoVta) And NUMEROFISCAL = (@Numero)";

            int _numero = Convert.ToInt32(_nrocomprobante) - 1;

            Tickets _tck = new Tickets();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;
                //Parametros
                _cmd.Parameters.AddWithValue("@PtoVta", _ptovta);
                _cmd.Parameters.AddWithValue("@Numero", _numero);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _tck.N = _reader["N"].ToString();
                            _tck.NumeroFiscal = Convert.ToInt32(_reader["NUMEROFISCAL"]);
                            _tck.NumFactura = Convert.ToInt32(_reader["NUMFACTURA"]);
                            _tck.NumSerie = _reader["NUMSERIE"].ToString();
                            _tck.SerieFiscal1 = _reader["SERIEFISCAL1"].ToString();
                            _tck.SerieFiscal2 = _reader["SERIEFISCAL2"].ToString();
                        }
                    }
                }
            }
            //Vemos si tenemos algo
            if (_tck.NumSerie == null)
            {
                ImprimoCancelado(_cab, _pathOut, _local, _contrato, _pos, _letra, _tipoComprobante, _ptovta, _numero.ToString(), _tipoMovimiento, _rubro);
            }
            
        }

        /// <summary>
        /// Lanza el proceso para informar las ventas al host de IRSA
        /// </summary>
        /// <param name="_cabecera">Cabecera de la venta.</param>
        /// <param name="_pagos">Lista de Pagos</param>
        /// <param name="_tipoComprobante">Tipo de Comprobante a informas N op C</param>
        /// <param name="_nroComprobante">Numero del comprobante PtoVta + Numero</param>
        /// <param name="_conexion">Conexion SQL abierta.</param>
        public static void LanzarTrancomp(Hasar1raGen.Cabecera _cabecera, List<Hasar1raGen.Pagos> _pagos, InfoIrsa _infoirsa,
            string _tipoComprobante, string _nroComprobante, SqlConnection _conexion)
        {
            try
            {
                string _letra = "";
                string _ptoVta = "";
                string _nroFiscal = "";
                string _clMov = "N";

                if (_cabecera.regfacturacioncliente != 1)
                    _letra = "B";
                else
                    _letra = "A";

                string[] _nro = _nroComprobante.Split('-');
                if (_nro.Length == 2)
                {
                    _ptoVta = _nro[0];
                    _nroFiscal = _nro[1];
                }

                ExisteComprobante(_conexion, _cabecera, _infoirsa.pathSalida, _infoirsa.local, _infoirsa.contrato, _infoirsa.pos, _letra,
                    _tipoComprobante, _ptoVta, _nroFiscal, _clMov, _infoirsa.rubro);

                ImprimoCabecera(_cabecera, _infoirsa.pathSalida, _infoirsa.local, _infoirsa.contrato, _infoirsa.pos, _letra,
                    _tipoComprobante, _ptoVta, _nroFiscal, _clMov, _infoirsa.rubro);

                ImprimoPagos(_cabecera, _pagos, _infoirsa.pathSalida, _infoirsa.local, _infoirsa.contrato, _infoirsa.pos, _letra,
                    _tipoComprobante, _ptoVta, _nroFiscal, _clMov, _infoirsa.rubro);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Metodo que devuelve una lista con la claves de los comprobantes comprendidos entre dos fechas.
        /// </summary>
        /// <param name="Desde">Fecha Desde.</param>
        /// <param name="Hasta">Fecha Hasta.</param>
        /// <param name="_conexion">Conexion SQL ABIERTA.</param>
        /// <returns>Lista</returns>
        public static List<Tickets> GetTikets(DateTime Desde, DateTime Hasta, SqlConnection _conexion)
        {
            List<Tickets> lista = new List<Tickets>();

            string query = @"SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N,
                    FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.SERIEFISCAL2, FACTURASVENTASERIESRESOL.NUMEROFISCAL
                    FROM FACTURASVENTA INNER JOIN FACTURASVENTASERIESRESOL ON
                    FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE
                    AND FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA
                    AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N
                    WHERE FACTURASVENTA.FECHA >= @fDesde 
                    AND FACTURASVENTA.FECHA <= @fHasta";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _conexion;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = query;
                    //Parametros
                    _cmd.Parameters.AddWithValue("@fDesde", Desde);
                    _cmd.Parameters.AddWithValue("@fHasta", Hasta);

                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Tickets unticket = new Tickets();
                                unticket.N = reader["N"].ToString();
                                unticket.NumFactura = Convert.ToInt32(reader["NUMFACTURA"]);
                                unticket.NumSerie = reader["NUMSERIE"].ToString();
                                unticket.NumeroFiscal = Convert.ToInt32(reader["NUMEROFISCAL"]);
                                unticket.SerieFiscal1 = reader["SERIEFISCAL1"].ToString();
                                unticket.SerieFiscal2 = reader["SERIEFISCAL2"].ToString();
                                lista.Add(unticket);
                            }
                        }
                    }
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
    public class Tickets
    {
        public string NumSerie { get; set; }
        public int NumFactura { get; set; }
        public string N { get; set; }
        public string SerieFiscal1 { get; set; }
        public string SerieFiscal2 { get; set; }
        public int NumeroFiscal { get; set; }
    }
    public class InfoIrsa
    {
        public string contrato { get; set; }
        public string local { get; set; }
        public string pathSalida { get; set; }
        public string pos { get; set; }
        public string rubro { get; set; }
    }
}
