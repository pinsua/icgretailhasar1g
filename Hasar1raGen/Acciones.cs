﻿using System;
using FiscalPrinterLib;

namespace Hasar1raGen
{
    public class Acciones
    {
        public static Clases.ModelosImpresoras.Modelos modelo = Clases.ModelosImpresoras.Modelos.MODELO_715_201;
        public static int puerto = 4;
        public static int baudios = 9600;

        public static void Cancelar() {
            try
            {
                FiscalPrinterLib.HASAR _cf = new FiscalPrinterLib.HASAR();
                Conectar(_cf);
                _cf.TratarDeCancelarTodo();
            }
            catch {

            }
        }

        public static void Conectar(FiscalPrinterLib.HASAR _cf) {
            Conectar(_cf, modelo, puerto, baudios);
        }


        public static void Conectar(FiscalPrinterLib.HASAR _cf, Clases.ModelosImpresoras.Modelos _modelo, int _puerto = 4, int _baudios = 9600)
        {

            if (_modelo == 0)
            {
                throw new ArgumentNullException("Debe inicializar el modelo de la impresora");
            }

            if (_puerto == 0)
            {
                throw new ArgumentNullException("Debe inicializar el puerto COM de la impresora");
            }

            if (baudios == 0)
            {
                throw new ArgumentNullException("Debe inicializar los baudios");
            }

            try
            {
                _cf.Modelo = (FiscalPrinterLib.ModelosDeImpresoras)modelo;
                _cf.Puerto = puerto;
                _cf.Baudios = baudios;
                _cf.Comenzar();
                _cf.TratarDeCancelarTodo();
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo conectar con la Controladora Fiscal. Error:" + ex.Message);
            }
        }

        internal static void Cancelar(HASAR hasar)
        {
            try
            {
                hasar.TratarDeCancelarTodo();
            }
            catch
            {

            }
        }
    }
}
