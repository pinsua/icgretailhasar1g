﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Hasar1raGen
{
    public class Arqueo
    {
        public string _arqueo { get; set; }
        public int _numero { get; set; }
        public DateTime _fecha { get; set; }
        public string _hora { get; set; }


        public static List<Arqueo> GetArqueo(string _numSerie, string _n, int _numero, SqlConnection _con)
        {
            string _sql = "SELECT ARQUEO, NUMERO, FECHA, HORA FROM ARQUEOS WHERE ARQUEO = 'Z' AND(CLEANCASHCONTROLCODE1 IS NULL OR CLEANCASHCONTROLCODE1 = '') ";

            List<Arqueo> _items = new List<Arqueo>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            Arqueo _cls = new Arqueo();
                            _cls._arqueo = _reader["ARQUEO"].ToString();
                            _cls._fecha = Convert.ToDateTime(_reader["FECHA"]);
                            _cls._hora = _reader["HORA"].ToString();
                            _cls._numero = Convert.ToInt32(_reader["NUMERO"]);
                            _items.Add(_cls);
                        }
                    }
                }
            }

            return _items;
        }

        public static void GetArqueo(DataSet _dts, string _table, string _tipo, string _caja , SqlConnection _con)
        {
            string _sql = "SELECT ARQUEO, NUMERO, FECHA, HORA FROM ARQUEOS " +
                "WHERE ARQUEO = @tipo AND (CLEANCASHCONTROLCODE1 IS NULL OR CLEANCASHCONTROLCODE1 = '') " +
                "AND (CAJA = @caja) ORDER BY NUMERO";

                // Limpio el dataset
                _dts.Tables[_table].Clear();

                using (SqlCommand _cmd = new SqlCommand(_sql, _con))
                {
                    _cmd.Parameters.AddWithValue("@tipo", _tipo);
                _cmd.Parameters.AddWithValue("@caja", _caja);

                using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                    {
                        _sda.Fill(_dts, _table);
                    }
                }


        }

        public static int UpdateArqueos(string _tipo, int _numero, DateTime _fecha, string _hora, string _dato, string _caja, SqlConnection _conn)
        {
            string _sql = "UPDATE ARQUEOS SET CLEANCASHCONTROLCODE1 = @dato " +
                "WHERE ARQUEOS.ARQUEO = @Tipo AND ARQUEOS.NUMERO = @Numero and ARQUEOS.FECHA = @fecha and ARQUEOS.HORA = @hora and ARQUEOS.caja = @caja";

            int _rta = 0;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _conn;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@dato", _dato);
                _cmd.Parameters.AddWithValue("@Tipo", _tipo);
                _cmd.Parameters.AddWithValue("@Numero", _numero);
                _cmd.Parameters.AddWithValue("@fecha", _fecha);
                _cmd.Parameters.AddWithValue("@hora", _hora);
                _cmd.Parameters.AddWithValue("@caja", _caja);

                _rta =_cmd.ExecuteNonQuery();
                
            }

            return _rta;
        }

        public static int UpdateArqueos(string _tipo, int _numero, string _dato, string _caja, SqlConnection _conn)
        {
            string _sql = "UPDATE ARQUEOS SET CLEANCASHCONTROLCODE1 = @dato " +
                "WHERE ARQUEOS.ARQUEO = @Tipo AND ARQUEOS.NUMERO = @Numero and ARQUEOS.caja = @caja";

            int _rta = 0;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _conn;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@dato", _dato);
                _cmd.Parameters.AddWithValue("@Tipo", _tipo);
                _cmd.Parameters.AddWithValue("@Numero", _numero);
                _cmd.Parameters.AddWithValue("@caja", _caja);

                _rta = _cmd.ExecuteNonQuery();

            }

            return _rta;
        }

        public static int UpdateArqueosOK(string _tipo, string _dato, SqlConnection _conn)
        {
            string _sql = "UPDATE ARQUEOS SET CLEANCASHCONTROLCODE1 = @dato " +
                "WHERE ARQUEOS.ARQUEO = @Tipo AND (CLEANCASHCONTROLCODE1 IS NULL OR CLEANCASHCONTROLCODE1 = '')";

            int _rta = 0;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _conn;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@dato", _dato);
                _cmd.Parameters.AddWithValue("@Tipo", _tipo);
                _rta = _cmd.ExecuteNonQuery();

            }

            return _rta;
        }
    }
}
