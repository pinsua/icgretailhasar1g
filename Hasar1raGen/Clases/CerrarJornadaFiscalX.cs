﻿namespace Hasar1raGen
{
    public class CerrarJornadaFiscalX
    {
        public object NumeroX;
        public object CantidadDFCancelados;
        public object CantidadDNFHEmitidos;
        public object CantidadDNFEmitidos;
        public object CantidadDFEmitidos;
        public object UltimoDocFiscalBC;
        public object UltimoDocFiscalA;
        public object MontoVentasDocFiscal;
        public object MontoIVADocFiscal;
        public object MontoImpInternosDocFiscal;
        public object MontoPercepcionesDocFiscal;
        public object MontoIVANoInscriptoDocFiscal;
        public object UltimaNotaCreditoBC;
        public object UltimaNotaCreditoA;
        public object MontoVentasNotaCredito;
        public object MontoIVANotaCredito;
        public object MontoImpInternosNotaCredito;
        public object MontoPercepcionesNotaCredito;
        public object MontoIVANoInscriptoNotaCredito;
        public object UltimoRemito;
        public object CantidadNCCanceladas;
        public object CantidadDFBCEmitidos;
        public object CantidadDFAEmitidos;
        public object CantidadNCBCEmitidas;
        public object CantidadNCAEmitidas;
    }
}