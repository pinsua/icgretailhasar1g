﻿using System.Xml.Serialization;
namespace Hasar1raGen
{
    [XmlRoot(ElementName = "impresora")]
    public class Impresora
    {
        [XmlElement(ElementName = "modelo")]
        public string Modelo { get; set; }
        [XmlElement(ElementName = "puerto")]
        public string Puerto { get; set; }
        [XmlElement(ElementName = "baudios")]
        public string Baudios { get; set; }
    }

    [XmlRoot(ElementName = "config")]
    public class Config1raG
    {
        [XmlElement(ElementName = "ip")]
        public string Ip { get; set; }
        [XmlElement(ElementName = "caja")]
        public string Caja { get; set; }
        [XmlElement(ElementName = "tktregalo1")]
        public string Tktregalo1 { get; set; }
        [XmlElement(ElementName = "tktregalo2")]
        public string Tktregalo2 { get; set; }
        [XmlElement(ElementName = "tktregalo3")]
        public string Tktregalo3 { get; set; }
        [XmlElement(ElementName = "textoajuste")]
        public string TextoAjuste { get; set; }
        [XmlElement(ElementName = "terminal")]
        public string Terminal { get; set; }
        [XmlElement(ElementName = "super")]
        public string Super { get; set; }
        [XmlElement(ElementName = "habilitoPassword")]
        public string HabilitoPassword { get; set; }
        [XmlElement(ElementName = "hasarlog")]
        public string Hasarlog { get; set; }
        [XmlElement(ElementName = "impresora")]
        public Impresora Impresora { get; set; }
        [XmlElement(ElementName = "bd")]
        public Bd Bd { get; set; }
    }

}
