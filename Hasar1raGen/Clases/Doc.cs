﻿using System.Xml.Serialization;
namespace Hasar1raGen
{
    [XmlRoot(ElementName = "bd")]
    public class Bd
    {
        [XmlElement(ElementName = "server")]
        public string Server { get; set; }
        [XmlElement(ElementName = "database")]
        public string Database { get; set; }
        [XmlElement(ElementName = "user")]
        public string User { get; set; }
    }

    [XmlRoot(ElementName = "doc")]
    public class Doc
    {
        [XmlElement(ElementName = "bd")]
        public Bd Bd { get; set; }
        [XmlElement(ElementName = "codvendedor")]
        public string Codvendedor { get; set; }
        [XmlElement(ElementName = "tipodoc")]
        public string Tipodoc { get; set; }
        [XmlElement(ElementName = "serie")]
        public string Serie { get; set; }
        [XmlElement(ElementName = "numero")]
        public string Numero { get; set; }
        [XmlElement(ElementName = "n")]
        public string N { get; set; }
        [XmlElement(ElementName = "GuardandoTef")]
        public string GuardandoTef { get; set; }
    }

}
