﻿namespace Hasar1raGen
{
    internal class CerrarJornadaFiscalZPorNumero
    {
        public object NumeroJornada;
        public object FechaZ;
        public object NumeroZeta;
        public object UltimoDocFiscalBC;
        public object UltimoDocFiscalA;
        public object MontoVentasDocFiscal;
        public object MontoIVADocFiscal;
        public object MontoImpInternosDocFiscal;
        public object MontoPercepcionesDocFiscal;
        public object MontoIVANoInscriptoDocFiscal;
        public object UltimaNotaCreditoBC;
        public object UltimaNotaCreditoA;
        public object MontoVentasNotaCredito;
        public object MontoIVANotaCredito;
        public object MontoImpInternosNotaCredito;
        public object MontoPercepcionesNotaCredito;
        public object MontoIVANoInscriptoNotaCredito;
        public object UltimoRemito;
    }
}