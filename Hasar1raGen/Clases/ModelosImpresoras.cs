﻿namespace Hasar1raGen.Clases
{
    public class ModelosImpresoras
    {
        public enum Modelos
        {
            MODELO_614 = 1,
            MODELO_615 = 2,
            MODELO_PR4 = 3,
            MODELO_950 = 4,
            MODELO_951 = 5,
            MODELO_262 = 6,
            MODELO_PJ20 = 7,
            MODELO_P320 = 8,
            MODELO_715 = 9,
            MODELO_PR5 = 10,
            MODELO_272 = 11,
            MODELO_PPL8 = 12,
            MODELO_P321 = 13,
            MODELO_P322 = 14,
            MODELO_P425 = 15,
            MODELO_P425_201 = 16,
            MODELO_PPL8_201 = 17,
            MODELO_P322_201 = 18,
            MODELO_P330 = 19,
            MODELO_P435 = 20,
            MODELO_P330_201 = 21,
            MODELO_PPL9 = 22,
            MODELO_P330_202 = 23,
            MODELO_P435_101 = 24,
            MODELO_715_201 = 25,
            MODELO_PR5_201 = 26,
            MODELO_P435_102 = 27,
            MODELO_PPL23 = 28,
            MODELO_715_302 = 29,
            MODELO_715_403 = 30,
            MODELO_P330_203 = 31,
            MODELO_P441 = 32,
            MODELO_PPL23_101 = 33,
            MODELO_P435_203 = 34,
            MODELO_P1120 = 35,
            MODELO_PPL23_202 = 36,
            MODELO_PR5_302 = 37,
            MODELO_272_201 = 38,
            MODELO_P441_201 = 39,
            MODELO_715_504 = 40,
            MODELO_P340 = 41
        }
    }
}
