﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hasar1raGen
{
    public class ShoppingCaballito
    {
        /// <summary>
        /// Lanza el proceso para informar las ventas al host de IRSA
        /// </summary>
        /// <param name="_cabecera">Cabecera de la venta.</param>
        /// <param name="_pagos">Lista de Pagos</param>
        /// <param name="_tipoComprobante">Tipo de Comprobante a informas N op C</param>
        /// <param name="_nroComprobante">Numero del comprobante PtoVta + Numero</param>
        /// <param name="_conexion">Conexion SQL abierta.</param>
        public static void LanzarShoppingCaballito(Hasar1raGen.Cabecera _cabecera, List<Hasar1raGen.Pagos> _pagos, string _pathOut,
            string _tipoComprobante, string _nroComprobante, SqlConnection _conexion)
        {
            try
            {
                string _letra = "";
                string _ptoVta = "";
                string _nroFiscal = "";
                string _clMov = "N";

                if (_cabecera.regfacturacioncliente != 1)
                    _letra = "B";
                else
                    _letra = "A";

                string[] _nro = _nroComprobante.Split('-');
                if (_nro.Length == 2)
                {
                    _ptoVta = _nro[0];
                    _nroFiscal = _nro[1];
                }
                                
                ImprimoPagos(_cabecera, _pagos, _pathOut, _letra,
                    _tipoComprobante, _ptoVta, _nroFiscal, _clMov);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public static bool ImprimoPagos(Cabecera _cab, List<Pagos> _pagos, string _pathOut, 
            string _letra, string _tipoComprobante, string _ptovta, string _nrocomprobante,
            string _tipoMovimiento)
        {
            bool _rta = false;

            string _line = "";

            List<Pagos> _new = new List<Pagos>();

            foreach (Pagos x in _pagos)
            {
                if (x.formapago.ToUpper() != "E")
                {
                    if (x.descripcion.ToUpper() != "ANTICIPO")
                    {
                        Pagos _p = new Pagos()
                        {
                            descripcion = x.descripcion,
                            tipopago = x.tipopago,
                            monto = x.monto,
                            cuotas = x.cuotas,
                            cupon = x.cupon,
                            formapago = x.formapago,
                            codigotarjeta = x.codigotarjeta
                        };

                        _new.Add(_p);
                    }
                    else
                    {
                        x.formapago = "E";
                    }
                }
            }
            //Calculamos el total del efectivo.
            decimal _totEfectivo = _pagos.Where(x => x.formapago.ToUpper() == "E").Sum(x => x.monto);
            //vemos si tenemos efectivo.
            if (Math.Abs(_totEfectivo) > 0)
            {
                Pagos _p = new Pagos()
                {
                    descripcion = "EFECTIVO",
                    tipopago = "EFECTIVO",
                    monto = Math.Round(Math.Abs(_totEfectivo), 2),
                    cuotas = "",
                    cupon = "",
                    formapago = "E",
                    codigotarjeta = ""
                };
                _new.Add(_p);
            }

            try
            {
                foreach (Pagos pg in _new)
                {
                    //string _vendedor = _cab.codvendedor.ToString().Length > 2 ? _cab.codvendedor.ToString().Substring(0, 2) : _cab.codvendedor.ToString().PadLeft(2, '0');
                    string _tipopag = pg.tipopago.Length > 5 ? pg.tipopago.Substring(0, 5) : pg.tipopago.PadLeft(5, ' ');
                    string _formapago = pg.formapago == "E" ? "01" : "03";
                    string _monto = Math.Round(Math.Abs(pg.monto), 2).ToString("N2").Replace(",", "").Replace(",", ".").PadLeft(7, '0');
                    if (Math.Abs(pg.monto) > 9999)
                    {
                        decimal _importe = Math.Round(Math.Abs(pg.monto), 2);
                        //
                        while (_importe > 9999)
                        {
                            _importe = _importe - 9999;
                            ImprimoLinea(_tipoComprobante, _letra, _ptovta, _nrocomprobante, _cab.fecha, _cab.hora, "9999.00", _formapago, _tipopag, _pathOut);
                        }
                        if(_importe > 0)
                        {
                            string _importe2 = Math.Round(Math.Abs(_importe), 2).ToString("N2").Replace(",", "").Replace(",", ".").PadLeft(7, '0');
                            ImprimoLinea(_tipoComprobante, _letra, _ptovta, _nrocomprobante, _cab.fecha, _cab.hora, _importe2, _formapago, _tipopag, _pathOut);
                        }
                    }
                    else
                    {
                        ImprimoLinea(_tipoComprobante, _letra, _ptovta, _nrocomprobante, _cab.fecha, _cab.hora, _monto, _formapago, _tipopag, _pathOut);
                    }

                }

                _rta = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


            return _rta;
        }

        private static void ImprimoLinea(string _tipocomprobante, string _letra, string _ptoVta, string _nroComprobante, 
            DateTime _fecha, DateTime _hora, string _monto, string _formaPago, string _tipoPago, string _path)
        {
            string _line = "";
            //Comun a todos
            _line = _tipocomprobante;
            _line = _line + _letra;
            _line = _line + _ptoVta.PadLeft(4, '0');
            _line = _line + _nroComprobante.PadLeft(8, '0');
            _line = _line + _fecha.Day.ToString().PadLeft(2, '0') + "/" + _fecha.Month.ToString().PadLeft(2, '0') + "/" + _fecha.Year.ToString().Substring(2, 2).PadLeft(2, '0');
            _line = _line + _hora.Hour.ToString().PadLeft(2, '0') + ":" + _hora.Minute.ToString().PadLeft(2, '0') + ":" + _hora.Second.ToString().PadLeft(2, '0');
            _line = _line + "1";
            _line = _line + _monto.PadLeft(7, '0');  //
                                                     //Por tipo de pago.
            _line = _line + _formaPago;
            _line = _line + _tipoPago.PadLeft(5, ' ');

            using (StreamWriter sw = new StreamWriter(_path, true))
            {
                sw.WriteLine(_line);
                sw.Flush();
            }
        }
    }
}
