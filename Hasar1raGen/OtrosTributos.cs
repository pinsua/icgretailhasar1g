﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Hasar1raGen
{
    public class OtrosTributos
    {
        public double _base { get; set; }
        public double _importe { get; set; }
        public string _nombre { get; set; }

        public static List<OtrosTributos> GetOtrosTributos(string _numSerie, string _n, int _numero, SqlConnection _con)
        {
            string _sql = "SELECT FACTURASVENTADTOS.BASE, FACTURASVENTADTOS.IMPORTE, CARGOSDTOS.NOMBRE " +
                "FROM FACTURASVENTADTOS left join CARGOSDTOS On FACTURASVENTADTOS.CODDTO = CARGOSDTOS.CODIGO " +
                "WHERE FACTURASVENTADTOS.NUMSERIE = @NumSerie AND FACTURASVENTADTOS.NUMERO = @Numero AND FACTURASVENTADTOS.N = " +
                "@N AND CARGOSDTOS.CODIGOFISCAL = '005'";

            List<OtrosTributos> _items = new List<OtrosTributos>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@NumSerie", _numSerie);
                _cmd.Parameters.AddWithValue("@N", _n);
                _cmd.Parameters.AddWithValue("@Numero", _numero);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            OtrosTributos _cls = new OtrosTributos();
                            _cls._base = Convert.ToDouble(_reader["BASE"]);
                            _cls._importe = Convert.ToDouble(_reader["IMPORTE"]);
                            _cls._nombre = _reader["NOMBRE"].ToString();
                            
                            _items.Add(_cls);
                        }
                    }
                }
            }

            return _items;
        }

        public static List<OtrosTributos> GetOtrosTributos(Doc doc)
        {
            List<OtrosTributos> result = new List<OtrosTributos>();
            if (doc == null)
            {
                throw new ArgumentNullException(nameof(doc));
            }
            return result;
        }
    }
}
