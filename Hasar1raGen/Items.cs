﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Hasar1raGen
{
    public class Items
    {
        public string _codArticulo { get; set; }
        public string _descripcion { get; set; }
        public decimal _cantidad { get; set; }
        public decimal _precio { get; set; }
        public decimal _iva { get; set; }
        public decimal _descuento { get; set; }
        public string _textoPromo { get; set; }
        public string _abonoDeNumseie { get; set; }
        public int _abonoDeNumAlbaran { get; set; }
        public string _abonoDe_N { get; set; }
        public string _referencia { get; set; }
        public decimal _precioDefecto { get; set; }
        public decimal _dto2 { get; set; }
        public string _codBarra { get; set; }
        public string _nomVendedor { get; set; }

        public static List<Items> GetItems(string _numSerie, string _n, int _nroAlbaran, SqlConnection _con)
        {

            string _sql = @"SELECT ALBVENTALIN.CODARTICULO, ALBVENTALIN.DESCRIPCION, ALBVENTALIN.UNIDADESTOTAL, ALBVENTALIN.PRECIO, 
                             ALBVENTALIN.IVA, ALBVENTALIN.DTO, PROMOCIONES.TEXTOIMPRIMIR, ALBVENTALIN.ABONODE_NUMSERIE, ALBVENTALIN.ABONODE_NUMALBARAN, 
                             ALBVENTALIN.ABONODE_N, ALBVENTALIN.REFERENCIA, ALBVENTALIN.PRECIOIVA AS PRECIODEFECTO, ALBVENTALINPROMOCIONES.DTO as DTO2, ARTICULOSLIN.CODBARRAS, VENDEDORES.NOMVENDEDOR
                             FROM ALBVENTALIN left join ALBVENTALINPROMOCIONES ON ALBVENTALIN.NUMSERIE = ALBVENTALINPROMOCIONES.NUMSERIE 
                             AND ALBVENTALIN.NUMALBARAN = ALBVENTALINPROMOCIONES.NUMALBARAN 
                             AND ALBVENTALIN.N = ALBVENTALINPROMOCIONES.N 
                             AND ALBVENTALIN.NUMLIN = ALBVENTALINPROMOCIONES.NUMLIN and ALBVENTALINPROMOCIONES.IMPORTEDTO > 0 
                             LEFT JOIN PROMOCIONES ON ALBVENTALINPROMOCIONES.IDPROMOCION = PROMOCIONES.IDPROMOCION  
                             INNER JOIN ARTICULOSLIN ON ALBVENTALIN.CODARTICULO = ARTICULOSLIN.CODARTICULO 
                            AND ALBVENTALIN.COLOR = ARTICULOSLIN.COLOR 
                            AND ALBVENTALIN.TALLA = ARTICULOSLIN.TALLA
                    INNER JOIN VENDEDORES on ALBVENTALIN.CODVENDEDOR = VENDEDORES.CODVENDEDOR
                    WHERE ALBVENTALIN.NUMSERIE = @Numserie and ALBVENTALIN.N = @N and ALBVENTALIN.NUMALBARAN = @NumAlbaran 
                    ORDER BY ALBVENTALIN.UNIDADESTOTAL desc";

            List<Items> _items = new List<Items>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@NumSerie", _numSerie);
                _cmd.Parameters.AddWithValue("@N", _n);
                _cmd.Parameters.AddWithValue("@NumAlbaran", _nroAlbaran);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            Items _cls = new Items();
                            _cls._cantidad = Convert.ToDecimal(_reader["UNIDADESTOTAL"]);
                            _cls._codArticulo = _reader["CODARTICULO"].ToString();
                            _cls._descripcion = _reader["DESCRIPCION"].ToString();
                            _cls._descuento = Convert.ToDecimal(_reader["DTO"]);
                            _cls._iva = Convert.ToDecimal(_reader["IVA"]);
                            _cls._precio = Convert.ToDecimal(_reader["PRECIO"]);
                            _cls._textoPromo = _reader["TEXTOIMPRIMIR"].ToString();
                            _cls._abonoDeNumseie = _reader["ABONODE_NUMSERIE"].ToString();
                            _cls._abonoDeNumAlbaran = Convert.ToInt32(_reader["ABONODE_NUMALBARAN"]);
                            _cls._abonoDe_N = _reader["ABONODE_N"].ToString();
                            _cls._referencia = _reader["REFERENCIA"].ToString();
                            _cls._precioDefecto = Convert.ToDecimal(_reader["PRECIODEFECTO"]);
                            _cls._dto2 = _reader["DTO2"] == DBNull.Value ? 0 : Convert.ToDecimal(_reader["DTO2"]);
                            _cls._codBarra = _reader["CODBARRAS"].ToString();
                            _cls._nomVendedor = _reader["NOMVENDEDOR"].ToString();

                            _items.Add(_cls);
                        }
                    }
                }
            }

            return _items;
        }
    }

    public class ItemsAnticipo
    {
        public decimal importe { get; set; }
        public int codArticulo { get; set; }
        public string referencia { get; set; }
        public string descripcion { get; set; }
        public int unidades { get; set; }
        public decimal precio { get; set; }
        public decimal dto { get; set; }
        public decimal total { get; set; }
        public string codbarras { get; set; }
        public string nomVendedor { get; set; }

        public static List<ItemsAnticipo> GetItems(string _numSerie, string _n, int _nroPedido, SqlConnection _con)
        {

            string _sql = @"select TESORERIA.IMPORTE, PEDVENTALIN.CODARTICULO, PEDVENTALIN.REFERENCIA, PEDVENTALIN.DESCRIPCION, 
                PEDVENTALIN.UNIDADESTOTAL, PEDVENTALIN.PRECIO, PEDVENTALIN.DTO, PEDVENTALIN.TOTAL, ARTICULOSLIN.CODBARRAS, VENDEDORES.NOMVENDEDOR
                from PEDVENTACAB inner join CLIENTES on PedventaCab.codcliente = Clientes.CODCLIENTE
                inner join TESORERIA on PEDVENTACAB.SUPEDIDO = TESORERIA.SUDOCUMENTO
                inner join PEDVENTALIN on PEDVENTACAB.NUMSERIE = PEDVENTALIN.NUMSERIE AND PEDVENTACAB.NUMPEDIDO = PEDVENTALIN.NUMPEDIDO and PEDVENTACAB.N = PEDVENTALIN.N
                inner join ARTICULOSLIN on PEDVENTALIN.CODARTICULO = ARTICULOSLIN.CODARTICULO AND PEDVENTALIN.TALLA = ARTICULOSLIN.TALLA and PEDVENTALIN.COLOR = ARTICULOSLIN.COLOR
                inner join VENDEDORES on PEDVENTALIN.CODVENDEDOR = VENDEDORES.CODVENDEDOR
                Where PEDVENTACAB.NUMSERIE = @NumSerie and PEDVENTACAB.NUMPEDIDO = @NumPedido and PEDVENTACAB.N = @N";

            List<ItemsAnticipo> _items = new List<ItemsAnticipo>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@NumSerie", _numSerie);
                _cmd.Parameters.AddWithValue("@N", _n);
                _cmd.Parameters.AddWithValue("@NumPedido", _nroPedido);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            ItemsAnticipo _cls = new ItemsAnticipo();
                            _cls.codArticulo = Convert.ToInt32(_reader["CODARTICULO"]);
                            _cls.codbarras = _reader["CODBARRAS"].ToString();
                            _cls.descripcion = _reader["DESCRIPCION"].ToString();
                            _cls.dto = Convert.ToDecimal(_reader["DTO"]);
                            _cls.importe = Convert.ToDecimal(_reader["IMPORTE"]);
                            _cls.precio = Convert.ToDecimal(_reader["PRECIO"]);
                            _cls.referencia = _reader["REFERENCIA"].ToString();
                            _cls.total = Convert.ToDecimal(_reader["TOTAL"]);
                            _cls.unidades = Convert.ToInt32(_reader["UNIDADESTOTAL"]);
                            _cls.nomVendedor = _reader["NOMVENDEDOR"].ToString();

                            _items.Add(_cls);
                        }
                    }
                }
            }

            return _items;
        }
    }
}
