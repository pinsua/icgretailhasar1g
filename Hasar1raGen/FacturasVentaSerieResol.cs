﻿using System;
using System.Data.SqlClient;

namespace Hasar1raGen
{
    public class FacturasVentaSerieResol
    {
        public string numSerie { get; set; }
        public int numFactura { get; set; }
        public string n { get; set; }
        public string serieFiscal1 {get;set;}
        public string serieFiscal2 { get; set; }
        public int numeroFiscal { get; set; }

        public static bool GrabarTiquet(string _numSerie, int _numFactura, string _n, string _serieFiscal1, 
            string _serieFiscal2, int _numeroFiscal, SqlConnection _con)
         {
            string _sql = "INSERT INTO FACTURASVENTASERIESRESOL ( NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL) "+
                    "VALUES(@numSerie, @numFactura, @n, @serieFiscal1, @serieFiscal2, @numeroFiscal)";

            bool _rta = false;

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Numserie", _numSerie);
                    _cmd.Parameters.AddWithValue("@numFactura", _numFactura);
                    _cmd.Parameters.AddWithValue("@n", _n);
                    _cmd.Parameters.AddWithValue("@serieFiscal1", _serieFiscal1);
                    _cmd.Parameters.AddWithValue("@serieFiscal2", _serieFiscal2);
                    _cmd.Parameters.AddWithValue("@numeroFiscal", _numeroFiscal);

                    int _insert = _cmd.ExecuteNonQuery();
                    _rta = true;
                }
            }
            catch (Exception )
            {
                _rta = false;
            }
            return _rta;
        }

        public static FacturasVentaSerieResol GetTiquet(string _numSerie, int _numFactura, string _n, SqlConnection _con)
        {
            string _sql = "SELECT NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL FROM FACTURASVENTASERIESRESOL " +
                    "WHERE NUMSERIE = @numSerie AND NUMFACTURA = @numFactura AND N = @n";

            FacturasVentaSerieResol _cls = new FacturasVentaSerieResol();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@Numserie", _numSerie);
                _cmd.Parameters.AddWithValue("@numFactura", _numFactura);
                _cmd.Parameters.AddWithValue("@n", _n);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _cls.n = _reader["N"].ToString();
                            _cls.numeroFiscal = Convert.ToInt32(_reader["NUMEROFISCAL"]);
                            _cls.numFactura = Convert.ToInt32(_reader["NUMFACTURA"]);
                            _cls.numSerie = _reader["NUMSERIE"].ToString();
                            _cls.serieFiscal1 = _reader["SERIEFISCAL1"].ToString();
                            _cls.serieFiscal2 = _reader["SERIEFISCAL2"].ToString();
                        }
                    }
                }
            }
            return _cls;
        }        
    }
}
