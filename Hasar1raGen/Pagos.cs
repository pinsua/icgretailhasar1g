﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Hasar1raGen
{
    public class Pagos
    {
        public string descripcion { get; set; }
        public string tipopago { get; set; }
        public decimal monto { get; set; }
        public string cuotas { get; set; }
        public string cupon { get; set; }
        public string formapago { get; set; }
        public string codigotarjeta { get; set; }
        public string olmosTipoPago {get;set;}
        public string olmosTarjeta { get; set; }
        
        public static List<Pagos> GetPagos(string _serie, int _numero, string _N, SqlConnection _con)
        {

            string _sql = @"SELECT (TESORERIA.IMPORTE * TESORERIA.FACTORMONEDA) as IMPORTE, IsNull(FORMASPAGO.DESCRIPCION, 'Anticipo') as TARJETA, 
                IsNull(TIPOSPAGO.DESCRIPCION, 'Anticipo') as TIPOPAGO, ISNULL(tipospago.MARCASTARJETA,'') as FormaPago, 
                ISNULL(FORMASPAGO.TEXTOIMP,'') as CodigoTarjeta, TESORERIA.COMENTARIOVISIBLE,
                FORMASPAGO.MARCASTARJETA as OlmosTipoPago, FORMASPAGO.CODFORMAPAGOSOBREPAGO as OlmosTarjeta
                FROM TESORERIA 
                LEFT JOIN FORMASPAGO ON TESORERIA.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO 
                LEFT JOIN VENCIMFPAGO ON FORMASPAGO.CODFORMAPAGO = VENCIMFPAGO.CODFORMAPAGO 
                LEFT JOIN TIPOSPAGO ON VENCIMFPAGO.CODTIPOPAGO = TIPOSPAGO.CODTIPOPAGO 
                WHERE TESORERIA.serie = @NumSerie AND TESORERIA.NUMERO = @Numero AND TESORERIA.N = @N";

            List<Pagos> _pagos = new List<Pagos>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@NumSerie", _serie);
                _cmd.Parameters.AddWithValue("@N", _N);
                _cmd.Parameters.AddWithValue("@Numero", _numero);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            Pagos _cls = new Pagos();
                            _cls.descripcion = _reader["TARJETA"].ToString();
                            _cls.tipopago = _reader["TIPOPAGO"].ToString();
                            _cls.monto = Convert.ToDecimal(_reader["IMPORTE"]);
                            _cls.cuotas = _reader["COMENTARIOVISIBLE"].ToString();
                            _cls.cupon = "";
                            _cls.formapago = _reader["FormaPago"].ToString();
                            _cls.codigotarjeta = _reader["CodigoTarjeta"].ToString();
                            _cls.olmosTipoPago = _reader["OlmosTipoPago"].ToString();
                            _cls.olmosTarjeta = _reader["OlmosTarjeta"].ToString();

                            _pagos.Add(_cls);
                        }
                    }
                }
            }

            return _pagos;
        }

        public static List<Pagos> GetPagos(Doc doc) {
            List<Pagos> result = new List<Pagos>();
            return result;
        }
    }
}
