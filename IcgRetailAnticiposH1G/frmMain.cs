﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace IcgRetailAnticiposH1G
{
    public partial class frmMain : Form
    {
        public string _ip;
        public string _server;
        public string _user;
        public string _catalog;
        public int _puerto = 0;
        public int _baudios = 0;
        public Hasar1raGen.Clases.ModelosImpresoras.Modelos _modelo = 0;

        //public SqlConnection _conn = new SqlConnection();
        public string _N = "B";
        public string _numfiscal;
        public string _textoRegalo1;
        public string _textoRegalo2;
        public string _textoRegalo3;
        public string _textoAjuste;
        public string _caja;
        public string _terminal;
        public int _tipo = 12;
        public int _accion = 1;
        public bool _hasarLog = false;
        public string strConnection;
        public static string _password;
        public string _keyIcg = "";
        public bool _KeyIsOk;

        public frmMain()
        {
            InitializeComponent();
            this.Text = this.Text + " Consola Retail de Impresión Anticipos- V." + Application.ProductVersion;            
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists("Config1raGeneracion.xml"))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load("Config1raGeneracion.xml");

                    XmlNodeList _List1 = xDoc.SelectNodes("/config");
                    foreach (XmlNode xn in _List1)
                    {

                        //_ip = xn["ip"].InnerText;
                        _caja = xn["caja"].InnerText;
                        _textoRegalo1 = xn["tktregalo1"].InnerText;
                        _textoRegalo2 = xn["tktregalo2"].InnerText;
                        _textoRegalo3 = xn["tktregalo3"].InnerText;
                        _textoAjuste = xn["textoajuste"].InnerText;
                        _terminal = xn["terminal"].InnerText;
                        //_password = xn["super"].InnerText;
                        //_hasarLog = Convert.ToBoolean(xn["hasarlog"].InnerText);
                        _keyIcg = xn["keyICG"].InnerText;
                    }

                    XmlNodeList _Listdb = xDoc.SelectNodes("/config/bd");
                    foreach (XmlNode xn in _Listdb)
                    {
                        _server = xn["server"].InnerText;
                        _user = xn["user"].InnerText;
                        _catalog = xn["database"].InnerText;
                    }

                    XmlNodeList _ListImpresora = xDoc.SelectNodes("/config/impresora");
                    foreach (XmlNode xn in _ListImpresora)
                    {
                        Hasar1raGen.Clases.ModelosImpresoras.Modelos x = 0;

                        Enum.TryParse(xn["modelo"].InnerText, out x);

                        _modelo = x;
                        _puerto = Convert.ToInt32(xn["puerto"].InnerText);
                        _baudios = Convert.ToInt32(xn["baudios"].InnerText);

                        Hasar1raGen.Acciones.baudios = _baudios;
                        Hasar1raGen.Acciones.modelo = _modelo;
                        Hasar1raGen.Acciones.puerto = _puerto;
                    }

                    // Armamos el stringConnection.
                    strConnection = "Data Source=" + _server + ";Initial Catalog=" + _catalog + ";User Id=" + _user + ";Password=masterkey;";

                    // Validamos el KeySoftware.
                    _KeyIsOk = ValidoKeySoftware(_keyIcg);
                    if (_KeyIsOk)
                    {
                        this.dtpFecha.Value = DateTime.Now;

                        SetGrid();
                    }
                    else
                    {
                        MessageBox.Show("La licencia no es valida. Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        this.Close();
                    }
                }
                else
                {
                    //DeshabilitarBotones();
                    MessageBox.Show("No se encuentra el archivo de configuración. Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Se producjo el siguente error: " + ex.Message + Environment.NewLine + ". Por favor comuniquese con ICG Argentina.",
                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        #region Privados
        private bool ValidoKeySoftware(string keyIcg)
        {
            string _key = IcgVarios.NumerosSerie.MotherBoardUUID();

            string _cod = IcgVarios.Licencia.GenerarLicenciaICG(_key);

            if (keyIcg == _cod)
                return true;
            else
                return false;

        }

        /// <summary>
        /// Encrypts the string.
        /// </summary>
        /// <param name="clearText">The clear text.</param>
        /// <param name="Key">The key.</param>
        /// <param name="IV">The IV.</param>
        /// <returns></returns>
        private static byte[] EncryptString(byte[] clearText, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = Key;
            alg.IV = IV;
            CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(clearText, 0, clearText.Length);
            cs.Close();
            byte[] encryptedData = ms.ToArray();
            return encryptedData;
        }

        //private void GetDataset(SqlConnection _con)
        private void GetDataset(string _connection)
        {
            string _sql;

            _sql = @"SELECT PEDVENTACAB.NUMSERIE, PEDVENTACAB.NUMPEDIDO, PEDVENTACAB.N, PEDVENTACAB.SUPEDIDO, PEDVENTACAB.CODCLIENTE, PEDVENTACAB.FECHAPEDIDO, PEDVENTACAB.TOTBRUTO, PEDVENTACAB.TOTNETO, PEDVENTACAB.TOTIMPUESTOS,
                CLIENTES.NOMBRECLIENTE, CLIENTES.DIRECCION1, CLIENTES.NIF20,
                TESORERIA.IMPORTE
                from PEDVENTACAB inner join CLIENTES on PedventaCab.codcliente = Clientes.CODCLIENTE
                inner join TESORERIA on PEDVENTACAB.SUPEDIDO = TESORERIA.SUDOCUMENTO
                Where PEDVENTACAB.NUMEROALBARAN = -1
                AND TESORERIA.ORIGEN = 'C' AND TESORERIA.TIPODOCUMENTO = 'E' AND PEDVENTACAB.FECHAPEDIDO = @Fecha";
            // Limpio el dataset
            dtsAnticipos.Tables["Impresiones"].Clear();
            using (SqlConnection _con = new SqlConnection(_connection))
            {
                _con.Open();

                using (SqlCommand _cmd = new SqlCommand(_sql, _con))
                {
                    _cmd.Parameters.AddWithValue("@Fecha", dtpFecha.Value.Date);
                    using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                    {
                        _sda.Fill(dtsAnticipos, "Impresiones");
                    }
                }
            }
        }

        private void SetGrid()
        {
            dgvMain.Columns[7].DefaultCellStyle.Format = "c";
            dgvMain.Columns[8].DefaultCellStyle.Format = "c";
            dgvMain.Columns[9].DefaultCellStyle.Format = "c";
            dgvMain.Columns[12].DefaultCellStyle.Format = "c";
        }
        #endregion

        private void btExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMain.RowCount > 0)
                {
                    if (dgvMain.SelectedRows.Count > 0)
                    {
                        string _serie = dgvMain.SelectedRows[0].Cells[0].Value.ToString();
                        int _numero = Convert.ToInt32(dgvMain.SelectedRows[0].Cells[1].Value);
                        string _n = dgvMain.SelectedRows[0].Cells[2].Value.ToString();
                        int _codCliente = Convert.ToInt32(dgvMain.SelectedRows[0].Cells[4].Value);
                        decimal _importe = Convert.ToDecimal(dgvMain.SelectedRows[0].Cells[7].Value);

                        Hasar1raGen.Clientes _cliente = new Hasar1raGen.Clientes();
                        List<Hasar1raGen.ItemsAnticipo> _anticipo = new List<Hasar1raGen.ItemsAnticipo>();
                        using (SqlConnection _con = new SqlConnection(strConnection))
                        {
                            _con.Open();

                            _cliente = Hasar1raGen.Clientes.GetCliente(_codCliente,_con);
                            _anticipo = Hasar1raGen.ItemsAnticipo.GetItems(_serie, _n, _numero, _con);

                        }

                        bool _faltaPapel;
                        Hasar1raGen.Impresiones.ImprimirAnticipo(_cliente, _anticipo, _codCliente.ToString(), _importe, out _faltaPapel);

                        if (_faltaPapel)
                            MessageBox.Show("La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se producjo el siguente error: " + ex.Message + Environment.NewLine + ". Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void dtpFecha_ValueChanged(object sender, EventArgs e)
        {
            GetDataset(strConnection);
        }

        private void dgvMain_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
