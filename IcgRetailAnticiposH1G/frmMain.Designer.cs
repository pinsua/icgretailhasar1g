﻿namespace IcgRetailAnticiposH1G
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvMain = new System.Windows.Forms.DataGridView();
            this.dtsAnticipos = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.btImprimir = new System.Windows.Forms.Button();
            this.btExit = new System.Windows.Forms.Button();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.nUMSERIEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nUMPEDIDODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sUPEDIDODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cODCLIENTEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fECHAPEDIDODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nOMBRECLIENTEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iMPORTEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tOTBRUTODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tOTNETODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dIRECCION1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NIF20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTIMPUESTOS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsAnticipos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMain
            // 
            this.dgvMain.AllowUserToAddRows = false;
            this.dgvMain.AllowUserToDeleteRows = false;
            this.dgvMain.AllowUserToResizeRows = false;
            this.dgvMain.AutoGenerateColumns = false;
            this.dgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nUMSERIEDataGridViewTextBoxColumn,
            this.nUMPEDIDODataGridViewTextBoxColumn,
            this.nDataGridViewTextBoxColumn,
            this.sUPEDIDODataGridViewTextBoxColumn,
            this.cODCLIENTEDataGridViewTextBoxColumn,
            this.fECHAPEDIDODataGridViewTextBoxColumn,
            this.nOMBRECLIENTEDataGridViewTextBoxColumn,
            this.iMPORTEDataGridViewTextBoxColumn,
            this.tOTBRUTODataGridViewTextBoxColumn,
            this.tOTNETODataGridViewTextBoxColumn,
            this.dIRECCION1DataGridViewTextBoxColumn,
            this.NIF20,
            this.TOTIMPUESTOS});
            this.dgvMain.DataMember = "Impresiones";
            this.dgvMain.DataSource = this.dtsAnticipos;
            this.dgvMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvMain.Location = new System.Drawing.Point(0, 0);
            this.dgvMain.MultiSelect = false;
            this.dgvMain.Name = "dgvMain";
            this.dgvMain.ReadOnly = true;
            this.dgvMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMain.Size = new System.Drawing.Size(800, 240);
            this.dgvMain.TabIndex = 0;
            // 
            // dtsAnticipos
            // 
            this.dtsAnticipos.DataSetName = "NewDataSet";
            this.dtsAnticipos.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13});
            this.dataTable1.TableName = "Impresiones";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "SERIE";
            this.dataColumn1.ColumnName = "NUMSERIE";
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "PEDIDO";
            this.dataColumn2.ColumnName = "NUMPEDIDO";
            this.dataColumn2.DataType = typeof(int);
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "N";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "SUPEDIDO";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "CODCLIENTE";
            this.dataColumn5.DataType = typeof(int);
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "FECHA";
            this.dataColumn6.ColumnName = "FECHAPEDIDO";
            this.dataColumn6.DataType = typeof(System.DateTime);
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "TOTBRUTO";
            this.dataColumn7.DataType = typeof(decimal);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "TOTNETO";
            this.dataColumn8.DataType = typeof(decimal);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "TOTIMPUESTOS";
            this.dataColumn9.DataType = typeof(decimal);
            // 
            // dataColumn10
            // 
            this.dataColumn10.Caption = "CLIENTE";
            this.dataColumn10.ColumnName = "NOMBRECLIENTE";
            // 
            // dataColumn11
            // 
            this.dataColumn11.Caption = "DIRECCION";
            this.dataColumn11.ColumnName = "DIRECCION1";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "NIF20";
            // 
            // dataColumn13
            // 
            this.dataColumn13.Caption = "ANTICIPO";
            this.dataColumn13.ColumnName = "IMPORTE";
            this.dataColumn13.DataType = typeof(decimal);
            // 
            // btImprimir
            // 
            this.btImprimir.Location = new System.Drawing.Point(353, 246);
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(268, 23);
            this.btImprimir.TabIndex = 1;
            this.btImprimir.Text = "Imprimir Recibo";
            this.btImprimir.UseVisualStyleBackColor = true;
            this.btImprimir.Click += new System.EventHandler(this.btImprimir_Click);
            // 
            // btExit
            // 
            this.btExit.Location = new System.Drawing.Point(661, 246);
            this.btExit.Name = "btExit";
            this.btExit.Size = new System.Drawing.Size(75, 23);
            this.btExit.TabIndex = 2;
            this.btExit.Text = "Salir";
            this.btExit.UseVisualStyleBackColor = true;
            this.btExit.Click += new System.EventHandler(this.btExit_Click);
            // 
            // dtpFecha
            // 
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecha.Location = new System.Drawing.Point(171, 246);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(119, 20);
            this.dtpFecha.TabIndex = 3;
            this.dtpFecha.ValueChanged += new System.EventHandler(this.dtpFecha_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(114, 251);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Fecha";
            // 
            // nUMSERIEDataGridViewTextBoxColumn
            // 
            this.nUMSERIEDataGridViewTextBoxColumn.DataPropertyName = "NUMSERIE";
            this.nUMSERIEDataGridViewTextBoxColumn.HeaderText = "Serie";
            this.nUMSERIEDataGridViewTextBoxColumn.Name = "nUMSERIEDataGridViewTextBoxColumn";
            this.nUMSERIEDataGridViewTextBoxColumn.ReadOnly = true;
            this.nUMSERIEDataGridViewTextBoxColumn.Width = 50;
            // 
            // nUMPEDIDODataGridViewTextBoxColumn
            // 
            this.nUMPEDIDODataGridViewTextBoxColumn.DataPropertyName = "NUMPEDIDO";
            this.nUMPEDIDODataGridViewTextBoxColumn.HeaderText = "Nro. Pedido";
            this.nUMPEDIDODataGridViewTextBoxColumn.Name = "nUMPEDIDODataGridViewTextBoxColumn";
            this.nUMPEDIDODataGridViewTextBoxColumn.ReadOnly = true;
            this.nUMPEDIDODataGridViewTextBoxColumn.Width = 80;
            // 
            // nDataGridViewTextBoxColumn
            // 
            this.nDataGridViewTextBoxColumn.DataPropertyName = "N";
            this.nDataGridViewTextBoxColumn.HeaderText = "N";
            this.nDataGridViewTextBoxColumn.Name = "nDataGridViewTextBoxColumn";
            this.nDataGridViewTextBoxColumn.ReadOnly = true;
            this.nDataGridViewTextBoxColumn.Visible = false;
            // 
            // sUPEDIDODataGridViewTextBoxColumn
            // 
            this.sUPEDIDODataGridViewTextBoxColumn.DataPropertyName = "SUPEDIDO";
            this.sUPEDIDODataGridViewTextBoxColumn.HeaderText = "SUPEDIDO";
            this.sUPEDIDODataGridViewTextBoxColumn.Name = "sUPEDIDODataGridViewTextBoxColumn";
            this.sUPEDIDODataGridViewTextBoxColumn.ReadOnly = true;
            this.sUPEDIDODataGridViewTextBoxColumn.Visible = false;
            // 
            // cODCLIENTEDataGridViewTextBoxColumn
            // 
            this.cODCLIENTEDataGridViewTextBoxColumn.DataPropertyName = "CODCLIENTE";
            this.cODCLIENTEDataGridViewTextBoxColumn.HeaderText = "Cod. Cliente";
            this.cODCLIENTEDataGridViewTextBoxColumn.Name = "cODCLIENTEDataGridViewTextBoxColumn";
            this.cODCLIENTEDataGridViewTextBoxColumn.ReadOnly = true;
            this.cODCLIENTEDataGridViewTextBoxColumn.Width = 50;
            // 
            // fECHAPEDIDODataGridViewTextBoxColumn
            // 
            this.fECHAPEDIDODataGridViewTextBoxColumn.DataPropertyName = "FECHAPEDIDO";
            this.fECHAPEDIDODataGridViewTextBoxColumn.HeaderText = "Fecha Pedido";
            this.fECHAPEDIDODataGridViewTextBoxColumn.Name = "fECHAPEDIDODataGridViewTextBoxColumn";
            this.fECHAPEDIDODataGridViewTextBoxColumn.ReadOnly = true;
            this.fECHAPEDIDODataGridViewTextBoxColumn.Width = 80;
            // 
            // nOMBRECLIENTEDataGridViewTextBoxColumn
            // 
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.DataPropertyName = "NOMBRECLIENTE";
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.HeaderText = "Nombre Cliente";
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.Name = "nOMBRECLIENTEDataGridViewTextBoxColumn";
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.ReadOnly = true;
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.Width = 150;
            // 
            // iMPORTEDataGridViewTextBoxColumn
            // 
            this.iMPORTEDataGridViewTextBoxColumn.DataPropertyName = "IMPORTE";
            this.iMPORTEDataGridViewTextBoxColumn.HeaderText = "Importe";
            this.iMPORTEDataGridViewTextBoxColumn.Name = "iMPORTEDataGridViewTextBoxColumn";
            this.iMPORTEDataGridViewTextBoxColumn.ReadOnly = true;
            this.iMPORTEDataGridViewTextBoxColumn.Width = 80;
            // 
            // tOTBRUTODataGridViewTextBoxColumn
            // 
            this.tOTBRUTODataGridViewTextBoxColumn.DataPropertyName = "TOTBRUTO";
            this.tOTBRUTODataGridViewTextBoxColumn.HeaderText = "Tot. Bruto";
            this.tOTBRUTODataGridViewTextBoxColumn.Name = "tOTBRUTODataGridViewTextBoxColumn";
            this.tOTBRUTODataGridViewTextBoxColumn.ReadOnly = true;
            this.tOTBRUTODataGridViewTextBoxColumn.Width = 80;
            // 
            // tOTNETODataGridViewTextBoxColumn
            // 
            this.tOTNETODataGridViewTextBoxColumn.DataPropertyName = "TOTNETO";
            this.tOTNETODataGridViewTextBoxColumn.HeaderText = "Tot. Neto";
            this.tOTNETODataGridViewTextBoxColumn.Name = "tOTNETODataGridViewTextBoxColumn";
            this.tOTNETODataGridViewTextBoxColumn.ReadOnly = true;
            this.tOTNETODataGridViewTextBoxColumn.Width = 80;
            // 
            // dIRECCION1DataGridViewTextBoxColumn
            // 
            this.dIRECCION1DataGridViewTextBoxColumn.DataPropertyName = "DIRECCION1";
            this.dIRECCION1DataGridViewTextBoxColumn.HeaderText = "Dirección";
            this.dIRECCION1DataGridViewTextBoxColumn.Name = "dIRECCION1DataGridViewTextBoxColumn";
            this.dIRECCION1DataGridViewTextBoxColumn.ReadOnly = true;
            this.dIRECCION1DataGridViewTextBoxColumn.Width = 150;
            // 
            // NIF20
            // 
            this.NIF20.DataPropertyName = "NIF20";
            this.NIF20.HeaderText = "DNI/CUIT";
            this.NIF20.Name = "NIF20";
            this.NIF20.ReadOnly = true;
            this.NIF20.Width = 80;
            // 
            // TOTIMPUESTOS
            // 
            this.TOTIMPUESTOS.DataPropertyName = "TOTIMPUESTOS";
            this.TOTIMPUESTOS.HeaderText = "Tot. con Impuestos";
            this.TOTIMPUESTOS.Name = "TOTIMPUESTOS";
            this.TOTIMPUESTOS.ReadOnly = true;
            this.TOTIMPUESTOS.Width = 80;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 282);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.btExit);
            this.Controls.Add(this.btImprimir);
            this.Controls.Add(this.dgvMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMain";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsAnticipos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMain;
        private System.Data.DataSet dtsAnticipos;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Windows.Forms.Button btImprimir;
        private System.Windows.Forms.Button btExit;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nUMSERIEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nUMPEDIDODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sUPEDIDODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cODCLIENTEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fECHAPEDIDODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nOMBRECLIENTEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iMPORTEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tOTBRUTODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tOTNETODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dIRECCION1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NIF20;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOTIMPUESTOS;
    }
}